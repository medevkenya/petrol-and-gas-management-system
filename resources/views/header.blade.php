<header class="header">
    <div class="page-brand">
        <a class="link" href="index.html">
            <span class="brand"><?php echo env("APP_SHORT_NAME"); ?>
                <!-- <span class="brand-tip">CAST</span> -->
            </span>
            <span class="brand-mini">PG</span>
        </a>
    </div>
    <div class="flexbox flex-1">
        <!-- START TOP-LEFT TOOLBAR-->
        <ul class="nav navbar-toolbar">
            <li>
                <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
            </li>
            <li>
                <form class="navbar-search" action="javascript:;">
                    <div class="rel">
                        <span class="search-icon"><i class="ti-search"></i></span>
                        <input class="form-control" placeholder="Search here...">
                    </div>
                </form>
            </li>
        </ul>
        <!-- END TOP-LEFT TOOLBAR-->
        <!-- START TOP-RIGHT TOOLBAR-->
        <ul class="nav navbar-toolbar">
            <li class="dropdown dropdown-notification">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o rel"><span class="notify-signal"></span></i></a>
                <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media">
                    <li class="dropdown-menu-header">
                        <div>
                            <span><strong><?php echo \App\Notifications::countToday(); ?> New</strong> Notifications</span>
                            <a class="pull-right" href="{{URL::to('/notifications')}}">view all</a>
                        </div>
                    </li>
                    <li class="list-group list-group-divider scroller" data-height="240px" data-color="#71808f">
                        <div>
                          <?php $notifications = \App\Notifications::getLatest();
                          foreach ($notifications as $nt) { ?>
                            <a class="list-group-item">
                                <div class="media">
                                    <div class="media-img">
                                        <span class="badge badge-success badge-big"><i class="fa fa-check"></i></span>
                                    </div>
                                    <div class="media-body">
                                        <div class="font-13"><?php echo $nt->message; ?></div><small class="text-muted"><?php echo $nt->created_at->diffForHumans(); ?></small></div>
                                </div>
                            </a>
                          <?php } ?>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="dropdown dropdown-user">
                <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                    <img src="{{ URL::to('/') }}/profiles/<?php echo Auth::user()->profilePic; ?>"/>
                    <span></span>{{Auth::user()->firstName}} {{Auth::user()->lastName}}<i class="fa fa-angle-down m-l-5"></i></a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{URL::to('/profile')}}"><i class="fa fa-user"></i>Profile</a>
                    <!-- <a class="dropdown-item" href="{{URL::to('/profile')}}"><i class="fa fa-lock"></i>Change Password</a> -->
                    <!-- <a class="dropdown-item" href="{{URL::to('/support')}}"><i class="fa fa-support"></i>Support</a> -->
                    <li class="dropdown-divider"></li>
                    <a class="dropdown-item" href="{{URL::to('/logout')}}"><i class="fa fa-power-off"></i>Logout</a>
                </ul>
            </li>
        </ul>
        <!-- END TOP-RIGHT TOOLBAR-->
    </div>
</header>
