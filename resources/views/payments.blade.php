<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Payments</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Payments</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#"><i class="la la-home font-20"></i></a>
                    </li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                  @if (count($errors) > 0)
                         <div class="alert alert-danger">
                             <ul>
                                 @foreach ($errors->all() as $error)
                                 <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                         </div>
                        @endif

                        @if ($message = Session::get('error'))
                             <div class="alert alert-danger">
                                 {{ $message }}
                             </div>
                        @endif

                        @if ($message = Session::get('success'))
                             <div class="alert alert-success">
                                 {{ $message }}
                             </div>
                        @endif

                        @if (session('status0'))
                        <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ session('status0') }}
                        </div>
                        @endif

                        @if (session('status1'))
                        <div class="alert alert-success alert-dismissible alertbox" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ session('status1') }}
                        </div>
                        @endif

                    <div class="col-md-6">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Payments For <?php echo date('F', mktime(0, 0, 0, $currentMonth, 10)); ?></div>
                                <div class="ibox-tools">
                                  {!! Form::open(['url' => 'paymentsbymonth']) !!}
                                  <select class="form-control" name="month" id="month" style="margin-left:-100%;margin-bottom:-28%;" required>
                                 <option value="<?php echo date('m'); ?>"><?php echo date('F'); ?></option>
                                 <?php for($i=1; $i<=12; $i++){ $month = date('F', mktime(0, 0, 0, $i, 10)); $monthNumber = date('m', mktime(0, 0, 0, $i, 10));  ?>
                                   <option value="<?php echo $monthNumber; ?>"><?php echo $month; ?></option>
                                 <?php } ?>
                               </select>
                                <button type="submit" class="btn btn-success" style="margin-left:5%;"><i class="fa fa-search"></i> Search</button>
                                {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="ibox-body">
                                    <?php if(isset($list) && !empty($list)) { ?>
                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                          <th>Date</th>
                                          <th>Amount (Ksh.)</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                      <tr>
                                        <th>Date</th>
                                        <th>Amount (Ksh.)</th>
                                      </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $sale) {
                                        ?>
                                        <tr>
                                          <td><?php echo $sale['dayKey'].".".$currentMonth.".".date('Y'); ?></td>
                                          <td><?php echo $sale['sums']; ?></td>
                                        </tr>
                                      <?php } ?>
                                    </tbody>
                                </table>
                              <?php } ?>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-6">
                            <div class="ibox">
                                <div class="ibox-head">
                                    <div class="ibox-title">Payments Year <?php echo $year; $lastYear = date("Y",strtotime("-1 year")); ?></div>
                                    <div class="ibox-tools">
                                      {!! Form::open(['url' => 'paymentsbyyear']) !!}
                                      <select class="form-control" name="year" id="year" style="margin-left:-100%;margin-bottom:-40%;" required>
                                     <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
                                     <?php foreach(range($lastYear, 2020) as $year){  ?>
                                       <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                     <?php } ?>
                                   </select>
                                    <button type="submit" class="btn btn-success" style="margin-left:5%;"><i class="fa fa-search"></i> Search</button>
                                    {!! Form::close() !!}
                                    </div>
                                </div>
                                <div class="ibox-body">
                                        <?php if(isset($yearlist) && !empty($yearlist)) { ?>
                                        <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                              <th>Month</th>
                                              <th>Amount (Ksh.)</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                          <tr>
                                            <th>Month</th>
                                            <th>Amount (Ksh.)</th>
                                          </tr>
                                        </tfoot>
                                        <tbody>
                                          <?php foreach ($yearlist as $saley) {
                                            ?>
                                            <tr>
                                              <td><?php echo date('F', mktime(0, 0, 0, $saley['monthKey'], 10)); ?></td>
                                              <td><?php echo $saley['sums']; ?></td>
                                            </tr>
                                          <?php } ?>
                                        </tbody>
                                    </table>
                                  <?php } ?>
                                </div>
                              </div>
                            </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')

  </body>

  </html>
