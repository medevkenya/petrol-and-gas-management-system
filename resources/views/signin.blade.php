<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Login</title>
    @include('headerlinks')

    <style>
    form {
      border-radius:4px;
    }
    </style>
</head>

<body class="bg-silver-300">
    <div class="content" style="background-image: linear-gradient(135deg, #28696D, #66C2C2);border-radius:4px;">
        <div class="brand" style="border-radius:4px;">
            <a class="link" href="#">
<img src="{{ URL::to('/') }}/images/logo.png" width="80%"/>
            </a>
        </div>

          @if (count($errors) > 0)
                 <div class="alert alert-danger">
                     <ul>
                         @foreach ($errors->all() as $error)
                         <li>{{ $error }}</li>
                         @endforeach
                     </ul>
                 </div>
                @endif

                @if ($message = Session::get('error'))
                     <div class="alert alert-danger">
                         {{ $message }}
                     </div>
                @endif

                @if ($message = Session::get('success'))
                     <div class="alert alert-success">
                         {{ $message }}
                     </div>
                @endif

               {!! Form::open(['url'=>'dosignin','id'=>'login-form']) !!}
               {{ csrf_field() }}
            <h2 class="login-title">Log In</h2>
            <div class="form-group">
                <div class="input-group-icon right">
                    <div class="input-icon"><i class="fa fa-envelope"></i></div>
                    <input class="form-control" type="email" name="email" value="{{ old('email') }}" placeholder="Email" autocomplete="off">
                    @if ($errors->has('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                      @endif
                </div>
            </div>
            <div class="form-group">
                <div class="input-group-icon right">
                    <div class="input-icon"><i class="fa fa-lock font-16"></i></div>
                    <input class="form-control" type="password" name="password" placeholder="Password">
                    @if ($errors->has('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                      @endif
                </div>
            </div>
            <div class="form-group d-flex justify-content-between">
                <label class="ui-checkbox ui-checkbox-info">
                    <input type="checkbox">
                    <span class="input-span"></span>Remember me</label>
                <a href="{{URL::to('/forgotPassword')}}">Forgot password?</a>
            </div>
            <div class="form-group">
                <button class="btn btn-info btn-block" type="submit">Login</button>
            </div>


            <!-- <div class="text-center">Check Pricing
                <a class="color-blue" href="register.html">Check Pe</a>
            </div> -->
        </form>
    </div>
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    @include('footerlinks')
</body>

</html>
