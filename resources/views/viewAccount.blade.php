<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Account Transfers</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title"><?php echo $details->accountName; ?></h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All Account Transfers</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Account Transfers <?php if(isset($fromdate)) { echo "From: ".$fromdate." To: ".$todate; } ?></div>
                                              <div class="ibox-tools">
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addrole"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-addrole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                {!! Form::open(['url' => 'addaccounttransfer']) !!}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">

                                                  <div class="col-sm-6 form-group">
                                                      <label>Account</label>
                                                      <select class="form-control" name="accountId" id="accountId" required>
                                                       <option value="<?php echo $details->id; ?>"><?php echo $details->accountName; ?></option>
                                                   </select>
                                                  </div>

                                                  <div class="col-sm-6 form-group">
                                                      <label>Amount (Ksh.)</label>
                                                      <input class="form-control" type="number" name="amount" required>
                                                  </div>

                                                  <div class="col-sm-12 form-group">
                                                      <label>Reason</label>
                                                      <textarea rows="3" class="form-control" name="reason" required></textarea>
                                                  </div>

                                                </div>

                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>

                                            @if (count($errors) > 0)
                                               <div class="alert alert-danger">
                                                   <ul>
                                                       @foreach ($errors->all() as $error)
                                                       <li>{{ $error }}</li>
                                                       @endforeach
                                                   </ul>
                                               </div>
                                              @endif

                                              @if ($message = Session::get('error'))
                                                   <div class="alert alert-danger">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if ($message = Session::get('success'))
                                                   <div class="alert alert-success">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if (session('status0'))
                                              <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status0') }}
                                              </div>
                                              @endif

                                              @if (session('status1'))
                                              <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status1') }}
                                              </div>
                                              @endif

                                              {!! Form::open(['url' => 'postfiltertransfersbyaccount']) !!}
                                              {{ csrf_field() }}
                                              <div class="row">

                                                <input type="hidden" class="form-control" name="accountId" value="<?php echo $details->id; ?>" required>

                                              <div class="form-group col-md-3">
                                                <label>From Date</label>
                                                <input type="date" class="form-control" name="fromdate" required>
                                              </div>
                                              <div class="form-group col-md-3">
                                                <label>To Date</label>
                                                <input type="date" class="form-control" name="todate" required>
                                              </div>
                                              <div class="form-group col-md-3">
                                                <label>.</label>
                                                <button type="submit" class="btn btn-block btn-primary">Search</button>
                                              </div>
                                              </div>
                                              {!! Form::close() !!}

                                              <?php if(isset($list) && !empty($list)) { ?>

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Type</th>
                                            <th>Amount (Ksh.)</th>
                                            <th>Date</th>
                                            <th>Reason</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                             <th>Type</th>
                                            <th>Amount (Ksh.)</th>
                                            <th>Date</th>
                                            <th>Reason</th>
                                            <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php $totals = 0; foreach ($list as $accounttransfer) {
                                        $totals += $accounttransfer->amount;
                                        ?>
                                        <tr>
                                          <td><?php echo $accounttransfer->type; ?></td>
                                          <td><?php echo $accounttransfer->amount; ?></td>
                                          <td><?php echo $accounttransfer->created_at; ?></td>
                                          <td><?php echo $accounttransfer->reason; ?></td>
                                          <td>
                                          <!-- <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editrole<?php //echo $accounttransfer->id; ?>"><i class="fa fa-edit"></i> Edit</button> -->
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleterole<?php echo $accounttransfer->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                        </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-editrole<?php echo $accounttransfer->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'editaccounttransfer']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Shift</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $accounttransfer->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Account</label>
                                              <select class="form-control" name="accountId" id="accountId" required>
                                             <option value="<?php echo $accounttransfer->accountId; ?>"><?php echo $accounttransfer->accountName; ?></option>
                                           </select>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Amount (Ksh.)</label>
                                              <input class="form-control" type="number" name="amount" value="<?php echo $accounttransfer->amount; ?>" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Reason</label>
                                              <textarea rows="3" class="form-control" name="reason" required><?php echo $accounttransfer->reason; ?></textarea>
                                          </div>

                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deleterole<?php echo $accounttransfer->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deleteaccounttransfer']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Shift</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $accounttransfer->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this account transfer</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>

                                <h4 style="margin-top:2%;"><strong>Total Ksh.</strong> <?php echo number_format($totals,0); ?></h4>

                              <?php } ?>

                                          </div>
                                        </div>
                                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
