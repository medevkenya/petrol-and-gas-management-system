<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Accounts</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Accounts</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All Accounts</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

              <?php $list = \App\Accounts::getAll(); ?>

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Accounts</div>
                                              <div class="ibox-tools">
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addrole"><i class="fa fa-plus"></i> Create New</button>
                                                  <a href="<?php $url = URL::to("/statements/"); print_r($url); ?>">
                                                    <button type="button" class="btn btn-warning"><i class="fa fa-list"></i> Get Statement as at <?php echo date('d-m-Y H:i'); ?>
                                                    </button>
                                                    </a>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-addrole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                {!! Form::open(['url' => 'addaccount']) !!}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New Account</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">

                                              <div class="col-sm-12 form-group">
                                                  <label>Account Name</label>
                                                  <input class="form-control" type="text" name="accountName" required>
                                              </div>

                                              <div class="col-sm-12 form-group">
                                                  <label>Account Balance</label>
                                                  <input class="form-control" type="number" name="balance" required>
                                              </div>

                                            </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>

                                            @if (count($errors) > 0)
                                               <div class="alert alert-danger">
                                                   <ul>
                                                       @foreach ($errors->all() as $error)
                                                       <li>{{ $error }}</li>
                                                       @endforeach
                                                   </ul>
                                               </div>
                                              @endif

                                              @if ($message = Session::get('error'))
                                                   <div class="alert alert-danger">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if ($message = Session::get('success'))
                                                   <div class="alert alert-success">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if (session('status0'))
                                              <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status0') }}
                                              </div>
                                              @endif

                                              @if (session('status1'))
                                              <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status1') }}
                                              </div>
                                              @endif

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Account Name</th>
                                            <th>Balance</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Account Name</th>
                                          <th>Balance</th>
                                          <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $account) {
                                        ?>
                                        <tr>
                                          <td><?php echo $account->accountName; ?></td>
                                          <td><?php echo $account->balance; ?></td>
                                            <td>
                                              <a href="<?php $url = URL::to("/viewAccount/".$account->id); print_r($url); ?>" class="btn btn-primary"><i class="fa fa-eye"></i> View Account</a>
                                              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-withdraw<?php echo $account->id; ?>"><i class="fa fa-edit"></i> Withdraw Cash</button>
                                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editrole<?php echo $account->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleterole<?php echo $account->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                        </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-editrole<?php echo $account->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'editaccount']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Account</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $account->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Account Name</label>
                                              <input class="form-control" type="text" name="accountName" value="<?php echo $account->accountName; ?>" required>
                                          </div>

                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-withdraw<?php echo $account->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'withdrawcash']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Withdraw Cash</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $account->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Amount</label>
                                              <input class="form-control" type="number" name="amount" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Reason</label>
                                              <textarea rows="6" class="form-control" name="reason" required></textarea>
                                          </div>

                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deleterole<?php echo $account->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deleteaccount']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Account</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $account->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this account</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>

                                          </div>
                                        </div>
                                      </div>

                              <div class="col-md-12">
                                  <div class="ibox">
                                      <div class="ibox-head">
                                          <div class="ibox-title">Account Payments For <?php echo date('F', mktime(0, 0, 0, $currentMonth, 10)); ?></div>
                                          <div class="ibox-tools">
                                            {!! Form::open(['url' => 'accountpaymentsbymonth']) !!}
                                            <select class="form-control" name="month" id="month" style="margin-left:-100%;margin-bottom:-28%;" required>
                                           <option value="<?php echo date('m'); ?>"><?php echo date('F'); ?></option>
                                           <?php for($i=1; $i<=12; $i++){ $month = date('F', mktime(0, 0, 0, $i, 10)); $monthNumber = date('m', mktime(0, 0, 0, $i, 10));  ?>
                                             <option value="<?php echo $monthNumber; ?>"><?php echo $month; ?></option>
                                           <?php } ?>
                                         </select>
                                          <button type="submit" class="btn btn-success" style="margin-left:5%;"><i class="fa fa-search"></i> Search</button>
                                          {!! Form::close() !!}
                                          </div>
                                      </div>
                                      <div class="ibox-body">
                                              <?php if(isset($monthlist) && !empty($monthlist)) { ?>
                                              <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                              <thead>
                                                  <tr>
                                                    <th>Date</th>
                                                    <?php foreach ($list as $accountm) { ?>
                                                    <th><?php echo $accountm->accountName; ?></th>
                                                  <?php } ?>
                                                  <th>Total</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                <?php foreach ($monthlist as $sale) {
                                                  $datem = date('Y')."-".$currentMonth."-".$sale['dayKey'];
                                                  ?>
                                                  <tr>
                                                    <td><?php echo $sale['dayKey'].".".$currentMonth.".".date('Y'); ?></td>
                                                    <?php foreach ($list as $accountmt) { ?>
                                                    <td><?php echo \App\Statements::getTotalAcountForDate($accountmt->id,$datem); ?></td>
                                                    <?php } ?>
                                                    <td><?php echo \App\Statements::getTotalForDate($datem); ?></td>
                                                  </tr>
                                                <?php } ?>
                                                <tr>
                                                  <th></th>
                                                  <?php $ggtots = 0; foreach ($list as $accountmto) { ?>
                                                  <th><?php echo $gtot = \App\Statements::getTotalAcountForMonth($accountmto->id,$currentMonth); ?></th>
                                                <?php $ggtots += $gtot; } ?>
                                                <th>Total: Ksh. <?php echo $ggtots; ?></th>
                                                </tr>
                                              </tbody>
                                          </table>
                                        <?php } ?>
                                      </div>
                                    </div>
                                  </div>


                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
