<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Suppliers</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Suppliers</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All suppliers</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Suppliers</div>
                                              <div class="ibox-tools">
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addsupplier"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-addsupplier" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                {!! Form::open(['url' => 'addsupplier']) !!}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New Supplier</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">

                                              <div class="col-sm-12 form-group">
                                                  <label>Supplier Name</label>
                                                  <input class="form-control" type="text" name="supplierName" required>
                                              </div>
                                              <div class="col-sm-12 form-group">
                                                  <label>Email Address</label>
                                                  <input class="form-control" type="email" name="email">
                                              </div>
                                              <div class="col-sm-12 form-group">
                                                  <label>Contacts</label>
                                                  <input class="form-control" type="text" name="contacts">
                                              </div>
                                            </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>

                                            @if (count($errors) > 0)
                                               <div class="alert alert-danger">
                                                   <ul>
                                                       @foreach ($errors->all() as $error)
                                                       <li>{{ $error }}</li>
                                                       @endforeach
                                                   </ul>
                                               </div>
                                              @endif

                                              @if ($message = Session::get('error'))
                                                   <div class="alert alert-danger">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if ($message = Session::get('success'))
                                                   <div class="alert alert-success">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if (session('status0'))
                                              <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status0') }}
                                              </div>
                                              @endif

                                              @if (session('status1'))
                                              <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status1') }}
                                              </div>
                                              @endif

                                              <?php $accounts = \App\Accounts::getAll(); ?>

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email Address</th>
                                            <th>Contacts</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Name</th>
                                          <th>Email Address</th>
                                          <th>Contacts</th>
                                          <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $supplier)
                                      {
                                      //  $amountOwed = \App\Batches::where('supplierId',$supplier->id)->where('type','Credit')->where('isPaid','Pending')->where('isDeleted',0)->sum('purchasePrice');
                                        ?>
                                        <tr>
                                          <td><?php echo $supplier->supplierName; ?></td>
                                          <td><?php echo $supplier->email; ?></td>
                                          <td><?php echo $supplier->contacts; ?></td>
                                          <td>
                                            <a href="<?php $url = URL::to("/viewSupplier/".$supplier->id); print_r($url); ?>" class="btn btn-primary"><i class="fa fa-eye"></i> View Supplier</a>
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-receivepayment<?php echo $supplier->id; ?>"><i class="fa fa-money"></i> Submit Payment</button>
                                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editsupplier<?php echo $supplier->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deletesupplier<?php echo $supplier->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                          </td>
                                          </tr>

                                          <!-- Modal -->
                                          <div class="modal fade text-left" id="modal-receivepayment<?php echo $supplier->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                              {!! Form::open(['url' => 'paysupplier']) !!}
                                            <div class="modal-content">
                                              <div class="modal-header">
                                              <h4 class="modal-title" id="myModalLabel1">Amount paid to supplier</h4>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                              </div>
                                              <div class="modal-body">
                                              <div class="row">
                                              <div class="col-xl-12 col-lg-12 col-md-12">
                                                <input type="hidden" name="id" value="<?php echo $supplier->id; ?>" class="form-control" required>
                                            </div>

                                            <div class="col-sm-12 form-group">
                                                <label>Amount (Ksh.)</label>
                                                <input class="form-control" type="number" name="amount" required>
                                            </div>

                                            <div class="col-sm-12 form-group">
                                              <label>Pay from which account</label>
                                              <select class="form-control" name="accountId" required>
                                               <option value=""></option>
                                               <?php foreach ($accounts as $keyacc) { ?>
                                                 <option value="<?php echo $keyacc->id; ?>"><?php echo $keyacc->accountName; ?></option>
                                               <?php } ?>
                                             </select>
                                            </div>

                                          </div>
                                              </div>
                                              <div class="modal-footer">
                                              <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                              <button type="submit" class="btn btn-primary">Submit</button>
                                              </div>
                                            </div>
                                            {!! Form::close() !!}
                                            </div>
                                          </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-editsupplier<?php echo $supplier->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'editsupplier']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Supplier</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $supplier->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Supplier Name</label>
                                              <input class="form-control" type="text" name="supplierName" value="<?php echo $supplier->supplierName; ?>" required>
                                          </div>
                                          <div class="col-sm-12 form-group">
                                              <label>Email Address</label>
                                              <input class="form-control" type="email" name="email" value="<?php echo $supplier->email; ?>">
                                          </div>
                                          <div class="col-sm-12 form-group">
                                              <label>Contacts</label>
                                              <input class="form-control" type="text" name="contacts" value="<?php echo $supplier->contacts; ?>">
                                          </div>

                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deletesupplier<?php echo $supplier->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deletesupplier']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Supplier</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $supplier->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this supplier</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>

                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-12">
                                          <div class="ibox">
                                              <div class="ibox-head">
                                                <div class="ibox-title">Statements</div>
                                              </div>
                                              <div class="ibox-body">
                                      <table class="table table-striped table-bordered table-hover" id="example-table3" cellspacing="0" width="100%">
                                      <thead>
                                          <tr>
                                              <th>Name</th>
                                              <th>Credit Brought Forward</th>
                                              <th>Current Month Supply</th>
                                              <th>Current Month Payment (Cash/Bank)</th>
                                              <th>Net Balance</th>
                                          </tr>
                                      </thead>
                                      <tfoot>
                                          <tr>
                                              <th>Name</th>
                                              <th>Credit Brought Forward</th>
                                              <th>Current Month Supply</th>
                                              <th>Current Month Payment (Cash/Bank)</th>
                                              <th>Net Balance</th>
                                          </tr>
                                      </tfoot>
                                      <tbody>
                                        <?php $grandbalanceBroughtForward= 0;
                                              $grandcurrentMonthDebt = 0;
                                              $granddebtPaidThisMonth = 0;
                                              $grandnetDebt = 0;
                                               foreach ($statements as $statement) {
                                          $grandbalanceBroughtForward += intval(preg_replace('/[^\d.]/', '', $statement['balanceBroughtForward']));
                                          $grandcurrentMonthDebt += intval(preg_replace('/[^\d.]/', '', $statement['currentMonthDebt']));
                                          $granddebtPaidThisMonth += intval(preg_replace('/[^\d.]/', '', $statement['debtPaidThisMonth']));
                                          if($statement['netDebt'] > 0) {
                                          $grandnetDebt += intval(preg_replace('/[^\d.]/', '', $statement['netDebt']));
                                          }
                                          else {
                                           $grandnetDebt -= intval(preg_replace('/[^\d.]/', '', $statement['netDebt']));
                                          }
                                          ?>
                                          <tr>
                                            <td><?php echo $statement['supplierName']; ?></td>
                                            <td><?php echo $statement['balanceBroughtForward']; ?></td>
                                            <td>
                                              <a href="<?php $url = URL::to("/supplierbatches/".$statement['supplierId']); print_r($url); ?>">
                                              <?php echo $statement['currentMonthDebt']; ?>
                                            </a></td>
                                            <td><a href="<?php $url = URL::to("/supplierpayments/".$statement['supplierId']); print_r($url); ?>">
                                              <?php echo $statement['debtPaidThisMonth']; ?></a></td>
                                            <td><?php echo $statement['netDebt']; ?></td>
                                          </tr>
                                        <?php } ?>
                                        <tfoot>
                                          <tr>
                                            <td><strong>TOTALS</strong></td>
                                            <td><strong><?php echo number_format($grandbalanceBroughtForward,2); ?></strong></td>
                                            <td><strong><?php echo number_format($grandcurrentMonthDebt,2); ?></strong></td>
                                            <td><strong><?php echo number_format($granddebtPaidThisMonth,2); ?></strong></td>
                                            <td><strong><?php echo number_format($grandnetDebt,2); ?></strong></td>
                                          </tr>
                                        </tfoot>
                                      </tbody>
                                    </table>
                                    </div>
                                    </div>
                                    </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
    <script>
    $(document).ready(function() {
        $('#example-table3').DataTable();
    } );
    </script>
  </body>

  </html>
