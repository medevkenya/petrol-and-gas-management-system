<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Batches</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Batches</h1>
                <p style="color:#8B0000;">Confirm that dip stick reading is updated before adding new stock for the day.</p>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All Batches</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Batches</div>
                                              <div class="ibox-tools">
                                                  <a href="<?php $url = URL::to("/stockreport/"); print_r($url); ?>">
                                                    <button type="button" class="btn btn-warning"><i class="fa fa-list"></i> Get Report as at <?php echo date('d-m-Y H:i'); ?>
                                                    </button>
                                                    </a>
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addrole"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-addrole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                {!! Form::open(['url' => 'addbatch']) !!}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New Batch</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">

                                                  <?php
                                                  $categories = \App\Categories::getAll();
                                                  $suppliers = \App\Suppliers::getAll();
                                                  $tanks = \App\Tanks::getAll();
                                                  $accounts = \App\Accounts::getAll();
                                                  ?>

                                                  <div class="col-sm-12 form-group">
                                                      <label>Supplier</label>
                                                      <select class="form-control" name="supplierId" required>
                                                     <option value=""></option>
                                                     <?php foreach ($suppliers as $keyfs) { ?>
                                                       <option value="<?php echo $keyfs->id; ?>"><?php echo $keyfs->supplierName; ?></option>
                                                     <?php } ?>
                                                   </select>
                                                  </div>

                                                  <div class="col-sm-12 form-group">
                                                      <label>Category</label>
                                                      <select class="form-control" name="categoryId" id="categoryIdBatch" required>
                                                     <option></option>
                                                     <?php foreach ($categories as $keyf) { ?>
                                                       <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->categoryName; ?></option>
                                                     <?php } ?>
                                                   </select>
                                                  </div>

                                                  <div class="col-sm-12 form-group">
                                                      <label>Product</label>
                                                      <select class="form-control" name="productId" id="batchProductId" required>
                                                   </select>
                                                  </div>

                                                </div>
                                                <div class="row">

                                              <div class="col-sm-6 form-group">
                                                  <label>Quantity</label>
                                                  <input class="form-control" type="number" step="0.01" min="0" name="quantity" required>
                                              </div>

                                              <div class="col-sm-6 form-group">
                                                  <label>Type</label>
                                                  <select class="form-control" name="type" id="type" required>
                                                    <option value=""></option>
                                                   <option value="Cash">Cash</option>
                                                   <option value="Credit">Credit</option>
                                               </select>
                                              </div>

                                            </div>
                                            <div class="row">

                                          <div class="col-sm-6 form-group">
                                              <label>Purchase Price</label>
                                              <input class="form-control" type="number" step="0.01" min="0" name="purchasePrice" required>
                                          </div>

                                          <div class="col-sm-6 form-group" id="accountId" style="display:none;">
                                              <label>Pay from which account</label>
                                              <select class="form-control" name="accountId" style="border-color: #cf850f;">
                                             <option value=""></option>
                                             <?php foreach ($accounts as $key_account) { ?>
                                               <option value="<?php echo $key_account->id; ?>"><?php echo $key_account->accountName; ?></option>
                                             <?php } ?>
                                           </select>
                                          </div>

                                          <div class="col-sm-12 form-group" id="tankId" style="display:none;">
                                              <label>Tank</label>
                                              <select class="form-control" name="tankId" id="tanks" style="border-color: #cf850f;">
                                             <option value=""></option>
                                           </select>
                                          </div>



                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>

                                            @if (count($errors) > 0)
                                             <div class="alert alert-danger">
                                                 <ul>
                                                     @foreach ($errors->all() as $error)
                                                     <li>{{ $error }}</li>
                                                     @endforeach
                                                 </ul>
                                             </div>
                                            @endif

                                            @if ($message = Session::get('error'))
                                                 <div class="alert alert-danger">
                                                     {{ $message }}
                                                 </div>
                                            @endif

                                            @if ($message = Session::get('success'))
                                                 <div class="alert alert-success">
                                                     {{ $message }}
                                                 </div>
                                            @endif

                                            @if (session('status0'))
                                            <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            {{ session('status0') }}
                                            </div>
                                            @endif

                                            @if (session('status1'))
                                            <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            {{ session('status1') }}
                                            </div>
                                            @endif

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Supplier</th>
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>Purchase Price</th>
                                            <th>Total Value</th>
                                            <th>Created On</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php foreach ($list as $batch) {
                                        ?>
                                        <tr>
                                          <td><?php echo $batch->supplierName; ?></td>
                                          <td><?php echo $batch->productName; ?></td>
                                          <td><?php echo $batch->quantity." ".$batch->unit; ?></td>
                                          <!-- <td><?php //echo $batch->quantity - $batch->sold; ?></td>
                                          <td><?php //echo $batch->sold." ".$batch->unit; ?></td> -->
                                          <td><?php echo $batch->purchasePrice; ?></td>
                                          <td><?php echo number_format($batch->purchasePrice * $batch->quantity,0); ?></td>
                                          <td><?php echo $batch->created_at; ?></td>
                                          <td>
                                          <!-- <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editrole<?php //echo $batch->id; ?>"><i class="fa fa-edit"></i> Edit</button> -->
                                          <!-- <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-edit<?php echo $batch->id; ?>"><i class="fa fa-edit"></i> Eject-Stock</button> -->
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleterole<?php echo $batch->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                        </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-edit<?php echo $batch->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'ejectStock']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Eject Batch</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                              <input type="hidden" name="id" value="<?php echo $batch->id; ?>" class="form-control" required>
                                              <div class="col-sm-6 form-group">
                                                  <label>Quantity</label>
                                                  <input class="form-control" type="number" step="0.01" name="quantity" required>
                                              </div>
                                            </div>

                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-editrole<?php echo $batch->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'editbatch']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Batch</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $batch->id; ?>" class="form-control" required>
                                          </div>
                                          <div class="col-sm-12 form-group">
                                              <label>Supplier</label>
                                              <select class="form-control" name="supplierId" required>
                                             <option value="<?php echo $batch->supplierId; ?>"><?php echo $batch->supplierName; ?></option>
                                             <?php foreach ($suppliers as $keyfs) { ?>
                                               <option value="<?php echo $keyfs->id; ?>"><?php echo $keyfs->supplierName; ?></option>
                                             <?php } ?>
                                           </select>
                                          </div>
                                          <div class="col-sm-12 form-group">
                                            <label>Category</label>
                                            <select class="form-control" name="categoryId" onchange="categoryIdbe('<?php echo $batch->id; ?>')" id="categoryIdbe<?php echo $batch->id; ?>" required>
                                             <option value="<?php echo $batch->categoryId; ?>"><?php echo $batch->categoryName; ?></option>
                                             <?php foreach ($categories as $keyf) { ?>
                                               <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->categoryName; ?></option>
                                             <?php } ?>
                                           </select>
                                          </div>
                                          <div class="col-sm-12 form-group">
                                            <label>Product</label>
                                            <select class="form-control" name="productId" id="productIde<?php echo $batch->id; ?>" required>
                                              <option value="<?php echo $batch->productId; ?>"><?php echo $batch->productName; ?></option>
                                           </select>
                                          </div>
                                        </div>
                                        <div class="row">
                                          <div class="col-sm-6 form-group">
                                              <label>Quantity</label>
                                              <input class="form-control" type="number" step="0.01" name="quantity" value="<?php echo $batch->quantity; ?>" required>
                                          </div>
                                          <div class="col-sm-6 form-group">
                                              <label>Type</label>
                                              <select class="form-control" name="type" required>
                                                <option value="<?php echo $batch->type; ?>"><?php echo $batch->type; ?></option>
                                               <option value="Cash">Cash</option>
                                               <option value="Credit">Credit</option>
                                           </select>
                                          </div>
                                        </div>
                                        <div class="row">

                                          <div class="col-sm-6 form-group">
                                              <label>Purchase Price</label>
                                              <input class="form-control" type="number" name="purchasePrice" value="<?php echo $batch->purchasePrice; ?>" required>
                                          </div>

                                          <?php if($batch->productId == 1 || $batch->productId == 2) { ?>
                                          <div class="col-sm-12 form-group" id="tankIdEdit" style="display:none;">
                                              <label>Tank</label>
                                              <select class="form-control" name="tankId" required>
                                             <option value="<?php echo $batch->tankId; ?>"><?php echo $batch->tankName; ?></option>
                                             <?php foreach ($tanks as $key_tank) { ?>
                                               <option value="<?php echo $key_tank->id; ?>"><?php echo $key_tank->tankName; ?></option>
                                             <?php } ?>
                                           </select>
                                          </div>
                                          <?php } ?>

                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deleterole<?php echo $batch->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deletebatch']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Batch</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $batch->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this batch</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>

                                          </div>
                                        </div>
                                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')

    @include('datatablesfooter')

    <script>
    $("#batchProductId").change(function(){
      var productId = $(this).val(); console.log("batchProductId---"+productId);
      if(productId == "1" || productId == "2")
      {
        document.getElementById("tankId").style.display = "block";

        $.ajax({
            url: "{{ route('getTanks') }}?productId=" + $(this).val(),
            method: 'GET',
            success: function(data) {
              //console.log("js products--"+data.products);
                $('#tanks').html(data.tanks);
            }
        });

      }
      else
      {
        document.getElementById("tankId").style.display = "none";
      }
    });

    $("#type").change(function(){
      var type = $(this).val(); console.log("type---"+type);
      if(type == "Cash")
      {
        document.getElementById("accountId").style.display = "block";
      }
      else
      {
        document.getElementById("accountId").style.display = "none";
      }
    });
    </script>

  </body>

  </html>
