<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<style>
    html, body {font-size: 16px;}
</style>
<table style="background:#00AEED;width:100%;border-radius:4px;padding-left:2%;">
<p style="color:#fff;">
Dear {{ $fullName }},
</p>
<p style="color:#fff;">
Order No.: {{ $orderNo }}
</p>
<p style="color:#fff;">
Category: {{ $categoryName }} | {{ $subCategoryName }}
</p>
<p style="color:#fff;">
Status: {{ $orderStatusName }}
</p>
<?php if($link !=null) { ?>
<p>
  <a href="{{$link}}" style="text-decoration:none;background-color: #FFA500;
      color: #000000;
      padding: 14px 20px;
      margin: 8px 0;
      border: none;
      cursor: pointer;
      border-radius: 4px;
      width: 100%;">{{$button}}</a>
</p>
<?php } ?>
<br><br>
<p>We request you to add our email to your address book to avoid missing out on information that we send to you from time to time.</p>
<p style="color:#000000;">
Thank you for choosing AlternetSMS.
</p>
<p style="color:#fff;">All emails sent to or from AlternetSMS are subject to AlternetSMS's Terms & Conditions of use.</p>
<p style="color:#fff;">&copy; All rights reserved</p>
</table>
