<table style="background: #ffffff;
color: #000000;width:100%;border-radius:4px;">
<img src="{{ URL::to('/') }}/images/logo.png" style="width:15%;"/>
<p style="color:#000000;">
The following product is below stock
</p>
<p style="color:#fff;">
Product: {{$productName}}
</p>
<p style="color:#fff;">
Current Quantity: {{ $quantity }}
</p>
<hr>
<p style="color:#000000;">All emails sent to or from <?php echo env("MAIL_FROM_NAME"); ?> are subject to our Terms & Conditions of use.</p>
<p style="color:#000000;">&copy; All rights reserved</p>
</table>
