<table style="background: #ffffff;
color: #000000;width:100%;border-radius:4px;">
<img src="{{ URL::to('/') }}/images/logo.png" style="width:15%;"/>
<p style="color:#000000;">
Your AlternetSMS, SMS sent status.
</p>
<p style="color:#fff;">
Dear {{ $fullName }}, below is a summary of a just concluded SMS sent out.
</p>
<p style="color:#fff;">
Batch Size: {{ $batch_size }}
</p>
<p style="color:#fff;">
Batch Sent: {{ $batch_sent }}
</p>
<p style="color:#fff;">
Failed to Full 3rd Trial: {{ $fullTrial }}
</p>
<hr>
<p style="color:#fff;">
Units Balance: {{ $unitBalance }}
</p>
<hr>
<p style="color:#000000;">
Thank you for using AlternetSMS as your favourite Bulk SMS platform.
</p>
<p style="color:#000000;">All emails sent to or from AlternetSMS are subject to our Terms & Conditions of use.</p>
<p style="color:#000000;">&copy; All rights reserved</p>
</table>
