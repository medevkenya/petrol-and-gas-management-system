<table style="background: #ffffff;
color: #000000;width:100%;border-radius:4px;">
<img src="{{ URL::to('/') }}/images/logo.png" style="width:15%;"/>
<p style="color:#000000;">
Your new AlternetSMS account password has been set successfully.
</p>
<hr>
<p style="color:#000000;">
Thank you for choosing AlternetSMS.
</p>
<p style="color:#000000;">All emails sent to or from AlternetSMS are subject to our Terms & Conditions of use.</p>
<p style="color:#000000;">&copy; All rights reserved</p>
</table>
