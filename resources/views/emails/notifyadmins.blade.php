<table style="background: #ffffff;
color: #000000;width:100%;border-radius:4px;">
<img src="{{ URL::to('/') }}/images/logo.png" style="width:15%;"/>
<p style="color:#000000;">
Dear {{$name}},
</p>
<hr>
<h4>
{{$type}}
</h4>
<p style="color:#000000;">
{{$description}}
</p>
<p style="color:#000000;">
{{$reason}}
</p>
<p style="color:#000000;">All emails sent to or from <?php echo env("APP_NAME"); ?> are subject to our Terms & Conditions of use.</p>
<p style="color:#000000;">&copy; All rights reserved</p>
</table>
