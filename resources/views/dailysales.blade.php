<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Daily Sales</title>
    @include('headerlink')
    @include('datatables')
    <style>
    table tr th {
      text-transform: uppercase;
    }

    table#imagetable tbody
    {
       overflow-y: auto;
       height: 100px !important;
    }

table#imagetable {
    width: 100% !important;
    font-family: verdana,arial,sans-serif;
    font-size:11px;
    color:#333333;
    border-width: 1px;
    border-color: #999999;
}

table#imagetable th
{
    background:#5499c7 ;
    border-width: 1px;
    padding: 8px;
    border-style: solid;
    border-color: #ffffff;
    text-align:left;
    color:#ffffff;
}

table#imagetable td {
  background:#fff;
  border-width: 1px;
  padding: 8px;
  border-style: solid;
  border-color: #999999;
}

.tfoot {
  background:#f1c40f !important;
  color: #ffffff;
}
    </style>
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Daily Sales</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All Daily Sales</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Daily Sales</div>
                                              <div class="ibox-tools">
                                                {!! Form::open(['url' => 'searchdailysales']) !!}
                                              <input type="date" class="form-control" name="date" style="margin-left:-100%;margin-bottom:-28%;" value="<?php echo $date; ?>" required>
                                              <button type="submit" class="btn btn-success" style="margin-left:5%;"><i class="fa fa-search"></i> Search</button>
                                              {!! Form::close() !!}
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            @if (count($errors) > 0)
                                               <div class="alert alert-danger">
                                                   <ul>
                                                       @foreach ($errors->all() as $error)
                                                       <li>{{ $error }}</li>
                                                       @endforeach
                                                   </ul>
                                               </div>
                                              @endif

                                              @if ($message = Session::get('error'))
                                                   <div class="alert alert-danger">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if ($message = Session::get('success'))
                                                   <div class="alert alert-success">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if (session('status0'))
                                              <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status0') }}
                                              </div>
                                              @endif

                                              @if (session('status1'))
                                              <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status1') }}
                                              </div>
                                              @endif

                                              <table class="table table-striped table-bordered table-hover" id="imagetable" cellspacing="0" width="100%">
                                              <thead>
                                                  <tr>
                                                      <th>Product</th>
                                                      <th width="14%">Previous<br>Meter Reading</th>
                                                      <th width="14%">Current<br>Meter Reading</th>
                                                      <th width="10%">Litres Sold</th>
                                                      <th width="10%">Unit Price</th>
                                                      <th width="10%">Cost Price</th>
                                                      <th width="10%">Amount</th>
                                                      <th width="7%">Margin</th>
                                                      <th width="10%">Profit</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                <?php
                                                //PETROL
                                                $totalsold = 0;
                                                $totalamount = 0;
                                                $totalprofit = 0;

                                                 foreach ($list as $item) {
                                                   $readdetails = \App\Readings::pumpMeterReading($item->shiftId,$date);
                                                   $currentmeterreading = $readdetails['openReading'];
                                                   $previousmeterreading = $readdetails['closeReading'];

                                                   $sold = $item->cashQuantity + $item->creditQuantity;
                                                   $totalsold += intval(preg_replace('/[^\d.]/', '', $sold));

                                                   $amount = $item->sellPrice * ($item->cashQuantity + $item->creditQuantity);
                                                   $totalamount += intval(preg_replace('/[^\d.]/', '', $amount));

                                                   $profit = $item->sellPrice * ($item->cashQuantity + $item->creditQuantity) - $item->costPrice * ($item->cashQuantity + $item->creditQuantity);
                                                   $totalprofit += intval(preg_replace('/[^\d.]/', '', $profit));

                                                   $shiftdetails = \App\Shifts::getShiftDetails($item->shiftId);

                                                  ?>
                                                  <tr>
                                                    <td><?php echo $shiftdetails->firstName; ?> <?php echo $shiftdetails->lastName; ?> (<?php echo $shiftdetails->pumpName; ?>) (<?php echo $shiftdetails->shifttypeName; ?>)</td>
                                                      <td><?php echo $previousmeterreading; ?></td>
                                                      <td><?php echo $currentmeterreading; ?></td>
                                                      <td><?php echo $sold; ?></td>
                                                      <td><?php echo $item->sellPrice; ?></td>
                                                      <td><?php echo $item->costPrice; ?></td>
                                                      <td><?php echo $amount; ?></td>
                                                      <td><?php echo $item->sellPrice - $item->costPrice; ?></td>
                                                      <td><?php echo $profit; ?></td>
                                                  </tr>
                                                <?php } ?>
                                                <tfoot>
                                                  <tr>
                                                    <td class="tfoot"><strong>TOTALS PETROL</strong></td>
                                                    <td class="tfoot"></td>
                                                    <td class="tfoot"></td>
                                                    <td class="tfoot"><strong><?php echo number_format($totalsold,0); ?></strong></td>
                                                    <td class="tfoot"></td>
                                                    <td class="tfoot"></td>
                                                    <td class="tfoot"><strong>Ksh. <?php echo number_format($totalamount,0); ?></strong></td>
                                                    <td class="tfoot"></td>
                                                    <td class="tfoot"><strong>Ksh. <?php echo number_format($totalprofit,0); ?></strong></td>
                                                  </tr>
                                                </tfoot>
                                              </tbody>
                                          </table>

                                    <table class="table table-striped table-bordered table-hover" id="imagetable" cellspacing="0" width="100%">
                                    <thead>
                                      <tr>
                                          <th>Product</th>
                                          <th width="14%">Previous<br>Meter Reading</th>
                                          <th width="14%">Current<br>Meter Reading</th>
                                          <th width="10%">Litres Sold</th>
                                          <th width="10%">Unit Price</th>
                                          <th width="10%">Cost Price</th>
                                          <th width="10%">Amount</th>
                                          <th width="7%">Margin</th>
                                          <th width="10%">Profit</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <?php
                                      //PETROL
                                      $totalsoldd = 0;
                                      $totalamountd = 0;
                                      $totalprofitd = 0;

                                       foreach ($listdisel as $itemd) {

                                         $readdetailsd = \App\Readings::pumpMeterReading($itemd->shiftId,$date);
                                         $currentmeterreadingd = $readdetailsd['openReading'];
                                         $previousmeterreadingd = $readdetailsd['closeReading'];

                                         $soldd = $itemd->cashQuantity + $itemd->creditQuantity;
                                         $totalsoldd += intval(preg_replace('/[^\d.]/', '', $soldd));

                                         $amountd = $itemd->sellPrice * ($itemd->cashQuantity + $itemd->creditQuantity);
                                         $totalamountd += intval(preg_replace('/[^\d.]/', '', $amountd));

                                         $profitd = $itemd->sellPrice * ($itemd->cashQuantity + $itemd->creditQuantity) - $itemd->costPrice * ($itemd->cashQuantity + $itemd->creditQuantity);
                                         $totalprofitd += intval(preg_replace('/[^\d.]/', '', $profitd));

                                         $shiftdetails = \App\Shifts::getShiftDetails($itemd->shiftId);

                                        ?>
                                        <tr>
                                          <td><?php echo $shiftdetails->firstName; ?> <?php echo $shiftdetails->lastName; ?> (<?php echo $shiftdetails->pumpName; ?>) (<?php echo $shiftdetails->shifttypeName; ?>)</td>
                                            <td><?php echo $previousmeterreadingd; ?></td>
                                            <td><?php echo $currentmeterreadingd; ?></td>
                                            <td><?php echo $soldd; ?></td>
                                            <td><?php echo $itemd->sellPrice; ?></td>
                                            <td><?php echo $itemd->costPrice; ?></td>
                                            <td><?php echo $amountd; ?></td>
                                            <td><?php echo $itemd->sellPrice - $itemd->costPrice; ?></td>
                                            <td><?php echo $profitd; ?></td>
                                        </tr>
                                      <?php } ?>
                                      <tfoot>
                                        <tr>
                                          <td class="tfoot"><strong>TOTALS DIESEL</strong></td>
                                          <td class="tfoot"></td>
                                          <td class="tfoot"></td>
                                          <td class="tfoot"><strong><?php echo number_format($totalsoldd,0); ?></strong></td>
                                          <td class="tfoot"></td>
                                          <td class="tfoot"></td>
                                          <td class="tfoot"><strong>Ksh. <?php echo number_format($totalamountd,0); ?></strong></td>
                                          <td class="tfoot"></td>
                                          <td class="tfoot"><strong>Ksh. <?php echo number_format($totalprofitd,0); ?></strong></td>
                                        </tr>
                                      </tfoot>
                                    </tbody>
                                </table>

                                <table class="table table-striped table-bordered table-hover" id="imagetable" cellspacing="0" width="100%">
                                <tbody>
                                  <tfoot>
                                    <tr>
                                      <td class="tfoot"><strong>TOTALS</strong></td>
                                      <td class="tfoot" width="14%"></td>
                                      <td class="tfoot" width="14%"></td>
                                      <td class="tfoot" width="10%"><strong><?php $Gtotalsoldd = $totalsoldd + $totalsold; echo number_format($Gtotalsoldd,0); ?></strong></td>
                                      <td class="tfoot" width="10%"></td>
                                      <td class="tfoot" width="10%"></td>
                                      <td class="tfoot" width="10%"><strong>Ksh. <?php $Gtotalamountd = $totalamountd + $totalamount; echo number_format($Gtotalamountd,0); ?></strong></td>
                                      <td class="tfoot" width="7%"></td>
                                      <td class="tfoot" width="10%"><strong>Ksh. <?php $Gtotalprofitd = $totalprofitd + $totalprofit; echo number_format($Gtotalprofitd,0); ?></strong></td>
                                    </tr>
                                  </tfoot>
                                </tbody>
                            </table>

                                <table class="table table-striped table-bordered table-hover" id="imagetable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Opening Stock</th>
                                        <th>Added Stock</th>
                                        <th>Sold</th>
                                        <th>Closing Stock</th>
                                        <th>Dip Stick Reading</th>
                                        <th>Gain/Short</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  //TANKS
                                  $totalstocksold = 0;

                                   foreach ($listtanks as $tank) {

                                     $stocktracking = \App\Stocktracking::where('date',$date)->where('productId',$tank->productId)->first();
                                     if($stocktracking) {
                                       $openingstock = $stocktracking->openingstock;
                                       $closingstock = $stocktracking->closingstock;
                                     }
                                     else {
                                       $openingstock = 0;
                                       $closingstock = 0;
                                     }

                                     $addedstock = \App\Batches::where('date',$date)->where('productId',$tank->productId)->sum('quantity');
                                     $cashsoldstock = \App\Sales::where('date',$date)->where('productId',$tank->productId)->sum('cashQuantity');
                                     $creditsoldstock = \App\Sales::where('date',$date)->where('productId',$tank->productId)->sum('creditQuantity');
                                     $soldstock = $cashsoldstock + $creditsoldstock;

                                     $stickreading = \App\Readings::where('date',$date)->where('tankId',$tank->id)->orderBy('id','DESC')->limit(1)->first();
                                     if($stickreading) {
                                     $dipstick = $stickreading->meterReading;
                                   }
                                   else {
                                     $dipstick = 0;
                                   }

                                     $totalstocksold += $soldstock;

                                     $gl = $closingstock - $dipstick;
                                     if($gl < 0) {
                                       $gainloss = "(".$gl.")";
                                     }
                                     else {
                                       $gainloss = $gl;
                                     }

                                    ?>
                                    <tr>
                                      <td><?php echo $tank->tankName; ?> - <?php echo $tank->productName; ?></td>
                                        <td><?php echo $openingstock; ?></td>
                                        <td><?php echo $addedstock; ?></td>
                                        <td><?php echo $soldstock; ?></td>
                                        <td><?php echo $closingstock; ?></td>
                                        <td><?php echo $dipstick; ?></td>
                                        <td><?php echo $gainloss; ?></td>
                                    </tr>
                                  <?php } ?>
                                  <tfoot>
                                    <tr>
                                      <td class="tfoot"><strong>TOTALS</strong></td>
                                      <td class="tfoot"></td>
                                      <td class="tfoot"></td>
                                      <td class="tfoot"><strong><?php echo number_format($totalstocksold,0); ?></strong></td>
                                      <td class="tfoot"></td>
                                      <td class="tfoot"></td>
                                      <td class="tfoot"></td>
                                    </tr>
                                  </tfoot>
                                </tbody>
                                </table>

                                <?php
                                $products = \App\Products::select('id','productName','sellPrice','stockbalance')->where('isDeleted',0)->get();
                                $totalsales = \App\Sales::where('date',$date)->where('isDeleted',0)->sum('total');
                                $debtspaid = \App\Paiddebts::where('date',$date)->where('isDeleted',0)->sum('amount');
                                ?>

                                <h2>Income Sources</h2>
                                <table class="table table-striped table-bordered table-hover" id="imagetable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Unit Price</th>
                                        <th>Sold</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php foreach ($products as $catss) {
                                    $prosales = \App\Sales::where('date',$date)->where('productId',$catss->id)->where('isDeleted',0)->sum('total');
                                    $proquantity = \App\Sales::where('date',$date)->where('productId',$catss->id)->where('isDeleted',0)->sum('quantity');
                                    ?>
                                    <tr>
                                      <td><?php echo $catss->productName; ?></td>
                                      <td><?php echo $catss->sellPrice; ?></td>
                                      <td><?php echo $proquantity; ?></td>
                                        <td><?php echo $prosales; ?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                      <td>Debts Paid</td>
                                      <td></td>
                                      <td></td>
                                        <td><?php echo $debtspaid; ?></td>
                                    </tr>
                                    <tr>
                                      <td>Total Sales</td>
                                      <td></td>
                                      <td></td>
                                        <td><?php echo $totalsales; ?></td>
                                    </tr>
                                </tbody>
                                </table>

                                <?php
                                $allaccounts = \App\Accounts::getAll();
                                $discounts = \App\Sales::where('date',$date)->where('isDeleted',0)->sum('discount');
                                ?>

                                <h2>Breakdown Accounts</h2>
                                <table class="table table-striped table-bordered table-hover" id="imagetable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Total Sales</th>
                                        <?php foreach ($allaccounts as $acc) { ?>
                                        <th><?php echo $acc->accountName; ?></th>
                                        <?php } ?>
                                        <th>Discounts</th>
                                        <th>Gain/Short</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                      <td><?php echo $totalsales; ?></td>
                                      <?php foreach ($allaccounts as $acct) {
                                        $totaltransfer = \App\Statements::
                                        where('accountId',$acct->id)
                                        ->where('date',$date)
                                        ->where('type','Deposit')
                                        ->where('isDeleted',0)
                                        ->orwhere('accountId',$acct->id)
                                        ->where('date',$date)
                                        ->where('type','Transfer')
                                        ->where('isDeleted',0)->sum('amount');
                                        ?>
                                        <td><?php echo $totaltransfer; ?></td>
                                      <?php } ?>
                                        <td><?php echo $discounts; ?></td>
                                        <td><?php echo 0; ?></td>
                                    </tr>
                                </tbody>
                                </table>

                                <h2>Debtors</h2>
                                <table class="table table-striped table-bordered table-hover" id="imagetable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  $totaldebt = 0;
                                  $conlist = \App\Customerconsumption::getAllByDate($date);
                                  foreach ($conlist as $keyconl) {
                                    $totaldebt += intval(preg_replace('/[^\d.]/', '', $keyconl->total_debit));
                                    $customerDetails = \App\Customers::where('id',$keyconl->customerId)->first();
                                    $customerName = null;
                                    if($customerDetails) {
                                      $customerName = $customerDetails->customerName;
                                    }
                                    ?>
                                    <tr>
                                      <td><?php echo $customerName; ?></td>
                                      <td><?php echo $keyconl->total_debit; ?></td>
                                    </tr>
                                  <?php } ?>
                                  <tfoot>
                                    <tr>
                                      <td class="tfoot"><strong>TOTALS</strong></td>
                                      <td class="tfoot"><strong><?php echo number_format($totaldebt,2); ?></strong></td>
                                    </tr>
                                  </tfoot>
                                </tbody>
                              </table>


                                          </div>
                                        </div>
                                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
