<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Statements</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Statements <?php if(isset($accountName)) { echo $accountName; } ?></h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#"><i class="la la-home font-20"></i></a>
                    </li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                  <div class="col-md-12">
                      <div class="ibox">
                          <div class="ibox-head">
                              <div class="ibox-title">Payment Transactions</div>
                          </div>
                          <div class="ibox-body">
                            @if (count($errors) > 0)
                                   <div class="alert alert-danger">
                                       <ul>
                                           @foreach ($errors->all() as $error)
                                           <li>{{ $error }}</li>
                                           @endforeach
                                       </ul>
                                   </div>
                                  @endif

                                  @if ($message = Session::get('error'))
                                       <div class="alert alert-danger">
                                           {{ $message }}
                                       </div>
                                  @endif

                                  @if ($message = Session::get('success'))
                                       <div class="alert alert-success">
                                           {{ $message }}
                                       </div>
                                  @endif

                                  @if (session('status0'))
                                  <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  {{ session('status0') }}
                                  </div>
                                  @endif

                                  @if (session('status1'))
                                  <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  {{ session('status1') }}
                                  </div>
                                  @endif

                                    <?php if(isset($list) && !empty($list)) { ?>
                                      <table class="table table-striped table-bordered table-hover" id="example-table2" cellspacing="0" width="100%">
                                      <thead>
                                          <tr>
                                              <th>Account</th>
                                              <th>Amount (Ksh.)</th>
                                              <th>Type</th>
                                              <th>Description</th>
                                              <th>Customer</th>
                                              <th>Supplier</th>
                                              <th>Date</th>
                                              <th>Actions</th>
                                          </tr>
                                      </thead>
                                      <tfoot>
                                        <tr>
                                            <th>Account</th>
                                            <th>Amount (Ksh.)</th>
                                            <th>Type</th>
                                            <th>Description</th>
                                            <th>Customer</th>
                                            <th>Supplier</th>
                                            <th>Date</th>
                                            <th>Actions</th>
                                        </tr>
                                      </tfoot>
                                      <tbody>
                                        <?php foreach ($list as $item) {
                                          $customer = "N/A";
                                          $getcust = \App\User::where('id',$item->customerId)->first();
                                          if($getcust) {
                                            $customer = $getcust->firstName." ".$getcust->lastName;
                                          }

                                          $supplier = "N/A";
                                          $getsup = \App\User::where('id',$item->supplierId)->first();
                                          if($getsup) {
                                            $supplier = $getsup->firstName." ".$getsup->lastName;
                                          }
                                          ?>
                                          <tr>
                                            <td><?php echo $item->accountName; ?></td>
                                            <td><?php echo $item->amount; ?></td>
                                            <td><?php echo $item->type; ?></td>
                                            <td><?php echo $item->description; ?></td>
                                            <td><?php echo $customer; ?></td>
                                            <td><?php echo $supplier; ?></td>
                                            <td><?php echo $item->created_at; ?></td>
                                            <td>
                                              <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleteitem<?php echo $item->id; ?>"><i class="fa fa-trash"></i></button>
                                          </td>
                                          </tr>

                                          <!-- Modal -->
                                          <div class="modal fade text-left" id="modal-deleteitem<?php echo $item->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                              {!! Form::open(['url' => 'deleteitem']) !!}
                                            <div class="modal-content">
                                              <div class="modal-header">
                                              <h4 class="modal-title" id="myModalLabel1">Delete Payment</h4>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                              </div>
                                              <div class="modal-body">
                                              <div class="row">
                                              <div class="col-xl-6 col-lg-6 col-md-6">
                                                <input type="hidden" name="id" value="<?php echo $item->id; ?>" class="form-control" required>
                                            </div>
                                            <h5 style="margin-left:2%;">Confirm that you want to delete this payment</h5>
                                          </div>
                                              </div>
                                              <div class="modal-footer">
                                              <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                              <button type="submit" class="btn btn-primary">Delete</button>
                                              </div>
                                            </div>
                                            {!! Form::close() !!}
                                            </div>
                                          </div>

                                        <?php } ?>
                                      </tbody>
                                    </table>
                              <?php } ?>
                            </div>
                          </div>
                        </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')

  </body>

  </html>
