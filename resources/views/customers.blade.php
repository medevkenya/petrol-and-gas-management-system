<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Customers</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Customers</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All customers</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Customers</div>
                                              <div class="ibox-tools">
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addcustomer"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-addcustomer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                {!! Form::open(['url' => 'addcustomer']) !!}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New Customer</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">

                                              <div class="col-sm-12 form-group">
                                                  <label>Customer Name</label>
                                                  <input class="form-control" type="text" name="customerName" required>
                                              </div>
                                              <div class="col-sm-12 form-group">
                                                  <label>Email Address</label>
                                                  <input class="form-control" type="email" name="email">
                                              </div>
                                              <div class="col-sm-12 form-group">
                                                  <label>Contacts</label>
                                                  <input class="form-control" type="text" name="contacts">
                                              </div>
                                            </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>

                                            @if (count($errors) > 0)
                                               <div class="alert alert-danger">
                                                   <ul>
                                                       @foreach ($errors->all() as $error)
                                                       <li>{{ $error }}</li>
                                                       @endforeach
                                                   </ul>
                                               </div>
                                              @endif

                                              @if ($message = Session::get('error'))
                                                   <div class="alert alert-danger">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if ($message = Session::get('success'))
                                                   <div class="alert alert-success">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if (session('status0'))
                                              <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status0') }}
                                              </div>
                                              @endif

                                              @if (session('status1'))
                                              <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status1') }}
                                              </div>
                                              @endif

                                              <?php
                                                  $accounts = \App\Accounts::getAll();
                                                  $categories = \App\Categories::getAll();
                                              ?>

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email Address</th>
                                            <th>Contacts</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Name</th>
                                          <th>Email Address</th>
                                          <th>Contacts</th>
                                          <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $customer) {
                                      //  $amountOwed = \App\Sales::where('customerId',$customer->id)->where('type','Credit')->where('isPaid','Pending')->where('isDeleted',0)->sum('total');
                                        ?>
                                        <tr>
                                          <td><?php echo $customer->customerName; ?></td>
                                          <td><?php echo $customer->email; ?></td>
                                          <td><?php echo $customer->contacts; ?></td>

                                            <td>
                                          <a href="<?php $url = URL::to("/viewCustomer/".$customer->id); print_r($url); ?>" class="btn btn-primary"><i class="fa fa-eye"></i> View Customer</a>
                                          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addconsumption<?php echo $customer->id; ?>"><i class="fa fa-money"></i> Add Consumption</button>
                                          <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-receivepayment<?php echo $customer->id; ?>"><i class="fa fa-money"></i> Receive Payment</button>
                                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editcustomer<?php echo $customer->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deletecustomer<?php echo $customer->id; ?>"><i class="fa fa-trash"></i> Edit</button>
                                        </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-editcustomer<?php echo $customer->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'editcustomer']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Customer</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $customer->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Customer Name</label>
                                              <input class="form-control" type="text" name="customerName" value="<?php echo $customer->customerName; ?>" required>
                                          </div>
                                          <div class="col-sm-12 form-group">
                                              <label>Email Address</label>
                                              <input class="form-control" type="email" name="email" value="<?php echo $customer->email; ?>">
                                          </div>
                                          <div class="col-sm-12 form-group">
                                              <label>Contacts</label>
                                              <input class="form-control" type="text" name="contacts" value="<?php echo $customer->contacts; ?>">
                                          </div>

                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-addconsumption<?php echo $customer->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'addconsumption']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Add Customer Consumption</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $customer->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                                      <label>Category</label>
                                                      <select class="form-control" name="categoryId" onchange="categoryIdbe('<?php echo $customer->id; ?>')" id="categoryIdbe<?php echo $customer->id; ?>" required>
                                                     <option></option>
                                                     <?php foreach ($categories as $keyf) { ?>
                                                       <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->categoryName; ?></option>
                                                     <?php } ?>
                                                   </select>
                                                  </div>

                                                  <div class="col-sm-6 form-group">
                                                      <label>Product</label>
                                                      <select class="form-control" name="productId" id="productIde<?php echo $customer->id; ?>" required>
                                                   </select>
                                                  </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Amount (Ksh.)</label>
                                              <input class="form-control" type="number" step="01" name="amount" required>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Quantity</label>
                                              <input class="form-control" type="number" step="01" name="qty" required>
                                          </div>

                                            </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-receivepayment<?php echo $customer->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'receivepayment']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Receive Payment from Customer</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $customer->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Amount (Ksh.)</label>
                                              <input class="form-control" type="number" step="01" name="amount" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                            <label>Deposit to which account</label>
                                            <select class="form-control" name="accountId" required>
                                             <option value=""></option>
                                             <?php foreach ($accounts as $keyacc) { ?>
                                               <option value="<?php echo $keyacc->id; ?>"><?php echo $keyacc->accountName; ?></option>
                                             <?php } ?>
                                           </select>
                                          </div>

                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deletecustomer<?php echo $customer->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deletecustomer']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Customer</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $customer->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this customer</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>


                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-12">
                                          <div class="ibox">
                                              <div class="ibox-head">
                                                <div class="ibox-title">Statements</div>
                                              </div>
                                              <div class="ibox-body">
                                      <table class="table table-striped table-bordered table-hover" id="example-table3" cellspacing="0" width="100%">
                                      <thead>
                                          <tr>
                                              <th>Name</th>
                                              <th>Debt Brought Forward</th>
                                              <th>Current Month Consumption</th>
                                              <th>Current Month Payment (Cash/Bank)</th>
                                              <th>Net Balance</th>
                                          </tr>
                                      </thead>
                                      <tfoot>
                                          <tr>
                                            <th>Name</th>
                                            <th>Debt Brought Forward</th>
                                            <th>Current Month Consumption</th>
                                            <th>Current Month Payment (Cash/Bank)</th>
                                            <th>Net Balance</th>
                                          </tr>
                                      </tfoot>
                                      <tbody>
                                        <?php $grandbalanceBroughtForward= 0;
                                              $grandcurrentMonthDebt = 0;
                                              $granddebtPaidThisMonth = 0;
                                              $grandnetDebt = 0;
                                         foreach ($statements as $statement) {
                                           $grandbalanceBroughtForward += intval(preg_replace('/[^\d.]/', '', $statement['balanceBroughtForward']));
                                           $grandcurrentMonthDebt += intval(preg_replace('/[^\d.]/', '', $statement['currentMonthDebt']));
                                           $granddebtPaidThisMonth += intval(preg_replace('/[^\d.]/', '', $statement['debtPaidThisMonth']));

                                           if($statement['netDebt'] > 0) {
                                           $grandnetDebt += intval(preg_replace('/[^\d.]/', '', $statement['netDebt']));
                                           }
                                           else {
                                            $grandnetDebt -= intval(preg_replace('/[^\d.]/', '', $statement['netDebt']));
                                           }
                                          ?>
                                          <tr>
                                            <td><?php echo $statement['customerName']; ?></td>
                                            <td><?php echo $statement['balanceBroughtForward']; ?></td>
                                            <td>
                                              <a href="<?php $url = URL::to("/customerconsumption/".$statement['customerId']); print_r($url); ?>">
                                              <?php echo $statement['currentMonthDebt']; ?>
                                            </a>
                                            </td>
                                            <td><a href="<?php $url = URL::to("/customerpayments/".$statement['customerId']); print_r($url); ?>">
                                              <?php echo $statement['debtPaidThisMonth']; ?>
                                            </a></td>
                                            <td><?php echo $statement['netDebt']; ?></td>
                                          </tr>
                                        <?php } ?>
                                        <tfoot>
                                          <tr>
                                            <td><strong>TOTALS</strong></td>
                                            <td><strong><?php echo number_format($grandbalanceBroughtForward,2); ?></strong></td>
                                            <td><strong><?php echo number_format($grandcurrentMonthDebt,2); ?></strong></td>
                                            <td><strong><?php echo number_format($granddebtPaidThisMonth,2); ?></strong></td>
                                            <td><strong><?php echo number_format($grandnetDebt,2); ?></strong></td>
                                          </tr>
                                        </tfoot>
                                      </tbody>
                                    </table>
                                    </div>
                                    </div>
                                    </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    <!-- @include('datatablesfooter') -->
    <script>
    // $(document).ready(function() {
    //     $('#example-table3').DataTable();
    // } );
    </script>
  </body>

  </html>
