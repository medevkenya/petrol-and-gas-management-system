<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Audit Logs</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Audit Logs</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All Audit Logs</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Audit Logs</div>
                                              <div class="ibox-tools">
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addrole"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            @if (count($errors) > 0)
                                               <div class="alert alert-danger">
                                                   <ul>
                                                       @foreach ($errors->all() as $error)
                                                       <li>{{ $error }}</li>
                                                       @endforeach
                                                   </ul>
                                               </div>
                                              @endif

                                              @if ($message = Session::get('error'))
                                                   <div class="alert alert-danger">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if ($message = Session::get('success'))
                                                   <div class="alert alert-success">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if (session('status0'))
                                              <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status0') }}
                                              </div>
                                              @endif

                                              @if (session('status1'))
                                              <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status1') }}
                                              </div>
                                              @endif

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>User</th>
                                            <th>Description</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                      <tr>
                                          <th>User</th>
                                          <th>Description</th>
                                          <th>Date</th>
                                      </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $category) {
                                        ?>
                                        <tr>
                                          <td><?php echo $category->firstName; ?> <?php echo $category->lastName; ?></td>
                                            <td><?php echo $category->description; ?></td>
                                            <td><?php echo $category->created_at; ?></td>
                                        </tr>

                                      <?php } ?>
                                    </tbody>
                                </table>

                                          </div>
                                        </div>
                                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
