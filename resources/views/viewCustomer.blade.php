<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | View Customer</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Customer | <?php echo $customerDetails->customerName; ?></h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#"><i class="la la-home font-20"></i></a>
                    </li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                  <div class="col-md-12">
                      <div class="ibox">
                          <div class="ibox-head">
                              <div class="ibox-title">Name: <?php echo $customerDetails->customerName; ?> | Tel: <?php echo $customerDetails->contacts; ?> | Email: <?php echo $customerDetails->email; ?></div>
                          </div>
                          <div class="ibox-body">
                            @if (count($errors) > 0)
                                   <div class="alert alert-danger">
                                       <ul>
                                           @foreach ($errors->all() as $error)
                                           <li>{{ $error }}</li>
                                           @endforeach
                                       </ul>
                                   </div>
                                  @endif

                                  @if ($message = Session::get('error'))
                                       <div class="alert alert-danger">
                                           {{ $message }}
                                       </div>
                                  @endif

                                  @if ($message = Session::get('success'))
                                       <div class="alert alert-success">
                                           {{ $message }}
                                       </div>
                                  @endif

                                  @if (session('status0'))
                                  <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  {{ session('status0') }}
                                  </div>
                                  @endif

                                  @if (session('status1'))
                                  <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  {{ session('status1') }}
                                  </div>
                                  @endif

                          </div>
                        </div>
                      </div>

                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Consumption</div>
                            </div>
                            <div class="ibox-body">

                              <?php if(isset($consumptionlist) && !empty($consumptionlist)) { ?>
                              <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                              <thead>
                                  <tr>
                                    <th>Amount (Ksh.)</th>
                                    <th>Created On</th>
                                    <th>Actions</th>
                                  </tr>
                              </thead>
                              <tfoot>
                                <tr>
                                  <th>Amount (Ksh.)</th>
                                  <th>Created On</th>
                                  <th>Actions</th>
                                </tr>
                              </tfoot>
                              <tbody>
                                <?php foreach ($consumptionlist as $sale) {
                                  ?>
                                  <tr>
                                    <td><?php echo $sale->amount; ?></td>
                                    <td><?php echo $sale->created_at; ?></td>
                                    <td>
                                      <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-edit<?php echo $sale->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete<?php echo $sale->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                    </td>
                                  </tr>

                                  <!-- Modal -->
                                  <div class="modal fade text-left" id="modal-edit<?php echo $sale->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                      {!! Form::open(['url' => 'editconsumption']) !!}
                                    <div class="modal-content">
                                      <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel1">Edit Consumption</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                      </div>
                                      <div class="modal-body">
                                      <div class="row">
                                      <div class="col-xl-12 col-lg-12 col-md-12">
                                        <input type="hidden" name="id" value="<?php echo $sale->id; ?>" class="form-control" required>
                                    </div>

                                    <div class="col-sm-12 form-group">
                                        <label>Amount (Ksh.)</label>
                                        <input class="form-control" type="number" name="amount" value="<?php echo $sale->amount; ?>" required>
                                    </div>

                                  </div>
                                      </div>
                                      <div class="modal-footer">
                                      <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Save Changes</button>
                                      </div>
                                    </div>
                                    {!! Form::close() !!}
                                    </div>
                                  </div>


                                  <!-- Modal -->
                                  <div class="modal fade text-left" id="modal-delete<?php echo $sale->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                      {!! Form::open(['url' => 'deleteconsumption']) !!}
                                    <div class="modal-content">
                                      <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel1">Delete Consumption</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                      </div>
                                      <div class="modal-body">
                                      <div class="row">
                                      <div class="col-xl-6 col-lg-6 col-md-6">
                                        <input type="hidden" name="id" value="<?php echo $sale->id; ?>" class="form-control" required>
                                    </div>
                                    <h5 style="margin-left:2%;">Confirm that you want to delete this consumption</h5>
                                  </div>
                                      </div>
                                      <div class="modal-footer">
                                      <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Delete</button>
                                      </div>
                                    </div>
                                    {!! Form::close() !!}
                                    </div>
                                  </div>

                                <?php } ?>
                              </tbody>
                          </table>
                        <?php } ?>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12">
                            <div class="ibox">
                                <div class="ibox-head">
                                  <div class="ibox-title">Payments</div>
                                </div>
                                <div class="ibox-body">
                        <table class="table table-striped table-bordered table-hover" id="example-table2" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Amount (Ksh.)</th>
                                <th>Date</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                              <th>Name</th>
                              <th>Amount (Ksh.)</th>
                              <th>Date</th>
                              <th>Actions</th>
                            </tr>
                        </tfoot>
                        <tbody>
                          <?php foreach ($paiddebts as $debt) {
                            ?>
                            <tr>
                              <td><?php echo $debt->customerName; ?></td>
                              <td><?php echo $debt->amount; ?></td>
                              <td><?php echo $debt->created_at; ?></td>
                              <td>
                              <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editdebt<?php echo $debt->id; ?>"><i class="fa fa-edit"></i></button>
                              <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deletedebt<?php echo $debt->id; ?>"><i class="fa fa-trash"></i></button>
                            </td>
                            </tr>

                            <!-- Modal -->
                            <div class="modal fade text-left" id="modal-editdebt<?php echo $debt->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                {!! Form::open(['url' => 'editdebt']) !!}
                              <div class="modal-content">
                                <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel1">Edit Payment</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12">
                                  <input type="hidden" name="id" value="<?php echo $debt->id; ?>" class="form-control" required>
                              </div>

                              <div class="col-sm-12 form-group">
                                  <label>Amount (Ksh.)</label>
                                  <input class="form-control" type="number" name="amount" value="<?php echo intval(preg_replace('/[^\d.]/', '', $debt->amount)); ?>" required>
                              </div>

                            </div>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                                </div>
                              </div>
                              {!! Form::close() !!}
                              </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade text-left" id="modal-deletedebt<?php echo $debt->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                {!! Form::open(['url' => 'deletedebt']) !!}
                              <div class="modal-content">
                                <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel1">Delete Payment</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6">
                                  <input type="hidden" name="id" value="<?php echo $debt->id; ?>" class="form-control" required>
                              </div>
                              <h5 style="margin-left:2%;">Confirm that you want to delete this payment</h5>
                            </div>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Delete</button>
                                </div>
                              </div>
                              {!! Form::close() !!}
                              </div>
                            </div>

                          <?php } ?>
                        </tbody>
                      </table>
                      </div>
                      </div>
                      </div>


                              <div class="col-md-12">
                                  <div class="ibox">
                                      <div class="ibox-head">
                                        <div class="ibox-title">Statement</div>
                                      </div>
                                      <div class="ibox-body">
                              <table class="table table-striped table-bordered table-hover" id="example-table3" cellspacing="0" width="100%">
                              <thead>
                                  <tr>
                                      <th>Name</th>
                                      <th>Debt Brought Forward</th>
                                      <th>Current Month Consumption</th>
                                      <th>Current Month Payment (Cash/Bank)</th>
                                      <th>Net Balance</th>
                                  </tr>
                              </thead>
                              <tfoot>
                                  <tr>
                                      <th>Name</th>
                                      <th>Debt Brought Forward</th>
                                      <th>Current Month Consumption</th>
                                      <th>Current Month Payment (Cash/Bank)</th>
                                      <th>Net Balance</th>
                                  </tr>
                              </tfoot>
                              <tbody>
                                <?php foreach ($statements as $statement) {
                                  ?>
                                  <tr>
                                    <td><?php echo $statement['customerName']; ?></td>
                                    <td><?php echo $statement['balanceBroughtForward']; ?></td>
                                    <td>
                                      <a href="<?php $url = URL::to("/customersales/".$statement['customerId']); print_r($url); ?>">
                                      <?php echo $statement['currentMonthDebt']; ?>
                                    </a>
                                    </td>
                                    <td><a href="<?php $url = URL::to("/customerpayments/".$statement['customerId']); print_r($url); ?>">
                                      <?php echo $statement['debtPaidThisMonth']; ?>
                                    </a></td>
                                    <td><?php echo $statement['netDebt']; ?></td>
                                  </tr>
                                <?php } ?>
                              </tbody>
                            </table>
                            </div>
                            </div>
                            </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')

    <script>
    $(document).ready(function() {
        $('#example-table2').DataTable();
        $('#example-table3').DataTable();
    } );
    </script>

  </body>

  </html>
