<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Make Sales</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Make Sales</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#"><i class="la la-home font-20"></i></a>
                    </li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                  <div class="col-md-12">
                      <div class="ibox">
                          <div class="ibox-head">
                              <div class="ibox-title">Make Sales</div>
                          </div>
                          <div class="ibox-body">
                            @if (count($errors) > 0)
                                   <div class="alert alert-danger">
                                       <ul>
                                           @foreach ($errors->all() as $error)
                                           <li>{{ $error }}</li>
                                           @endforeach
                                       </ul>
                                   </div>
                                  @endif

                                  @if ($message = Session::get('error'))
                                       <div class="alert alert-danger">
                                           {{ $message }}
                                       </div>
                                  @endif

                                  @if ($message = Session::get('success'))
                                       <div class="alert alert-success">
                                           {{ $message }}
                                       </div>
                                  @endif

                                  @if (session('status0'))
                                  <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  {{ session('status0') }}
                                  </div>
                                  @endif

                                  @if (session('status1'))
                                  <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  {{ session('status1') }}
                                  </div>
                                  @endif


                                  <div class="row">

                                    <?php
                                    $categories = \App\Categories::getAll();
                                    //$pumpattendants = \App\Shifts::getAllPumpAttendants();
                                    $shifttypes = \App\Shifttypes::getAll();
                                    ?>

                                    <div class="col-sm-3 form-group">
                                        <label>Pump/Attendant</label>
                                        <select class="form-control" name="shiftId" id="shiftId" required>
                                       <option></option>
                                       <?php foreach ($pumpattendants as $keyfcus) { ?>
                                         <option value="<?php echo $keyfcus->id; ?>"><?php echo $keyfcus->firstName; ?> <?php echo $keyfcus->lastName; ?> (<?php echo $keyfcus->pumpName; ?>) (<?php echo $keyfcus->shifttypeName; ?>)</option>
                                       <?php } ?>
                                     </select>
                                    </div>

                                    <div class="col-sm-2 form-group">
                                        <label>Category</label>
                                        <select class="form-control" name="categoryId" id="categoryId" required>
                                       <option></option>
                                       <?php foreach ($categories as $keyf) { ?>
                                         <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->categoryName; ?></option>
                                       <?php } ?>
                                     </select>
                                    </div>

                                    <div class="col-sm-3 form-group">
                                        <label>Product</label> <strong>(Unit Cost: Ksh. <span id="sellingPrice">_</span>)</strong>
                                        <select class="form-control" name="productId" id="productId" required>
                                     </select>
                                    </div>

                                  <div class="form-group col-md-2">
                                    <label>Cash Quantity</label>
                                    <input type="number" class="form-control" name="quantity" value="0" min="0" id="cashQuantity" required>
                                  </div>

                                  <div class="form-group col-md-2">
                                    <label>Credit Quantity</label>
                                    <input type="number" class="form-control" name="quantity" value="0" min="0" id="creditQuantity" required>
                                  </div>

                                </div>
                                <div class="row">

                                  <div class="form-group col-md-2">
                                    <label>Discount</label>
                                    <input type="number" class="form-control" name="discount" value="0" min="0" id="saleDiscount" required>
                                  </div>

                                  <div class="form-group col-md-2" style="margin-top:2.2%;">
                                    <button type="button" id="completesale" class="btn btn-block btn-success">SUBMIT</button>
                                  </div>

                                  <div class="form-group col-md-3" style="margin-top:2.2%;">
                                    <h3>Total: Ksh. <span id="totalCost">0</span></h3>
                                  </div>
                                  <div class="form-group col-md-1">
                                  <div class="ajax-loader">
                              			<img src="{{ URL::to('/') }}/public/images/loading.gif" class="img-responsive" />
                              		</div>
                                </div>

                                  </div>


                          </div>
                        </div>
                      </div>

                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Sales List <?php if(isset($fromdate)) { echo "From: ".$fromdate." To: ".$todate; } ?></div>
                                <?php if(isset($fromdate) && !empty($fromdate)) { ?>
                                <div class="ibox-tools">
                                    <a href="<?php $url = URL::to("/exportfiltersaleexcel/".$fromdate."/".$todate."/"); print_r($url); ?>">
                                      <button type="button" class="btn btn-warning"><i class="fa fa-file-excel-o"></i> Generate Excel
                                      </button>
                                      </a>
                                      <!-- <a href="<?php //$url = URL::to("/exportfiltersalepdf/".$fromdate."/".$todate."/"); print_r($url); ?>">
                                        <button type="button" class="btn btn-warning"><i class="fa fa-file-pdf-o"></i> Generate PDF
                                        </button>
                                        </a> -->
                                </div>
                              <?php } ?>
                            </div>
                            <div class="ibox-body">

                                    {!! Form::open(['url' => 'postfiltersalereport']) !!}
                                    {{ csrf_field() }}
                                    <div class="row">

                                    <div class="form-group col-md-3">
                                      <label>From Date</label>
                                      <input type="date" class="form-control" name="fromdate" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                      <label>To Date</label>
                                      <input type="date" class="form-control" name="todate" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                      <label>.</label>
                                      <button type="submit" class="btn btn-block btn-primary">Search</button>
                                    </div>
                                    </div>
                                    {!! Form::close() !!}

                                    <?php if(isset($list) && !empty($list)) { ?>
                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                          <th>Shift/Attendant</th>
                                          <th>Product</th>
                                          <th>Qty (cash/credit)</th>
                                          <th>Unit Cost (Ksh.)</th>
                                          <th>Discount (Ksh.)</th>
                                          <th>Total Sales</th>
                                          <th>Created On</th>
                                          <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php foreach ($list as $sale) {
                                        $shiftdetails = \App\Shifts::getShiftDetails($sale->shiftId);
                                        ?>
                                        <tr>
                                          <td><?php echo $shiftdetails->firstName; ?> <?php echo $shiftdetails->lastName; ?> (<?php echo $shiftdetails->pumpName; ?>) (<?php echo $shiftdetails->shifttypeName; ?>)</td>
                                          <td><?php echo $sale->productName; ?></td>
                                          <td><?php echo $sale->cashQuantity." ".$sale->unit; ?> / <?php echo $sale->creditQuantity." ".$sale->unit; ?></td>
                                          <td><?php echo $sale->unitCost; ?></td>
                                          <td><?php echo $sale->discount; ?></td>
                                          <td><?php echo $sale->total; ?></td>
                                          <td><?php echo $sale->created_at; ?></td>
                                          <td>
                                            <!-- <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-edit<?php //echo $sale->id; ?>"><i class="fa fa-edit"></i> Edit</button> -->
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete<?php echo $sale->id; ?>"><i class="fa fa-trash"></i></button>
                                          </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-edit<?php echo $sale->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'editsale']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Sale</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $sale->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Quantity</label>
                                              <input class="form-control" type="number" name="meterSale" value="<?php echo $sale->quantity; ?>" required>
                                            </div>

                                            </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-delete<?php echo $sale->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deletesale']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Sale</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $sale->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this sale</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>
                              <?php } ?>

                              <!-- Modal -->
                              <div class="modal fade" id="lowStockAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <!-- <div class="modal-header bg-success">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title">Completed</h4>
                                    </div> -->
                                    <div class="modal-body">
                                      <p>Not enough stock for this product</p>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <!-- Modal -->
                              <div class="modal fade" id="successmodal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <!-- <div class="modal-header bg-success">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title">Completed</h4>
                                    </div> -->
                                    <div class="modal-body">
                                      <p>New sale submited successfully</p>
                                    </div>
                                    <div class="modal-footer">
                                      <a href="{{URL::to('/makesales')}}" class="btn btn-default">Ok</a>
                                      <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button> -->
                                      <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <!-- Modal -->
                              <div class="modal fade" id="failmodal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                   <div class="modal-header bg-warning">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title">Failed</h4>
                                    </div>
                                    <div class="modal-body">
                                      <p id="failmessage"></p>
                                    </div>
                                    <div class="modal-footer">
                                      <!-- <a href="{{URL::to('/makesales')}}" class="btn btn-default">Ok</a> -->
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                      <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                    </div>
                                  </div>
                                </div>
                              </div>


                              <!-- Modal -->
                              <div class="modal fade" id="invalidsale" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <!-- <div class="modal-header bg-success">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title">Completed</h4>
                                    </div> -->
                                    <div class="modal-body">
                                      <p>Invalid sale. Check the fields and try again</p>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                      <!-- <a href="{{URL::to('/makesales')}}" class="btn btn-default">Close</a> -->
                                      <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                    </div>
                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')

    <script>

    // $('#successmodal').modal({
    //     backdrop: 'static',
    //     keyboard: false
    // })

    $(document).ready(function(){
        //$("#discount").on("input", function(){
    			$("#cashQuantity").on("keyup", function(event) {
            // Print entered value in a div box
            var quantity = $(this).val();
    				var productId = document.getElementById("productId").value;
    				var totalCost = document.getElementById("totalCost").innerHTML;
            var sellingPrice = document.getElementById("sellingPrice").innerHTML;
    				var discount = document.getElementById("saleDiscount").value;
            var creditQuantity = document.getElementById("creditQuantity").value;

    				//if(subtotal > discount) {
    				//var newsubtotal = +subtotal - +discount;

            var costFromCredit = +creditQuantity * +sellingPrice;

    				var newtotal = +quantity * +sellingPrice;
            var gnewtotal = newtotal + costFromCredit;
            var finalnewtotal = gnewtotal - discount;

            console.log("CHANGE QUANTITY productId="+productId+"--quantity="+quantity+"---sellingPrice="+sellingPrice+"----discount="+discount+"--totalCost="+newtotal+"---");
    				$("#totalCost").html(parseInt(finalnewtotal));
    				//$("#subTotal").html(parseInt(newsubtotal));
    			// }
    			// else {
    			// 	alert("Invalid discount");
    			// 	$("#discount").html("0");
    			// }
        });

        $("#creditQuantity").on("keyup", function(event) {
          // Print entered value in a div box
          var quantity = $(this).val();
          var productId = document.getElementById("productId").value;
          var totalCost = document.getElementById("totalCost").innerHTML;
          var sellingPrice = document.getElementById("sellingPrice").innerHTML;
          var discount = document.getElementById("saleDiscount").value;
          var cashQuantity = document.getElementById("cashQuantity").value;

          //if(subtotal > discount) {
          //var newsubtotal = +subtotal - +discount;

          var costFromCredit = +cashQuantity * +sellingPrice;

          var newtotal = +quantity * +sellingPrice;
          var gnewtotal = newtotal + costFromCredit;
          var finalnewtotal = gnewtotal - discount;

          console.log("CHANGE QUANTITY productId="+productId+"--quantity="+quantity+"---sellingPrice="+sellingPrice+"----discount="+discount+"--totalCost="+newtotal+"---");
          $("#totalCost").html(parseInt(finalnewtotal));
          //$("#subTotal").html(parseInt(newsubtotal));
        // }
        // else {
        // 	alert("Invalid discount");
        // 	$("#discount").html("0");
        // }
      });

        $("#saleDiscount").on("keyup", function(event) {
          // Print entered value in a div box
          var discount = $(this).val();
          var productId = document.getElementById("productId").value;
          var totalCost = document.getElementById("totalCost").innerHTML;
          var sellingPrice = document.getElementById("sellingPrice").innerHTML;
          var cashQuantity = document.getElementById("cashQuantity").value;
          var creditQuantity = document.getElementById("creditQuantity").value;

          var quantity = +creditQuantity + +cashQuantity;

          if(quantity > 0) {
          //var newsubtotal = +subtotal - +discount;
          var oldtotal = +quantity * +sellingPrice;
          var newtotal = +oldtotal - +discount;

          if(newtotal > 0) {

          $("#totalCost").html(parseInt(newtotal));
          //$("#subTotal").html(parseInt(newsubtotal));
        }
         }
        // else {
        // 	alert("Invalid discount");
        // 	$("#discount").html("0");
        // }
      });
    });

    $(document).ready(function(){
    		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    		$("#completesale").click(function(){

          var categoryId = document.getElementById("categoryId").value;
          var shiftId = document.getElementById("shiftId").value;
          var productId = document.getElementById("productId").value;
          var totalCost = document.getElementById("totalCost").innerHTML;
          var sellingPrice = document.getElementById("sellingPrice").innerHTML;
          var cashQuantity = document.getElementById("cashQuantity").value;
          var creditQuantity = document.getElementById("creditQuantity").value;
          var discount = document.getElementById("saleDiscount").value;

          if(shiftId=="") {
            document.getElementById("failmessage").innerHTML = "Select Pump / Attendant";
            $('#failmodal').modal('show');
          }

          else if(creditQuantity=="") {
            document.getElementById("failmessage").innerHTML = "Enter quantity sold in credit";
            $('#failmodal').modal('show');
          }
          else if(cashQuantity=="") {
            document.getElementById("failmessage").innerHTML = "Enter quantity sold in cash";
            $('#failmodal').modal('show');
          }
          else {

          var oldtotal = +cashQuantity * +sellingPrice;
          var newtotal = +oldtotal - +discount;

          if(cashQuantity >= 0 && newtotal >= 0) {

    			$('.ajax-loader').css("visibility", "visible");

    			$.ajax({
    					url: "{{ route('completesale') }}?discount=" + discount+"&creditQuantity="+creditQuantity+"&total="+totalCost+"&shiftId="+shiftId+"&cashQuantity="+cashQuantity+"&categoryId="+categoryId+"&productId="+productId+"&sellingPrice="+sellingPrice,
    					method: 'GET',
    					success: function(data) {
    							console.log("completesale html--"+JSON.stringify(data));
    							$('.ajax-loader').css("visibility", "hidden");

                  if(data.status==1)
                  {
    							  $('#successmodal').modal('show');
                  }
                  else
                  {
                    //alert(data.message);
                    document.getElementById("failmessage").innerHTML = data.error;
                    $('#failmodal').modal('show');
                  }

    					},
    					complete: function(){
    						$('.ajax-loader').css("visibility", "hidden");
    					}
    			});

        }
        else
        {
          $('#invalidsale').modal('show');
        }

      }

    		});
    });
  </script>

  </body>

  </html>
