<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | My Profile</title>
    @include('headerlink')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Profile</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">Profile</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">
                <div class="row">
                    <div class="col-lg-3 col-md-4">
                        <div class="ibox">
                            <div class="ibox-body text-center">
                                <div class="m-t-20">
                                    <img class="img-circle" style="width:150px;height:150px;" src="{{ URL::to('/') }}/profiles/<?php echo Auth::user()->profilePic; ?>" />
                                </div>
                                <h5 class="font-strong m-b-10 m-t-10">{{Auth::user()->firstName}} {{Auth::user()->lastName}}</h5>
                                <div class="m-b-20 text-muted">{{Auth::user()->email}}</div>
                                <div class="profile-social m-b-20">
                                    <!-- <a href="javascript:;"><i class="fa fa-twitter"></i></a>
                                    <a href="javascript:;"><i class="fa fa-facebook"></i></a>
                                    <a href="javascript:;"><i class="fa fa-pinterest"></i></a>
                                    <a href="javascript:;"><i class="fa fa-dribbble"></i></a> -->
                                </div>
                                <div>
                                    <button class="btn btn-default btn-rounded m-b-5" data-toggle="modal" data-target="#modal-photo">Update Profile Picture</button>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade text-left" id="modal-photo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <form action="{{ route('UpdatePic') }}" class="p-4 border rounded" method="post" enctype="multipart/form-data">
                          {{ csrf_field() }}
                          <div class="modal-content">
                            <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel1">Update Profile Photo</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                            <div class="row">

                            <div class="col-sm-12 form-group">
                                <label>Upload Photo</label>
                                <input class="form-control" type="file" name="profilePic" required>
                            </div>

                            </div>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Upload</button>
                            </div>
                          </div>
                          {!! Form::close() !!}
                          </div>
                        </div>

                        <div class="ibox">
                            <!-- <div class="ibox-body">
                                <div class="row text-center m-b-20">
                                    <div class="col-4">
                                        <div class="font-24 profile-stat-count">140</div>
                                        <div class="text-muted">Followers</div>
                                    </div>
                                    <div class="col-4">
                                        <div class="font-24 profile-stat-count">$780</div>
                                        <div class="text-muted">Sales</div>
                                    </div>
                                    <div class="col-4">
                                        <div class="font-24 profile-stat-count">15</div>
                                        <div class="text-muted">Projects</div>
                                    </div>
                                </div>
                                <p class="text-center">Lorem Ipsum is simply dummy text of the printing and industry. Lorem Ipsum has been</p>
                            </div> -->
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-8">
                        <div class="ibox">
                            <div class="ibox-body">
                                <ul class="nav nav-tabs tabs-line">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#tab-1" data-toggle="tab"><i class="ti-settings"></i> Profile Details</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#tab-2" data-toggle="tab"><i class="ti-lock"></i> Change Password</a>
                                    </li>
                                    <!-- <li class="nav-item">
                                        <a class="n av-link" href="#tab-3" data-toggle="tab"><i class="ti-announcement"></i> Notifications</a>
                                    </li> -->
                                    <li class="nav-item">
                                        <a class="nav-link" href="#tab-4" data-toggle="tab"><i class="ti-user"></i> Company Settings</a>
                                    </li>
                                </ul>
                                <div class="tab-content">

                                      @if (count($errors) > 0)
                                         <div class="alert alert-danger">
                                             <ul>
                                                 @foreach ($errors->all() as $error)
                                                 <li>{{ $error }}</li>
                                                 @endforeach
                                             </ul>
                                         </div>
                                        @endif

                                        @if ($message = Session::get('error'))
                                             <div class="alert alert-danger">
                                                 {{ $message }}
                                             </div>
                                        @endif

                                        @if ($message = Session::get('success'))
                                             <div class="alert alert-success">
                                                 {{ $message }}
                                             </div>
                                        @endif

                                        @if (session('status0'))
                                        <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{ session('status0') }}
                                        </div>
                                        @endif

                                        @if (session('status1'))
                                        <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{ session('status1') }}
                                        </div>
                                        @endif

                                    <div class="tab-pane fade show active" id="tab-1">
                                        {!! Form::open(['url' => 'updateProfile']) !!}
                                        {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label>First Name</label>
                                                    <input class="form-control" name="firstName" value="<?php echo $details->firstName; ?>" type="text" placeholder="First Name">
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <label>Last Name</label>
                                                    <input class="form-control" name="lastName" value="<?php echo $details->lastName; ?>" type="text" placeholder="First Name">
                                                </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Email</label>
                                                <input class="form-control" type="email" name="email" value="<?php echo $details->email; ?>" placeholder="Email address">
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Mobile No.</label>
                                                <input class="form-control" type="number" name="mobileNo" value="<?php echo $details->mobileNo; ?>" placeholder="Mobile No.">
                                            </div>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="submit">Save Changes</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="tab-2">
                                        {!! Form::open(['url' => 'doChangePass']) !!}
                                        {{ csrf_field() }}
                                            <div class="row">
                                            <div class="col-sm-4 form-group">
                                                <label>Current Password</label>
                                                <input class="form-control" type="password" name="oldpassword" placeholder="Password">
                                            </div>
                                            <div class="col-sm-4 form-group">
                                                <label>New Password</label>
                                                <input class="form-control" type="password" name="password" placeholder="Password">
                                            </div>
                                            <div class="col-sm-4 form-group">
                                                <label>Confirm New Password</label>
                                                <input class="form-control" type="password" name="cpassword" placeholder="Password">
                                            </div>
                                          </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="submit">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="tab-3">
                                        <h5 class="text-info m-b-20 m-t-20"><i class="fa fa-bullhorn"></i> Latest Notifications</h5>
                                        <ul class="media-list media-list-divider m-0">
                                          <?php $notifications = \App\Notifications::getLatest();
                                          foreach ($notifications as $nt) { ?>
                                            <li class="media">
                                                <div class="media-img"><i class="ti-user font-18 text-muted"></i></div>
                                                <div class="media-body">
                                                    <div class="media-heading"><?php echo $nt->firstName; ?> <?php echo $nt->lastName; ?> <small class="float-right text-muted"><?php echo $nt->created_at->diffForHumans(); ?></small></div>
                                                    <div class="font-13"><?php echo $nt->message; ?></div>
                                                </div>
                                            </li>
                                          <?php } ?>
                                        </ul>
                                    </div>
                                    <?php $settings = \App\Settings::getDetails(); ?>
                                    <div class="tab-pane fade" id="tab-4">
                                        <form action="{{ route('updateCompanyPhoto') }}" class="p-4 border rounded" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                            <div class="row">
                                            <div class="col-sm-3 form-group">
                                              <img class="img-circle" style="width:80px;height:80px;" src="{{ URL::to('/') }}/profiles/<?php echo $settings->companyPhoto; ?>" />
                                            </div>
                                            <div class="col-sm-4 form-group">
                                                <input class="form-control" type="file" name="companyPhoto" required>
                                            </div>
                                          </div>
                                            <div class="col-sm-3 form-group">
                                                <button class="btn btn-primary" type="submit">Update</button>
                                            </div>
                                        </form>
                                        {!! Form::open(['url' => 'updateCompanyDetails']) !!}
                                        {{ csrf_field() }}
                                            <div class="row">
                                            <div class="col-sm-6 form-group">
                                                <label>Company Name</label>
                                                <input class="form-control" value="<?php echo $settings->companyName; ?>" type="text" name="companyName" placeholder="Name" required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Company Location</label>
                                                <input class="form-control" value="<?php echo $settings->companyLocation; ?>" type="text" name="companyLocation" placeholder="Location" required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Company Email</label>
                                                <input class="form-control" value="<?php echo $settings->companyEmail; ?>" type="email" name="companyEmail" placeholder="Email" required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Company Telephone</label>
                                                <input class="form-control" value="<?php echo $settings->companyTelephone; ?>" type="number" name="companyTelephone" placeholder="Telephone" required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Default Tax (%)</label>
                                                <input class="form-control" value="<?php echo $settings->tax; ?>" type="number" name="tax" placeholder="Tax" required>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                                <label>Default Discount (Ksh)</label>
                                                <input class="form-control" value="<?php echo $settings->discount; ?>" type="number" name="discount" placeholder="Discount" required>
                                            </div>
                                          </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="submit">Save Changes</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
  </body>

  </html>
