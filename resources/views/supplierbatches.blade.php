<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Supplier</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Batches | <?php echo $supplierDetails->supplierName; ?></h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">Batches for <?php echo date('F'); ?></li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Name: <?php echo $supplierDetails->supplierName; ?> | Tel: <?php echo $supplierDetails->contacts; ?> | Email: <?php echo $supplierDetails->email; ?></div>
                                <?php if(isset($list) && !empty($list)) { ?>
                                <div class="ibox-tools">
                                    <!-- <a href="<?php //$url = URL::to("/exportfilterstockexcel/".$fromdate."/".$todate."/"); print_r($url); ?>">
                                      <button type="button" class="btn btn-warning"><i class="fa fa-file-excel-o"></i> Generate Excel
                                      </button>
                                      </a> -->
                                      <!-- <a href="<?php //$url = URL::to("/exportfilterstockpdf/".$fromdate."/".$todate."/"); print_r($url); ?>">
                                        <button type="button" class="btn btn-warning"><i class="fa fa-file-pdf-o"></i> Generate PDF
                                        </button>
                                        </a> -->
                                </div>
                              <?php } ?>
                            </div>
                            <div class="ibox-body">
                              @if (count($errors) > 0)
                                     <div class="alert alert-danger">
                                         <ul>
                                             @foreach ($errors->all() as $error)
                                             <li>{{ $error }}</li>
                                             @endforeach
                                         </ul>
                                     </div>
                                    @endif

                                    @if ($message = Session::get('error'))
                                         <div class="alert alert-danger">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if ($message = Session::get('success'))
                                         <div class="alert alert-success">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if (session('status0'))
                                    <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status0') }}
                                    </div>
                                    @endif

                                    @if (session('status1'))
                                    <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status1') }}
                                    </div>
                                    @endif

                                    <?php if(isset($list) && !empty($list)) { ?>
                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>

                                            <th>Product</th>
                                            <th>Purchase Price</th>
                                            <th>Quantity</th>
                                            <th>Value (Ksh.)</th>
                                            <th>Status</th>
                                            <th>Created On</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Product</th>
                                          <th>Purchase Price</th>
                                          <th>Quantity</th>
                                          <th>Value (Ksh.)</th>
                                          <th>Status</th>
                                          <th>Created On</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $batch) {
                                        $value = $batch->quantity * $batch->purchasePrice
                                        ?>
                                        <tr>
                                          <td><?php echo $batch->productName." ".$batch->unit; ?></td>
                                          <td><?php echo $batch->purchasePrice; ?></td>
                                          <td><?php echo $batch->quantity; ?></td>
                                          <td><?php echo number_format($value,2); ?></td>
                                          <td><?php echo $batch->status; ?></td>
                                          <td><?php echo $batch->created_at; ?></td>
                                        </tr>
                                      <?php } ?>
                                    </tbody>
                                </table>
                              <?php } ?>

                            </div>
                          </div>
                        </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
    <script>
    $(document).ready(function() {
        $('#example-table2').DataTable();
        $('#example-table3').DataTable();
    } );
    </script>
  </body>

  </html>
