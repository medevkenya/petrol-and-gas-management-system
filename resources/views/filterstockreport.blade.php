<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Stock Report</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Stock Report</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">Generate Stock report <?php if(isset($fromdate)) { echo "From: ".$fromdate." To: ".$todate; } ?></li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Stock report</div>
                                <?php if(isset($list) && !empty($list)) { ?>
                                <div class="ibox-tools">
                                    <!-- <a href="<?php //$url = URL::to("/exportfilterstockexcel/".$fromdate."/".$todate."/"); print_r($url); ?>">
                                      <button type="button" class="btn btn-warning"><i class="fa fa-file-excel-o"></i> Generate Excel
                                      </button>
                                      </a> -->
                                      <!--<a href="<?php //$url = URL::to("/exportfilterstockpdf/".$fromdate."/".$todate."/"); print_r($url); ?>">-->
                                      <!--  <button type="button" class="btn btn-warning"><i class="fa fa-file-pdf-o"></i> Generate PDF-->
                                      <!--  </button>-->
                                      <!--  </a>-->
                                </div>
                              <?php } ?>
                            </div>
                            <div class="ibox-body">
                              @if (count($errors) > 0)
                                     <div class="alert alert-danger">
                                         <ul>
                                             @foreach ($errors->all() as $error)
                                             <li>{{ $error }}</li>
                                             @endforeach
                                         </ul>
                                     </div>
                                    @endif

                                    @if ($message = Session::get('error'))
                                         <div class="alert alert-danger">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if ($message = Session::get('success'))
                                         <div class="alert alert-success">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if (session('status0'))
                                    <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status0') }}
                                    </div>
                                    @endif

                                    @if (session('status1'))
                                    <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status1') }}
                                    </div>
                                    @endif

                                    <?php
                                    $categories = \App\Categories::getAll();
                                    $suppliers = \App\Suppliers::getAll();
                                    $tanks = \App\Tanks::getAll();
                                    ?>

                                    {!! Form::open(['url' => 'postfilterstockreport']) !!}
                                    {{ csrf_field() }}
                                    <div class="row">

                                    <div class="form-group col-md-3">
                                      <label>From Date</label>
                                      <input type="date" class="form-control" name="fromdate" value="<?php echo $fromdate; ?>" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                      <label>To Date</label>
                                      <input type="date" class="form-control" name="todate" value="<?php echo $todate; ?>" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                      <label>.</label>
                                      <button type="submit" class="btn btn-block btn-primary">Search</button>
                                    </div>
                                    </div>
                                    {!! Form::close() !!}

                                    <?php if(isset($list) && !empty($list)) { ?>
                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Supplier</th>
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>Purchase Price</th>
                                            <th>Value (Ksh.)</th>
                                            <th>Created On</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php foreach ($list as $batch) {
                                        $value = $batch->quantity * $batch->purchasePrice
                                        ?>
                                        <tr>
                                          <td><?php echo $batch->supplierName; ?></td>
                                          <td><?php echo $batch->productName." ".$batch->unit; ?></td>
                                          <td><?php echo $batch->quantity; ?></td>
                                          <td><?php echo $batch->purchasePrice; ?></td>
                                          <td><?php echo number_format($value,2); ?></td>
                                          <td><?php echo $batch->created_at; ?></td>
                                          <td>
                                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editrole<?php echo $batch->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleterole<?php echo $batch->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                        </td>
                                        </tr>


                                                                                <!-- Modal -->
                                                                                <div class="modal fade text-left" id="modal-editrole<?php echo $batch->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                                                  <div class="modal-dialog" role="document">
                                                                                    {!! Form::open(['url' => 'editbatch']) !!}
                                                                                  <div class="modal-content">
                                                                                    <div class="modal-header">
                                                                                    <h4 class="modal-title" id="myModalLabel1">Edit Batch</h4>
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                      <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                    </div>
                                                                                    <div class="modal-body">
                                                                                    <div class="row">
                                                                                    <div class="col-xl-12 col-lg-12 col-md-12">
                                                                                      <input type="hidden" name="id" value="<?php echo $batch->id; ?>" class="form-control" required>
                                                                                  </div>
                                                                                  <div class="col-sm-12 form-group">
                                                                                      <label>Supplier</label>
                                                                                      <select class="form-control" name="supplierId" required>
                                                                                     <option value="<?php echo $batch->supplierId; ?>"><?php echo $batch->supplierName; ?></option>
                                                                                     <?php foreach ($suppliers as $keyfs) { ?>
                                                                                       <option value="<?php echo $keyfs->id; ?>"><?php echo $keyfs->supplierName; ?></option>
                                                                                     <?php } ?>
                                                                                   </select>
                                                                                  </div>
                                                                                  <div class="col-sm-12 form-group">
                                                                                    <label>Category</label>
                                                                                    <select class="form-control" name="categoryId" onchange="categoryIdbe('<?php echo $batch->id; ?>')" id="categoryIdbe<?php echo $batch->id; ?>" required>
                                                                                     <option value="<?php echo $batch->categoryId; ?>"><?php echo $batch->categoryName; ?></option>
                                                                                     <?php foreach ($categories as $keyf) { ?>
                                                                                       <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->categoryName; ?></option>
                                                                                     <?php } ?>
                                                                                   </select>
                                                                                  </div>
                                                                                  <div class="col-sm-12 form-group">
                                                                                    <label>Product</label>
                                                                                    <select class="form-control" name="productId" id="productIde<?php echo $batch->id; ?>" required>
                                                                                      <option value="<?php echo $batch->productId; ?>"><?php echo $batch->productName; ?></option>
                                                                                   </select>
                                                                                  </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                  <div class="col-sm-6 form-group">
                                                                                      <label>Quantity</label>
                                                                                      <input class="form-control" type="number" name="quantity" value="<?php echo $batch->quantity; ?>" required>
                                                                                  </div>
                                                                                  <div class="col-sm-6 form-group">
                                                                                      <label>Type</label>
                                                                                      <select class="form-control" name="type" required>
                                                                                        <option value="<?php echo $batch->type; ?>"><?php echo $batch->type; ?></option>
                                                                                       <option value="Cash">Cash</option>
                                                                                       <option value="Credit">Credit</option>
                                                                                   </select>
                                                                                  </div>
                                                                                </div>
                                                                                <div class="row">

                                                                                  <div class="col-sm-6 form-group">
                                                                                      <label>Purchase Price</label>
                                                                                      <input class="form-control" type="number" name="purchasePrice" value="<?php echo $batch->purchasePrice; ?>" required>
                                                                                  </div>

                                                                                  <?php if($batch->productId == 1 || $batch->productId == 2) { ?>
                                                                                  <div class="col-sm-12 form-group" id="tankIdEdit" style="display:none;">
                                                                                      <label>Tank</label>
                                                                                      <select class="form-control" name="tankId" required>
                                                                                     <option value="<?php echo $batch->tankId; ?>"><?php echo $batch->tankName; ?></option>
                                                                                     <?php foreach ($tanks as $key_tank) { ?>
                                                                                       <option value="<?php echo $key_tank->id; ?>"><?php echo $key_tank->tankName; ?></option>
                                                                                     <?php } ?>
                                                                                   </select>
                                                                                  </div>
                                                                                  <?php } ?>

                                                                                </div>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                    <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                                                    <button type="submit" class="btn btn-primary">Save Changes</button>
                                                                                    </div>
                                                                                  </div>
                                                                                  {!! Form::close() !!}
                                                                                  </div>
                                                                                </div>

                                                                                <!-- Modal -->
                                                                                <div class="modal fade text-left" id="modal-deleterole<?php echo $batch->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                                                  <div class="modal-dialog" role="document">
                                                                                    {!! Form::open(['url' => 'deletebatch']) !!}
                                                                                  <div class="modal-content">
                                                                                    <div class="modal-header">
                                                                                    <h4 class="modal-title" id="myModalLabel1">Delete Batch</h4>
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                      <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                    </div>
                                                                                    <div class="modal-body">
                                                                                    <div class="row">
                                                                                    <div class="col-xl-6 col-lg-6 col-md-6">
                                                                                      <input type="hidden" name="id" value="<?php echo $batch->id; ?>" class="form-control" required>
                                                                                  </div>
                                                                                  <h5 style="margin-left:2%;">Confirm that you want to delete this batch</h5>
                                                                                </div>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                    <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                                                    <button type="submit" class="btn btn-primary">Delete</button>
                                                                                    </div>
                                                                                  </div>
                                                                                  {!! Form::close() !!}
                                                                                  </div>
                                                                                </div>

                                      <?php } ?>
                                    </tbody>
                                </table>
                              <?php } ?>

                            </div>
                          </div>
                        </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
