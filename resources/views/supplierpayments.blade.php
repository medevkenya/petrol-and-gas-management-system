<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Supplier</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Payments | <?php echo $supplierDetails->supplierName; ?></h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">Payments for <?php echo date('F'); ?></li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Name: <?php echo $supplierDetails->supplierName; ?> | Tel: <?php echo $supplierDetails->contacts; ?> | Email: <?php echo $supplierDetails->email; ?></div>
                                <?php if(isset($list) && !empty($list)) { ?>
                                <div class="ibox-tools">
                                    <!-- <a href="<?php //$url = URL::to("/exportfilterstockexcel/".$fromdate."/".$todate."/"); print_r($url); ?>">
                                      <button type="button" class="btn btn-warning"><i class="fa fa-file-excel-o"></i> Generate Excel
                                      </button>
                                      </a> -->
                                      <!-- <a href="<?php //$url = URL::to("/exportfilterstockpdf/".$fromdate."/".$todate."/"); print_r($url); ?>">
                                        <button type="button" class="btn btn-warning"><i class="fa fa-file-pdf-o"></i> Generate PDF
                                        </button>
                                        </a> -->
                                </div>
                              <?php } ?>
                            </div>
                            <div class="ibox-body">
                              @if (count($errors) > 0)
                                     <div class="alert alert-danger">
                                         <ul>
                                             @foreach ($errors->all() as $error)
                                             <li>{{ $error }}</li>
                                             @endforeach
                                         </ul>
                                     </div>
                                    @endif

                                    @if ($message = Session::get('error'))
                                         <div class="alert alert-danger">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if ($message = Session::get('success'))
                                         <div class="alert alert-success">
                                             {{ $message }}
                                         </div>
                                    @endif

                                    @if (session('status0'))
                                    <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status0') }}
                                    </div>
                                    @endif

                                    @if (session('status1'))
                                    <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ session('status1') }}
                                    </div>
                                    @endif


                        <table class="table table-striped table-bordered table-hover" id="example-table2" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Amount (Ksh.)</th>
                                <th>Date</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                              <th>Name</th>
                              <th>Amount (Ksh.)</th>
                              <th>Date</th>
                              <th>Actions</th>
                            </tr>
                        </tfoot>
                        <tbody>
                          <?php foreach ($paidcredits as $credit) {
                            ?>
                            <tr>
                              <td><?php echo $credit->supplierName; ?></td>
                              <td><?php echo $credit->amount; ?></td>
                              <td><?php echo $credit->created_at; ?></td>
                              <td>
                              <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editcredit<?php echo $credit->id; ?>"><i class="fa fa-edit"></i></button>
                              <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deletecredit<?php echo $credit->id; ?>"><i class="fa fa-trash"></i></button>
                            </td>
                            </tr>

                            <!-- Modal -->
                            <div class="modal fade text-left" id="modal-editcredit<?php echo $credit->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                {!! Form::open(['url' => 'editcredit']) !!}
                              <div class="modal-content">
                                <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel1">Edit Payment</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12">
                                  <input type="hidden" name="id" value="<?php echo $credit->id; ?>" class="form-control" required>
                              </div>

                              <div class="col-sm-12 form-group">
                                  <label>Amount (Ksh.)</label>
                                  <input class="form-control" type="number" name="amount" value="<?php echo intval(preg_replace('/[^\d.]/', '', $credit->amount)); ?>" required>
                              </div>

                            </div>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                                </div>
                              </div>
                              {!! Form::close() !!}
                              </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade text-left" id="modal-deletecredit<?php echo $credit->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                {!! Form::open(['url' => 'deletecredit']) !!}
                              <div class="modal-content">
                                <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel1">Delete Payment</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6">
                                  <input type="hidden" name="id" value="<?php echo $credit->id; ?>" class="form-control" required>
                              </div>
                              <h5 style="margin-left:2%;">Confirm that you want to delete this payment</h5>
                            </div>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Delete</button>
                                </div>
                              </div>
                              {!! Form::close() !!}
                              </div>
                            </div>

                          <?php } ?>
                        </tbody>
                      </table>
                      </div>
                      </div>
                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
    <script>
    $(document).ready(function() {
        $('#example-table2').DataTable();
    } );
    </script>
  </body>

  </html>
