<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Users</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Users</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All Users</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Staff</div>
                                              <div class="ibox-tools">
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-adduser"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-adduser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                {!! Form::open(['url' => 'adduser']) !!}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New User</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">

                                                  <div class="col-sm-6 form-group">
                                                      <label>Role</label>
                                                      <select class="form-control" name="roleId" required>
                                                        <option value=""></option>
                                                        <?php foreach ($roles as $keyf) { ?>
                                                          <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->roleName; ?></option>
                                                        <?php } ?>
                                                      </select>
                                                  </div>

                                              <div class="col-sm-6 form-group">
                                                  <label>First Name</label>
                                                  <input class="form-control" type="text" name="firstName" required>
                                              </div>

                                              <div class="col-sm-6 form-group">
                                                  <label>Last Name</label>
                                                  <input class="form-control" type="text" name="lastName" required>
                                              </div>

                                              <div class="col-sm-6 form-group">
                                                  <label>Email Address</label>
                                                  <input class="form-control" type="text" name="email" required>
                                              </div>

                                              <div class="col-sm-6 form-group">
                                                  <label>Password</label>
                                                  <input class="form-control" type="password" name="password" required>
                                              </div>

                                            </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>

                                            @if (count($errors) > 0)
                                               <div class="alert alert-danger">
                                                   <ul>
                                                       @foreach ($errors->all() as $error)
                                                       <li>{{ $error }}</li>
                                                       @endforeach
                                                   </ul>
                                               </div>
                                              @endif

                                              @if ($message = Session::get('error'))
                                                   <div class="alert alert-danger">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if ($message = Session::get('success'))
                                                   <div class="alert alert-success">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if (session('status0'))
                                              <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status0') }}
                                              </div>
                                              @endif

                                              @if (session('status1'))
                                              <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status1') }}
                                              </div>
                                              @endif

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Role</th>
                                            <th>Email</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Name</th>
                                          <th>Role</th>
                                          <th>Email</th>
                                          <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $user) {
                                        ?>
                                        <tr>
                                          <td><?php echo $user->firstName; ?> <?php echo $user->lastName; ?></td>
                                          <td><?php echo $user->roleName; ?></td>
                                          <td><?php echo $user->email; ?></td>
                                            <td>
                                              <?php if($user->roleId == 3) { ?>
                                                <a href="<?php $url = URL::to("/viewAttendant/".$user->id); print_r($url); ?>" class="btn btn-primary"><i class="fa fa-eye"></i> View Transactions</a>
                                              <?php } ?>
                                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-edituser<?php echo $user->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleteuser<?php echo $user->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                        </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-edituser<?php echo $user->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'edituser']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit User</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $user->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Role</label>
                                              <select class="form-control" name="roleId" required>
                                                <option value="<?php echo $user->roleId; ?>"><?php echo $user->roleName; ?></option>
                                                <?php foreach ($roles as $keyf) { ?>
                                                  <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->roleName; ?></option>
                                                <?php } ?>
                                              </select>
                                          </div>

                                      <div class="col-sm-6 form-group">
                                          <label>First Name</label>
                                          <input class="form-control" type="text" name="firstName" value="<?php echo $user->firstName; ?>" required>
                                      </div>

                                      <div class="col-sm-6 form-group">
                                          <label>Last Name</label>
                                          <input class="form-control" type="text" name="lastName" value="<?php echo $user->lastName; ?>" required>
                                      </div>

                                      <div class="col-sm-6 form-group">
                                          <label>Email Address</label>
                                          <input class="form-control" type="text" name="email" value="<?php echo $user->email; ?>" required>
                                      </div>

                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deleteuser<?php echo $user->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deleteuser']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete User</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $user->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this User</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>

                                          </div>
                                        </div>
                                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
