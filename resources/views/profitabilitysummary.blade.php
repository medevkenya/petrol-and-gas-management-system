<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Profitability Summary</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Profitability Summary</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All Profitability Summary</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                  <div class="col-md-6">
                      <div class="ibox">
                          <div class="ibox-head">
                              <div class="ibox-title">Month of <?php echo date('F', mktime(0, 0, 0, $month, 10)); ?></div>
                              <div class="ibox-tools">
                                {!! Form::open(['url' => 'searchprofitbymonth']) !!}
                                <select class="form-control" name="month" id="month" style="margin-left:-100%;margin-bottom:-28%;" required>
                               <option value="<?php echo date('m'); ?>"><?php echo date('F'); ?></option>
                               <?php for($i=1; $i<=12; $i++){ $month = date('F', mktime(0, 0, 0, $i, 10)); $monthNumber = date('m', mktime(0, 0, 0, $i, 10));  ?>
                                 <option value="<?php echo $monthNumber; ?>"><?php echo $month; ?></option>
                               <?php } ?>
                             </select>
                              <button type="submit" class="btn btn-success" style="margin-left:5%;"><i class="fa fa-search"></i> Search</button>
                              {!! Form::close() !!}
                              </div>
                          </div>
                          <div class="ibox-body">

                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Profit</th>
                        </tr>
                    </thead>
                    <!-- <tfoot>
                        <tr>
                          <th>Product Name</th>
                          <th>Profit</th>
                        </tr>
                    </tfoot> -->
                    <tbody>
                      <?php $mtaxAmount = 0; $mgrandProfit = 0; $mgrandTax = 0;  foreach ($list as $item) {
                        $mgrandProfit += intval(preg_replace('/[^\d.]/', '', $item['profit']));
                        $mgrandTax += intval(preg_replace('/[^\d.]/', '', $item['taxAmount']));
                        ?>
                        <tr>
                          <td><?php echo $item['productName']; ?></td>
                          <td><?php echo $item['profit']; ?></td>
                        </tr>
                      <?php } ?>
                      <tfoot>
                          <tr>
                            <th>EXPENSES</th>
                            <th><?php echo $expenseForMonth; ?></th>
                          </tr>
                          <tr>
                            <th>TAX
                              <!-- (<?php //echo $taxPercentage; ?> %) -->
                            </th>
                            <th><?php echo $mgrandTax; ?></th>
                          </tr>
                          <tr>
                            <th>NET PROFIT</th>
                            <th><?php $tprof = $mgrandProfit - ($mgrandTax + $expenseForMonth);
                              echo number_format($tprof,2); ?></th>
                          </tr>
                          </tfoot>
                    </tbody>
                </table>

                          </div>
                        </div>
                      </div>

                                  <div class="col-md-6">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Year of <?php echo $year; $lastYear = date("Y",strtotime("-1 year")); ?></div>
                                              <div class="ibox-tools">
                                                {!! Form::open(['url' => 'searchprofitbyyear']) !!}
                                                <select class="form-control" name="year" id="year" style="margin-left:-100%;margin-bottom:-40%;" required>
                                               <option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option>
                                               <?php foreach(range($lastYear, 2020) as $year){  ?>
                                                 <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                                               <?php } ?>
                                             </select>
                                              <button type="submit" class="btn btn-success" style="margin-left:5%;"><i class="fa fa-search"></i> Search</button>
                                              {!! Form::close() !!}
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                    <table class="table table-striped table-bordered table-hover" id="example-table3" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Product Name</th>
                                            <th>Profit</th>
                                        </tr>
                                    </thead>
                                    <!-- <tfoot>
                                        <tr>
                                          <th>Product Name</th>
                                          <th>Profit</th>
                                        </tr>
                                    </tfoot> -->
                                    <tbody>
                                      <?php $taxAmount = 0; $grandProfit = 0; $grandTax =0; foreach ($yearlist as $itemy) {
                                        $grandProfit += intval(preg_replace('/[^\d.]/', '', $itemy['profit']));
                                        $grandTax += intval(preg_replace('/[^\d.]/', '', $itemy['taxAmount']));
                                        ?>
                                        <tr>
                                          <td><?php echo $itemy['productName']; ?></td>
                                          <td><?php echo $itemy['profit']; ?></td>
                                        </tr>
                                      <?php } ?>
                                      <tfoot>
                                          <tr>
                                            <th>EXPENSES</th>
                                            <th><?php echo $expenseForYear; ?></th>
                                          </tr>
                                          <tr>
                                            <th>TAX
                                              <!-- (<?php //echo $taxPercentage; ?> %) -->
                                            </th>
                                            <th><?php echo $grandTax; ?></th>
                                          </tr>
                                          <tr>
                                            <th>NET PROFIT</th>
                                            <th><?php $typrof = $grandProfit - ($grandTax + $expenseForYear);
                                              echo number_format($typrof,2); ?></th>
                                          </tr>
                                          </tfoot>
                                    </tbody>
                                </table>

                                          </div>
                                        </div>
                                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
    <script>
    $(document).ready(function() {
        $('#example-table3').DataTable();
    } );
    </script>
  </body>

  </html>
