<nav class="page-sidebar" id="sidebar">
    <div id="sidebar-collapse">
        <div class="admin-block d-flex">
            <div>
                <img src="{{ URL::to('/') }}/profiles/<?php echo Auth::user()->profilePic; ?>" width="50px" height="50px" style="border-radius:50%;" />
            </div>
            <div class="admin-info">
                <div class="font-strong">{{Auth::user()->firstName}} {{Auth::user()->lastName}}</div><small>{{Auth::user()->mobileNo}}</small></div>
        </div>
        <ul class="side-menu metismenu">
            <li>
                <a class="active" href="{{URL::to('/dashboard')}}"><i class="sidebar-item-icon fa fa-th-large"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>
            <li class="heading">NAVIGATION</li>
            <li>
                <a href="{{URL::to('/customers')}}"><i class="sidebar-item-icon fa fa-list"></i>
                    <span class="nav-label">Customers</span>
                </a>
            </li>
            <li>
                <a href="{{URL::to('/suppliers')}}"><i class="sidebar-item-icon fa fa-list"></i>
                    <span class="nav-label">Suppliers</span>
                </a>
            </li>
            <li>
                <a href="{{URL::to('/products')}}"><i class="sidebar-item-icon fa fa-list"></i>
                    <span class="nav-label">Products</span>
                </a>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-money"></i>
                    <span class="nav-label">Sales</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                  <li>
                      <a href="{{URL::to('/makesales')}}">Make Sales</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/salesReport')}}">Sales Report</a>
                  </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-leaf"></i>
                    <span class="nav-label">Stock Management</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                  <li>
                      <a href="{{URL::to('/batches')}}">Batches</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/filterstockreport')}}">Stock Report</a>
                  </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-money"></i>
                    <span class="nav-label">Cashier</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                  <li>
                      <a href="{{URL::to('/accounts')}}">Company Accounts</a>
                  </li>
                    <li>
                        <a href="{{URL::to('/accounttransfers')}}">Accounts Transfers</a>
                    </li>
                    <li>
                        <a href="{{URL::to('/expensetypes')}}">Expense Types</a>
                    </li>
                    <li>
                        <a href="{{URL::to('/expenses')}}">Expenses</a>
                    </li>
                    <!--<li>-->
                    <!--    <a href="{{URL::to('/salesReport')}}">Sales Report</a>-->
                    <!--</li>-->
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-money"></i>
                    <span class="nav-label">Reports</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                  <li>
                      <a href="{{URL::to('/dailysales')}}">Daily Sales</a>
                  </li>
                  <!-- <li>
                      <a href="{{URL::to('/salesReport')}}">Sales Report</a>
                  </li> -->
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-money"></i>
                    <span class="nav-label">Statements</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                  <li>
                      <a href="{{URL::to('/consumption')}}">Customer Consumption</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/payments')}}">Customer Payments</a>
                  </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-users"></i>
                    <span class="nav-label">System Users</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                  <li>
                      <a href="{{URL::to('/users')}}">Manage Users</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/shifts')}}">Staff Shifts</a>
                  </li>
                </ul>
            </li>

            <li>
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-cog"></i>
                    <span class="nav-label">Settings</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                  <li>
                      <a href="{{URL::to('/categories')}}">Categories</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/pumps')}}">Pumbs</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/tanks')}}">Tanks</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/companyassets')}}">Company Assets</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/readings')}}">Dip Stick Readings</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/shifttypes')}}">Shift Types</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/roles')}}">Roles</a>
                  </li>
                  <li>
                      <a href="{{URL::to('/permissions')}}">Permissions</a>
                  </li>
                </ul>
            </li>
            <li>
                <a href="{{URL::to('/auditlogs')}}"><i class="sidebar-item-icon fa fa-list"></i>
                    <span class="nav-label">Audit Logs</span>
                </a>
            </li>
            <!-- <li class="heading">Reports</li> -->
            <!-- <li>
                <a href="{{URL::to('/incomestatement')}}"><i class="sidebar-item-icon fa fa-hospital-o"></i>
                    <span class="nav-label">Income Statement</span>
                </a>
            </li> -->
            <!-- <li>
                <a href="{{URL::to('/profitabilitysummary')}}"><i class="sidebar-item-icon fa fa-shield"></i>
                    <span class="nav-label">Profitability Summary</span>
                </a>
            </li> -->
        </ul>
    </div>
</nav>
