<!-- GLOBAL MAINLY STYLES-->
<link href="{{ URL::asset('assets/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/vendors/themify-icons/css/themify-icons.css')}}" rel="stylesheet" />
<!-- PLUGINS STYLES-->
<link href="{{ URL::asset('assets/vendors/jvectormap/jquery-jvectormap-2.0.3.css')}}" rel="stylesheet" />
<!-- THEME STYLES-->
<link href="{{ URL::asset('assets/css/main.min.css')}}" rel="stylesheet" />
<!-- PAGE LEVEL STYLES-->
<style>
.ajax-loader {
visibility: hidden;
background-color: rgba(255,255,255,0.7);
position: absolute;
z-index: +100 !important;
width: 100%;
height:100%;
display: flex;
justify-content: center;
}

.ajax-loader img {
position: relative;
width: 40px;
height: 40px;
/* top:20%;
left:30%; */
}
</style>
<?php \App\Pumps::seedAll(); ?>
