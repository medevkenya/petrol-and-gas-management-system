<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Products</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Products</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All Products</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Products</div>
                                              <div class="ibox-tools">
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addrole"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-addrole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                              <form action="{{ route('addproduct') }}" method="post" enctype="multipart/form-data">
                                              {{ csrf_field() }}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New Product</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">

                                                  <?php
                                                  $categories = \App\Categories::getAll();
                                                  ?>

                                                  <div class="col-sm-6 form-group">
                                                      <label>Category</label>
                                                      <select class="form-control" name="categoryId" id="categoryId" required>
                                                     <option></option>
                                                     <?php foreach ($categories as $keyf) { ?>
                                                       <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->categoryName; ?></option>
                                                     <?php } ?>
                                                   </select>
                                                  </div>

                                                  <div class="col-sm-6 form-group">
                                                      <label>Unit</label>
                                                      <select class="form-control" name="unit" id="unit" required>
                                                     <option></option>
                                                       <option value="ltr">ltr</option>
                                                       <option value="pc">pc</option>
                                                       <option value="ml">ml</option>
                                                       <option value="gm">gm</option>
                                                       <option value="Kg">Kg</option>
                                                   </select>
                                                  </div>

                                                </div>
                                                <div class="row">

                                              <div class="col-sm-12 form-group">
                                                  <label>Product Name</label>
                                                  <input class="form-control" type="text" name="productName" required>
                                              </div>

                                            </div>
                                            <div class="row">

                                            <div class="col-sm-6 form-group">
                                              <label>Sell Price</label>
                                              <input class="form-control" type="number" step="0.01" name="sellPrice" required>
                                            </div>

                                            <div class="col-sm-6 form-group">
                                              <label>Alert Level</label>
                                              <input class="form-control" type="number" step="0.01" name="alertLevel" required>
                                            </div>

                                            </div>
                                            <div class="row">
                                              <div class="col-sm-6 form-group">
                                                  <label>Photo</label>
                                              <input type="file" name="photoUrl" placeholder="Choose photo" id="photoUrl">
                                            </div>
                                            <div class="col-sm-6 form-group">
                                              <img id="preview-image-before-upload" src="{{ URL::asset('photos/medicine.png')}}" alt="preview image" style="max-height: 150px;">
                                          </div>
                                            </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>

                                            @error('photoUrl')
                                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                            @enderror

                                            @if (count($errors) > 0)
                                             <div class="alert alert-danger">
                                                 <ul>
                                                     @foreach ($errors->all() as $error)
                                                     <li>{{ $error }}</li>
                                                     @endforeach
                                                 </ul>
                                             </div>
                                            @endif

                                            @if ($message = Session::get('error'))
                                                 <div class="alert alert-danger">
                                                     {{ $message }}
                                                 </div>
                                            @endif

                                            @if ($message = Session::get('success'))
                                                 <div class="alert alert-success">
                                                     {{ $message }}
                                                 </div>
                                            @endif

                                            @if (session('status0'))
                                            <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            {{ session('status0') }}
                                            </div>
                                            @endif

                                            @if (session('status1'))
                                            <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            {{ session('status1') }}
                                            </div>
                                            @endif

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                          <th width="8%">Photo</th>
                                            <th>Product Name</th>
                                            <th>Category</th>
                                            <th>Qty Bal</th>
                                            <th>Sell Price</th>
                                            <th>Total Value</th>
                                            <th>Alert Level</th>
                                            <th width="25%">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php $GrandtotalValueSell = 0; foreach ($list as $product) {
                                        $quantity = $product->stockbalance;
                                        $totalValueSell = $product->sellPrice * $quantity;
                                        $GrandtotalValueSell += intval(preg_replace('/[^\d.]/', '', $totalValueSell));
                                        ?>
                                        <tr>
                                          <td><img src="{{ URL::to('/') }}/public/photos/<?php echo $product->photoUrl; ?>" alt=""></td>
                                          <td><?php echo $product->productName; ?></td>
                                          <td><?php echo $product->categoryName; ?></td>
                                          <td><?php echo $quantity." ".$product->unit; ?></td>

                                          <td><?php echo $product->sellPrice; ?></td>
                                          <td><?php echo number_format($totalValueSell,0); ?></td>
                                          <td><?php echo $product->alertLevel." ".$product->unit; ?></td>
                                          <td>
                                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-eject<?php echo $product->id; ?>"><i class="fa fa-edit"></i> Eject</button>
                                          <a href="<?php $url = URL::to("/viewProduct/".$product->id); print_r($url); ?>" class="btn btn-primary"><i class="fa fa-eye"></i> View</a>
                                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editrole<?php echo $product->id; ?>"><i class="fa fa-edit"></i></button>
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleterole<?php echo $product->id; ?>"><i class="fa fa-trash"></i></button>
                                        </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-eject<?php echo $product->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'ejectStock']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Eject Quantity</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                              <input type="hidden" name="id" value="<?php echo $product->id; ?>" class="form-control" required>
                                              <div class="col-sm-6 form-group">
                                                  <label>Quantity</label>
                                                  <input class="form-control" type="number" step="0.01" name="quantity" required>
                                              </div>
                                            </div>

                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-editrole<?php echo $product->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            <form action="{{ route('editproduct') }}" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Product</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">

                                              <input type="hidden" name="id" value="<?php echo $product->id; ?>" class="form-control" required>

                                              <div class="row">

                                                <?php
                                                  $categories = \App\Categories::getAll();
                                                ?>

                                                <div class="col-sm-6 form-group">
                                                    <label>Category</label>
                                                    <select class="form-control" name="categoryId" id="categoryIde" required>
                                                   <option value="<?php echo $product->categoryId; ?>"><?php echo $product->categoryName; ?></option>
                                                   <?php foreach ($categories as $keyf) { ?>
                                                     <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->categoryName; ?></option>
                                                   <?php } ?>
                                                 </select>
                                                </div>

                                                <div class="col-sm-6 form-group">
                                                    <label>Unit</label>
                                                    <select class="form-control" name="unit" id="unit" required>
                                                   <option value="<?php echo $product->unit; ?>"><?php echo $product->unit; ?></option>
                                                     <option value="ltr">ltr</option>
                                                     <option value="pc">pc</option>
                                                     <option value="ml">ml</option>
                                                     <option value="gm">gm</option>
                                                     <option value="Kg">Kg</option>
                                                 </select>
                                                </div>

                                              </div>
                                              <div class="row">

                                              <div class="col-sm-12 form-group">
                                                <label>Product Name</label>
                                                <input class="form-control" type="text" name="productName" value="<?php echo $product->productName; ?>" required>
                                              </div>

                                              </div>
                                              <div class="row">

                                              <div class="col-sm-6 form-group">
                                                <label>Sell Price</label>
                                                <input class="form-control" type="number" step="0.01" name="sellPrice" value="<?php echo $product->sellPrice; ?>" required>
                                              </div>

                                              <div class="col-sm-6 form-group">
                                                <label>Alert Level</label>
                                                <input class="form-control" type="number" step="0.01" name="alertLevel" value="<?php echo $product->alertLevel; ?>" required>
                                              </div>

                                              </div>
                                              <div class="row">
                                              <div class="col-sm-6 form-group">
                                                <label>Photo</label>
                                              <input type="file" name="photoUrl" placeholder="Choose photo" id="photoUrl">
                                              </div>
                                              <div class="col-sm-6 form-group">
                                              <img id="preview-image-before-upload" src="{{ URL::to('/') }}/public/photos/<?php echo $product->photoUrl; ?>" alt="preview image" style="max-height: 150px;">
                                              </div>
                                              </div>

                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deleterole<?php echo $product->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deleteproduct']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Product</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $product->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this product</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                      <tfoot>
                                      <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>Ksh. <?php echo number_format($GrandtotalValueSell,0); ?></td>
                                        <td></td>
                                        <td></td>
                                      </tr>
                                    </tfoot>
                                    </tbody>
                                </table>

                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                            <div class="ibox-title">Statement</div>
                                          </div>
                                          <div class="ibox-body">
                                      <table class="table table-striped table-bordered table-hover" id="example-table3" cellspacing="0" width="100%">
                                      <thead>
                                          <tr>
                                              <th>Name</th>
                                              <th>Opening RD</th>
                                              <th>Purchase</th>
                                              <th>RTT</th>
                                              <th>Sold</th>
                                              <th>Actual BL</th>
                                              <th>Meter RD</th>
                                              <th>Loss/Excess</th>
                                              <th>Cost Price</th>
                                              <th>Sale Price</th>
                                              <th>Total Sales</th>
                                              <th>Total Cost</th>
                                              <th>Profit</th>
                                          </tr>
                                      </thead>
                                      <tfoot>
                                          <tr>
                                            <th>Name</th>
                                            <th>Opening RD</th>
                                            <th>Purchase</th>
                                            <th>RTT</th>
                                            <th>Sold</th>
                                            <th>Actual BL</th>
                                            <th>Meter RD</th>
                                            <th>Loss/Excess</th>
                                            <th>Cost Price</th>
                                            <th>Sale Price</th>
                                            <th>Total Sales</th>
                                            <th>Total Cost</th>
                                            <th>Profit</th>
                                          </tr>
                                      </tfoot>
                                      <tbody>
                                        <?php $grandtotalCost = 0; $grandtotalSales = 0; $totalProfit = 0; foreach ($statements as $statement) {
                                            $grandtotalCost += intval(preg_replace('/[^\d.]/', '', $statement['totalCost']));
                                            $grandtotalSales += intval(preg_replace('/[^\d.]/', '', $statement['totalSales']));
                                            $totalProfit += intval(preg_replace('/[^\d.]/', '', $statement['profit']));
                                          ?>
                                          <tr>
                                            <td><?php echo $statement['productName']; ?></td>
                                            <td><?php echo $statement['openingReading']; ?></td>
                                            <td><?php echo $statement['purchaseQuantity']; ?></td>
                                            <td><?php echo $statement['rtt']; ?></td>
                                            <td><?php echo $statement['soldQuantity']; ?></td>
                                            <td><?php echo $statement['actualBalance']; ?></td>
                                            <td><?php echo $statement['meterReading']; ?></td>
                                            <td><?php echo $statement['lossExcess']; ?></td>
                                            <td><?php echo $statement['costPrice']; ?></td>
                                            <td><?php echo $statement['sellPrice']; ?></td>
                                            <td><?php echo $statement['totalSales']; ?></td>
                                            <td><?php echo $statement['totalCost']; ?></td>
                                            <td><?php echo $statement['profit']; ?></td>
                                          </tr>
                                        <?php } ?>

                                      </tbody>
                                      <tfoot>
                                      <tr>
                                        <td><strong>TOTALS</strong></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><strong><?php echo number_format($grandtotalSales,2); ?></strong></td>
                                        <td><strong><?php echo number_format($grandtotalCost,2); ?></strong></td>
                                        <td><strong><?php echo number_format($totalProfit,2); ?></strong></td>
                                      </tr>
                                      </tfoot>
                                    </table>
                                    </div>
                                    </div>
                                    </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
    <script>
    $(document).ready(function() {
        //$('#example-table2').DataTable();
        $('#example-table3').DataTable();
    } );
    </script>
  </body>

  </html>
