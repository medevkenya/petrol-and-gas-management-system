<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Shifts</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Shifts</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All Shifts</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Shifts</div>
                                              <div class="ibox-tools">
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addrole"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-addrole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                {!! Form::open(['url' => 'addshift']) !!}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New Shift</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">

                                                  <?php
                                                  $pumps = \App\Pumps::getAll();
                                                  $users = \App\User::select('id','firstName','lastName')->where('roleId',3)->where('isDeleted',0)->get();
                                                  $shifttypes = \App\Shifttypes::getAll();
                                                  ?>

                                                  <div class="col-sm-6 form-group">
                                                      <label>User</label>
                                                      <select class="form-control" name="userId" id="userId" required>
                                                     <option></option>
                                                     <?php foreach ($users as $keyfus) { ?>
                                                       <option value="<?php echo $keyfus->id; ?>"><?php echo $keyfus->firstName; ?> <?php echo $keyfus->lastName; ?></option>
                                                     <?php } ?>
                                                   </select>
                                                  </div>

                                                  <div class="col-sm-6 form-group">
                                                      <label>Pump</label>
                                                      <select class="form-control" name="pumpId" id="pumpId" required>
                                                     <option></option>
                                                     <?php foreach ($pumps as $keyfp) { ?>
                                                       <option value="<?php echo $keyfp->id; ?>"><?php echo $keyfp->pumpName; ?></option>
                                                     <?php } ?>
                                                   </select>
                                                  </div>

                                                </div>
                                                  <div class="row">

                                              <div class="col-sm-6 form-group">
                                                  <label>Shift</label>
                                                  <select class="form-control" name="shiftTypeId" id="shiftTypeId" required>
                                                 <option></option>
                                                 <?php foreach ($shifttypes as $keyshi) { ?>
                                                   <option value="<?php echo $keyshi->id; ?>"><?php echo $keyshi->shifttypeName; ?></option>
                                                 <?php } ?>
                                               </select>
                                              </div>

                                            </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>

                                            @if (count($errors) > 0)
                                               <div class="alert alert-danger">
                                                   <ul>
                                                       @foreach ($errors->all() as $error)
                                                       <li>{{ $error }}</li>
                                                       @endforeach
                                                   </ul>
                                               </div>
                                              @endif

                                              @if ($message = Session::get('error'))
                                                   <div class="alert alert-danger">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if ($message = Session::get('success'))
                                                   <div class="alert alert-success">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if (session('status0'))
                                              <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status0') }}
                                              </div>
                                              @endif

                                              @if (session('status1'))
                                              <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status1') }}
                                              </div>
                                              @endif

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                          <th>Staff</th>
                                            <th>Pump</th>
                                            <th>Shift</th>
                                            <th>Date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Staff</th>
                                            <th>Pump</th>
                                            <th>Shift</th>
                                            <th>Date</th>
                                            <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $shift) {
                                        ?>
                                        <tr>
                                          <td><?php echo $shift->firstName; ?> <?php echo $shift->lastName; ?></td>
                                          <td><?php echo $shift->pumpName; ?></td>
                                          <td><?php echo $shift->shifttypeName; ?></td>
                                          <td><?php echo $shift->date; ?></td>
                                            <td>
                                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editrole<?php echo $shift->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleterole<?php echo $shift->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                        </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-editrole<?php echo $shift->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'editshift']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Shift</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $shift->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>User</label>
                                              <select class="form-control" name="userId" id="userId" required>
                                             <option value="<?php echo $shift->userId; ?>"><?php echo $shift->firstName; ?> <?php echo $shift->lastName; ?></option>
                                             <?php foreach ($users as $keyfu) { ?>
                                               <option value="<?php echo $keyfu->id; ?>"><?php echo $keyfu->firstName; ?> <?php echo $keyfu->lastName; ?></option>
                                             <?php } ?>
                                           </select>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Pump</label>
                                              <select class="form-control" name="pumpId" id="pumpId" required>
                                             <option value="<?php echo $shift->pumpId; ?>"><?php echo $shift->firstName; ?> <?php echo $shift->lastName; ?></option>
                                             <?php foreach ($pumps as $keyfp) { ?>
                                               <option value="<?php echo $keyfp->id; ?>"><?php echo $keyfp->pumpName; ?></option>
                                             <?php } ?>
                                           </select>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Shift</label>
                                              <select class="form-control" name="shiftTypeId" required>
                                             <option value="<?php echo $shift->shiftTypeId; ?>"><?php echo $shift->shifttypeName; ?></option>
                                             <?php foreach ($shifttypes as $keyshi) { ?>
                                               <option value="<?php echo $keyshi->id; ?>"><?php echo $keyshi->shifttypeName; ?></option>
                                             <?php } ?>
                                           </select>
                                          </div>

                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deleterole<?php echo $shift->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deleteshift']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Shift</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $shift->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this shift</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>

                                          </div>
                                        </div>
                                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
