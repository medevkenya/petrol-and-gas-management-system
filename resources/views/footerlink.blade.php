<!-- CORE PLUGINS-->
<script src="{{ URL::asset('assets/vendors/jquery/dist/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/popper.js/dist/umd/popper.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/metisMenu/dist/metisMenu.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<!-- PAGE LEVEL PLUGINS-->
<script src="{{ URL::asset('assets/vendors/chart.js/dist/Chart.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/jvectormap/jquery-jvectormap-us-aea-en.js')}}" type="text/javascript"></script>
<!-- CORE SCRIPTS-->
<script src="{{ URL::asset('assets/js/app.min.js')}}" type="text/javascript"></script>
<!-- PAGE LEVEL SCRIPTS-->
<script>
$("#categoryId").change(function(){
  $.ajax({
      url: "{{ route('getProducts') }}?categoryId=" + $(this).val(),
      method: 'GET',
      success: function(data) {
        //console.log("js products--"+data.products);
          $('#productId').html(data.products);
      }
  });
});

$("#categoryIdBatch").change(function(){
  $.ajax({
      url: "{{ route('getProducts') }}?categoryId=" + $(this).val(),
      method: 'GET',
      success: function(data) {
        //console.log("js products--"+data.products);
          $('#batchProductId').html(data.products);
      }
  });
});

$("#categoryIdbe").change(function(){
  $.ajax({
      url: "{{ route('getProducts') }}?categoryId=" + $(this).val(),
      method: 'GET',
      success: function(data) {
        //console.log("js products--"+data.products);
          $('#productIde').html(data.products);
      }
  });
});

function categoryIdbe(id) {
  var d = document.getElementById("categoryIdbe"+id+"").value;
  $.ajax({
      url: "{{ route('getProducts') }}?categoryId=" + d,
      method: 'GET',
      success: function(data) {
        //console.log("js products--"+data.products);
          $('#productIde'+id+'').html(data.products);
      }
  });
}

$("#productId").change(function(){
  $.ajax({
      url: "{{ route('getProduct') }}?productId=" + $(this).val(),
      method: 'GET',
      success: function(data)
      {
        console.log("js sellingPrice--"+data.sellingPrice);
        console.log("js availableStock--"+data.availableStock);
         if(data.status == 1)
         {
          $('#sellingPrice').html(data.sellingPrice);

          //var productId = $(this).val();
          var quantity = document.getElementById("saleQuantity").value;
          var totalCost = document.getElementById("totalCost").innerHTML;
          var sellingPrice = data.sellingPrice;//document.getElementById("sellingPrice").innerHTML;
          var discount = document.getElementById("saleDiscount").value;

          //if(subtotal > discount) {
          //var newsubtotal = +subtotal - +discount;
          var newtotal = +quantity * +sellingPrice;
          var finalnewtotal = newtotal - discount;
          console.log("CHANGE PRODUCT productId="+productId+"--quantity="+quantity+"---sellingPrice="+sellingPrice+"----discount="+discount+"--totalCost="+newtotal+"---");
          $("#totalCost").html(parseInt(finalnewtotal));

         }
         else if(data.status == 1)
         {
           document.getElementById("failmessage").innerHTML = "This product is out of stock";
           $('#failmodal').modal('show');
         }
      }
  });
});
$(document).ready(function (e) {
$('#photoUrl').change(function(){
let reader = new FileReader();
reader.onload = (e) => {
  $('#preview-image-before-upload').attr('src', e.target.result);
}
reader.readAsDataURL(this.files[0]);
});
});
</script>
