<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Pumps</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Pumps</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All Pumps</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Pumps</div>
                                              <div class="ibox-tools">
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addrole"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-addrole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                {!! Form::open(['url' => 'addpump']) !!}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New Pump</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">

                                              <div class="col-sm-12 form-group">
                                                  <label>Pump Name</label>
                                                  <input class="form-control" type="text" name="pumpName" required>
                                              </div>

                                            </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>

                                            @if (count($errors) > 0)
                                               <div class="alert alert-danger">
                                                   <ul>
                                                       @foreach ($errors->all() as $error)
                                                       <li>{{ $error }}</li>
                                                       @endforeach
                                                   </ul>
                                               </div>
                                              @endif

                                              @if ($message = Session::get('error'))
                                                   <div class="alert alert-danger">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if ($message = Session::get('success'))
                                                   <div class="alert alert-success">
                                                       {{ $message }}
                                                   </div>
                                              @endif

                                              @if (session('status0'))
                                              <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status0') }}
                                              </div>
                                              @endif

                                              @if (session('status1'))
                                              <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              {{ session('status1') }}
                                              </div>
                                              @endif

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Pump Name</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Pump Name</th>
                                          <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $pump) {
                                        $pumpreadings = \App\Pumpreadings::getToday($pump->id);
                                        ?>
                                        <tr>
                                          <td><?php echo $pump->pumpName; ?></td>
                                            <td>
                                              <?php foreach ($pumpreadings as $keyre) { ?>
                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-reading<?php echo $keyre->id; ?>"><i class="fa fa-edit"></i> Update <?php echo $keyre->shifttypeName; ?> Reading</button>
                                              <?php } ?>
                                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editrole<?php echo $pump->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleterole<?php echo $pump->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                        </td>
                                        </tr>

                                        <?php foreach ($pumpreadings as $keyre) { ?>
                                          <!-- Modal -->
                                          <div class="modal fade text-left" id="modal-reading<?php echo $keyre->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                              {!! Form::open(['url' => 'editpumpreading']) !!}
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1"><?php echo $pump->pumpName; ?></h4>
                                              <h4 class="modal-title" id="myModalLabel1">Update <?php echo $keyre->shifttypeName; ?> Reading</h4>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                              </div>
                                              <div class="modal-body">
                                              <div class="row">
                                              <div class="col-xl-12 col-lg-12 col-md-12">
                                                <input type="hidden" name="id" value="<?php echo $keyre->id; ?>" class="form-control" required>
                                            </div>

                                            <div class="col-sm-12 form-group">
                                                <label>Opening Reading</label>
                                                <input class="form-control" type="number" name="openReading" step="0.01" value="<?php echo $keyre->openReading; ?>">
                                            </div>

                                            <div class="col-sm-12 form-group">
                                                <label>Close Reading</label>
                                                <input class="form-control" type="number" name="closeReading" step="0.01" value="<?php echo $keyre->closeReading; ?>">
                                            </div>

                                          </div>
                                              </div>
                                              <div class="modal-footer">
                                              <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                              <button type="submit" class="btn btn-primary">Save Changes</button>
                                              </div>
                                            </div>
                                            {!! Form::close() !!}
                                            </div>
                                          </div>

                                        <?php } ?>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-editrole<?php echo $pump->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'editpump']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Pump</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $pump->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Pump Name</label>
                                              <input class="form-control" type="text" name="pumpName" value="<?php echo $pump->pumpName; ?>" required>
                                          </div>

                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deleterole<?php echo $pump->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deletepump']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Pump</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $pump->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this pump</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>

                              </div>
                            </div>
                          </div>

                          <div class="col-md-12">
                              <div class="ibox">
                                  <div class="ibox-head">
                                      <div class="ibox-title">Pump Readings</div>
                                  </div>
                                  <div class="ibox-body">
                          <table class="table table-striped table-bordered table-hover" id="example-table2" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                  <th>Pump Name</th>
                                  <th>Shift</th>
                                  <th>Open Reading</th>
                                  <th>Close Reading</th>
                                  <th>Date</th>
                                  <th>Last Update</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $pumpreadinglist = \App\Pumpreadings::getAll();
                             foreach ($pumpreadinglist as $rlist) {
                              ?>
                              <tr>
                                <td><?php echo $rlist->pumpName; ?></td>
                                <td><?php echo $rlist->shifttypeName; ?></td>
                                <td><?php echo $rlist->openReading; ?></td>
                                <td><?php echo $rlist->closeReading; ?></td>
                                <td><?php echo $rlist->date; ?></td>
                                <td><?php echo $rlist->updated_at; ?></td>
                              </tr>
                            <?php } ?>
                          </tbody>
                      </table>
                                          </div>
                                        </div>
                                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
    <script>
    $(document).ready(function() {
        $('#example-table2').DataTable();
    } );
    </script>
  </body>

  </html>
