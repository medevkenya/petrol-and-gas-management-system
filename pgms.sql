-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2021 at 09:28 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pgms`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `accountName` varchar(50) NOT NULL,
  `balance` decimal(10,2) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `accountName`, `balance`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'KCB BANK', '3995000.00', 1, 1, 0, '2021-05-16 08:15:09', '2021-05-17 13:14:11'),
(2, 'EQUITY BANK', '200690.00', 1, 1, 0, '2021-05-18 02:16:31', '2021-05-18 02:29:25'),
(3, 'M-PESA PAYBILL 25364', '10000.00', 1, 1, 0, '2021-05-18 02:16:49', '2021-05-18 02:16:49');

-- --------------------------------------------------------

--
-- Table structure for table `accounttransfers`
--

CREATE TABLE `accounttransfers` (
  `id` int(11) NOT NULL,
  `accountId` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `type` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `reason` varchar(200) DEFAULT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounttransfers`
--

INSERT INTO `accounttransfers` (`id`, `accountId`, `amount`, `type`, `date`, `reason`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, '1000000.00', 'Withdraw', '2021-05-16', '', 1, 1, 0, '2021-05-16 08:18:29', '2021-05-16 08:18:29'),
(2, 1, '5000.00', 'Withdraw', '2021-05-17', 'it was good', 1, 1, 0, '2021-05-17 13:14:11', '2021-05-17 13:14:11'),
(3, 2, '500.00', 'Deposit', '2021-05-18', NULL, 1, 1, 0, '2021-05-18 02:20:07', '2021-05-18 02:20:07'),
(4, 2, '800.00', 'sale', '2021-05-18', NULL, 1, 1, 0, '2021-05-18 02:24:02', '2021-05-18 02:24:02'),
(5, 2, '600.00', 'Transfer', '2021-05-18', 'cool biz', 1, 1, 0, '2021-05-18 02:25:37', '2021-05-18 02:25:37'),
(6, 2, '690.00', 'Transfer', '2021-05-18', 'Its lawyers day', 1, 1, 0, '2021-05-18 02:29:25', '2021-05-18 02:29:25');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `adminId`, `description`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'Created new account [KCB BANK]', 1, 0, '2021-05-16 08:15:09', '2021-05-16 08:15:09'),
(2, 1, 'Added new statement accountId [1]', 1, 0, '2021-05-16 08:15:09', '2021-05-16 08:15:09'),
(3, 1, 'Paid supplier Ksh. 5000 from account [KCB BANK]', 1, 0, '2021-05-16 08:16:30', '2021-05-16 08:16:30'),
(4, 1, 'Added new statement accountId [1]', 1, 0, '2021-05-16 08:16:30', '2021-05-16 08:16:30'),
(5, 1, 'Added new batch [1]', 1, 0, '2021-05-16 08:16:30', '2021-05-16 08:16:30'),
(6, 1, 'Added new account transfer [1000000]', 1, 0, '2021-05-16 08:18:29', '2021-05-16 08:18:29'),
(7, 1, 'Withdrew cash amount [1000000] from [KCB BANK]', 1, 0, '2021-05-16 08:18:29', '2021-05-16 08:18:29'),
(8, 1, 'Added new statement accountId [1]', 1, 0, '2021-05-16 08:18:29', '2021-05-16 08:18:29'),
(9, 1, 'Added new batch [1]', 1, 0, '2021-05-16 08:25:04', '2021-05-16 08:25:04'),
(10, 1, 'Added new sale [2]', 1, 0, '2021-05-16 08:28:16', '2021-05-16 08:28:16'),
(11, 1, 'Customer Deposited Ksh. 5000 to account [KCB BANK]', 1, 0, '2021-05-16 08:33:02', '2021-05-16 08:33:02'),
(12, 1, 'Added new statement accountId [1]', 1, 0, '2021-05-16 08:33:02', '2021-05-16 08:33:02'),
(13, 1, 'Added new role [Pump Attendant]', 1, 0, '2021-05-16 16:19:02', '2021-05-16 16:19:02'),
(14, 1, 'Added user [Andrew Mukoya - admin@admin.com]', 1, 0, '2021-05-16 16:39:07', '2021-05-16 16:39:07'),
(15, 1, 'Added new sale [1]', 1, 0, '2021-05-17 11:22:08', '2021-05-17 11:22:08'),
(16, 1, 'Added new customer consumption [1]', 1, 0, '2021-05-17 11:50:50', '2021-05-17 11:50:50'),
(17, 1, 'Added new customer consumption [1]', 1, 0, '2021-05-17 11:51:01', '2021-05-17 11:51:01'),
(18, 1, 'Edited customer consumption [1]', 1, 0, '2021-05-17 11:52:01', '2021-05-17 11:52:01'),
(19, 1, 'Added new account transfer [5000]', 1, 0, '2021-05-17 13:14:11', '2021-05-17 13:14:11'),
(20, 1, 'Withdrew cash amount [5000] from [KCB BANK]', 1, 0, '2021-05-17 13:14:11', '2021-05-17 13:14:11'),
(21, 1, 'Added new statement accountId [1]', 1, 0, '2021-05-17 13:14:11', '2021-05-17 13:14:11'),
(22, 1, 'Added new customer consumption [4]', 1, 0, '2021-05-18 01:51:12', '2021-05-18 01:51:12'),
(23, 1, 'Created new account [EQUITY BANK]', 1, 0, '2021-05-18 02:16:31', '2021-05-18 02:16:31'),
(24, 1, 'Added new statement accountId [2]', 1, 0, '2021-05-18 02:16:31', '2021-05-18 02:16:31'),
(25, 1, 'Created new account [M-PESA PAYBILL 25364]', 1, 0, '2021-05-18 02:16:49', '2021-05-18 02:16:49'),
(26, 1, 'Added new statement accountId [3]', 1, 0, '2021-05-18 02:16:49', '2021-05-18 02:16:49'),
(27, 1, 'Added new account transfer [500]', 1, 0, '2021-05-18 02:20:07', '2021-05-18 02:20:07'),
(28, 1, 'Added new account transfer [800] Reason : ', 1, 0, '2021-05-18 02:24:02', '2021-05-18 02:24:02'),
(29, 1, 'Added new account transfer [600] Reason : cool biz', 1, 0, '2021-05-18 02:25:37', '2021-05-18 02:25:37'),
(30, 1, 'Admin transferred Ksh. 690 to account [EQUITY BANK]', 1, 0, '2021-05-18 02:29:25', '2021-05-18 02:29:25'),
(31, 1, 'Added new statement accountId [2]', 1, 0, '2021-05-18 02:29:25', '2021-05-18 02:29:25'),
(32, 1, 'Added new account transfer [690] Reason : Its lawyers day', 1, 0, '2021-05-18 02:29:25', '2021-05-18 02:29:25');

-- --------------------------------------------------------

--
-- Table structure for table `batches`
--

CREATE TABLE `batches` (
  `id` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `purchasePrice` decimal(10,2) NOT NULL,
  `quantity` double NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `sold` double NOT NULL DEFAULT '0',
  `productId` int(11) NOT NULL,
  `status` enum('In Stock','Out of Stock','Expired','Inactive') NOT NULL DEFAULT 'In Stock',
  `type` enum('Cash','Credit') NOT NULL DEFAULT 'Cash',
  `tankId` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `isPaid` varchar(10) NOT NULL DEFAULT 'Paid',
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batches`
--

INSERT INTO `batches` (`id`, `supplierId`, `categoryId`, `purchasePrice`, `quantity`, `total`, `sold`, `productId`, `status`, `type`, `tankId`, `date`, `isPaid`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '100.00', 50, '5000.00', 20, 1, 'In Stock', 'Cash', 3, '2021-05-16', 'Paid', 1, 1, 0, '2021-05-16 08:16:30', '2021-05-17 11:22:08'),
(2, 1, 2, '100.00', 1000, '100000.00', 100, 2, 'In Stock', 'Credit', 2, '2021-05-16', 'Paid', 1, 1, 0, '2021-05-16 08:25:04', '2021-05-16 08:28:16');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `itemName` varchar(100) NOT NULL,
  `productId` int(11) NOT NULL,
  `quantity` int(2) NOT NULL,
  `sessionId` varchar(100) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `orderId` int(11) NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `totalCost` decimal(10,2) NOT NULL,
  `completed` tinyint(3) NOT NULL DEFAULT '0',
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `categoryName` varchar(50) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `photoUrl` text,
  `status` tinyint(3) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `adminId`, `categoryName`, `slug`, `photoUrl`, `status`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'Lubricants', NULL, NULL, 1, 1, 0, '2021-05-09 00:01:47', '2021-05-09 00:01:47'),
(2, 1, 'Fuel', NULL, NULL, 1, 1, 0, '2021-05-09 00:09:19', '2021-05-09 00:09:19'),
(3, 1, 'Break Fluid', NULL, NULL, 1, 1, 0, '2021-05-09 00:09:51', '2021-05-09 00:09:51'),
(4, 1, 'Cooking Gas', NULL, NULL, 1, 1, 0, '2021-05-14 10:15:38', '2021-05-14 10:15:38');

-- --------------------------------------------------------

--
-- Table structure for table `companyassets`
--

CREATE TABLE `companyassets` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `quantity` double NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `creditpayments`
--

CREATE TABLE `creditpayments` (
  `id` int(11) NOT NULL,
  `supplierId` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `date` date NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customerconsumption`
--

CREATE TABLE `customerconsumption` (
  `id` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `date` date NOT NULL,
  `amount` double NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` smallint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customerconsumption`
--

INSERT INTO `customerconsumption` (`id`, `customerId`, `date`, `amount`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, '0000-00-00', 1500, 1, 1, 0, '2021-05-17 11:50:50', '2021-05-17 11:51:11'),
(2, 1, '0000-00-00', 6000, 1, 1, 0, '2021-05-17 11:51:01', '2021-05-17 11:51:01'),
(3, 4, '0000-00-00', 3000, 1, 1, 0, '2021-05-18 01:51:12', '2021-05-18 01:51:12');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `customerName` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `contacts` text,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `customerName`, `email`, `contacts`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Embassava Sacco', 'medevkenya11@gmail.com', '0725557878', 1, 1, 0, '2021-05-09 07:45:58', '2021-05-09 07:46:25'),
(2, 'Kings Distributors', 'info@floatcapital.co.ke', '0701254545', 1, 1, 0, '2021-05-09 07:46:12', '2021-05-09 07:46:44'),
(3, 'KBS Sacco', 'kbssacco@gmail.com', '0756464663', 1, 1, 0, '2021-05-14 09:44:44', '2021-05-14 09:44:44'),
(4, 'Jayden', 'jd123@gmail.com', '0746934278', 1, 1, 0, '2021-05-14 10:53:37', '2021-05-14 10:53:37');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL,
  `expenseTypeId` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `description` text NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expensetypes`
--

CREATE TABLE `expensetypes` (
  `id` int(11) NOT NULL,
  `expensetypeName` varchar(100) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` smallint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expensetypes`
--

INSERT INTO `expensetypes` (`id`, `expensetypeName`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Water bills', 1, 1, 0, '2021-05-09 02:06:04', '2021-05-09 02:06:04'),
(2, 'Electricity bills', 1, 1, 0, '2021-05-09 02:06:22', '2021-05-09 02:06:22'),
(3, 'Transport', 1, 1, 0, '2021-05-14 10:34:04', '2021-05-14 10:34:04');

-- --------------------------------------------------------

--
-- Table structure for table `fcmmessages`
--

CREATE TABLE `fcmmessages` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `messageTitle` varchar(100) NOT NULL,
  `messageContent` varchar(255) NOT NULL,
  `methodName` varchar(30) NOT NULL,
  `dataArray` text NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `senderId` int(11) DEFAULT NULL,
  `userId` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `message` varchar(600) NOT NULL,
  `isRead` tinyint(3) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `senderId`, `userId`, `adminId`, `message`, `isRead`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 1, 'Petrol has reached re-order level', 0, 0, '2021-05-17 11:22:08', '2021-05-17 11:22:08'),
(2, NULL, 1, 1, 'Attempted to transfer ksh. 500 which is more than daily sales of Ksh. 0', 0, 0, '2021-05-18 02:19:13', '2021-05-18 02:19:13');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `paymentMethod` varchar(20) DEFAULT NULL,
  `status` enum('pending','packaging','shipping','delivered','cancelled') NOT NULL DEFAULT 'pending',
  `orderNo` varchar(20) NOT NULL,
  `sessionId` varchar(100) NOT NULL,
  `paid` varchar(10) NOT NULL DEFAULT 'pending',
  `paidOn` datetime DEFAULT NULL,
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `subtotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `taxamount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paiddebts`
--

CREATE TABLE `paiddebts` (
  `id` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `saleId` int(11) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL,
  `date` date NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paiddebts`
--

INSERT INTO `paiddebts` (`id`, `customerId`, `saleId`, `amount`, `date`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 5000, NULL, '1.00', '2021-05-16', 1, 1, 0, '2021-05-16 08:29:50', '2021-05-16 08:29:50'),
(2, 5000, NULL, '1.00', '2021-05-16', 1, 1, 0, '2021-05-16 08:30:37', '2021-05-16 08:30:37'),
(3, 5000, NULL, '1.00', '2021-05-16', 1, 1, 0, '2021-05-16 08:31:31', '2021-05-16 08:31:31'),
(4, 4, NULL, '5000.00', '2021-05-16', 1, 1, 0, '2021-05-16 08:33:02', '2021-05-16 08:33:02');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `packageId` int(11) DEFAULT NULL,
  `adminId` int(11) DEFAULT NULL,
  `patientId` int(11) DEFAULT NULL,
  `appointmentId` int(11) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `reference` varchar(50) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `fee` decimal(10,2) DEFAULT NULL,
  `method` varchar(20) DEFAULT NULL,
  `currency` varchar(5) NOT NULL,
  `MSISDN` varchar(20) DEFAULT NULL,
  `paidBy` varchar(50) DEFAULT NULL,
  `details` text,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `permissionName` varchar(30) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `permissionName`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'System Users', 0, '2021-02-10 13:32:56', '2021-04-24 10:01:50'),
(2, 'Tanks', 0, '2021-02-10 13:32:56', '2021-04-24 10:01:58'),
(3, 'Pumbs', 0, '2021-02-10 13:33:40', '2021-04-24 10:02:03'),
(4, 'Customers', 0, '2021-02-10 13:33:40', '2021-04-24 10:02:14'),
(5, 'Supervisors', 0, '2021-02-10 13:33:54', '2021-04-24 10:02:26'),
(6, 'Stock Management', 0, '2021-02-10 13:33:54', '2021-04-24 10:02:35'),
(7, 'Cashier', 0, '2021-02-10 13:34:11', '2021-04-24 10:03:02'),
(8, 'Products', 0, '2021-02-10 13:34:11', '2021-04-24 10:03:07'),
(9, 'Sales', 0, '2021-02-10 13:35:31', '2021-04-24 10:03:16'),
(10, 'Expenses', 0, '2021-02-10 13:35:31', '2021-04-24 10:03:25'),
(11, 'Expense Types', 0, '2021-02-10 13:36:01', '2021-04-24 10:03:32'),
(12, 'Account Transfers', 0, '2021-02-10 13:36:01', '2021-04-24 10:03:44'),
(13, 'Company Assets', 0, '2021-02-10 13:36:17', '2021-04-24 10:03:52'),
(14, 'Roles', 0, '2021-02-10 13:36:17', '2021-02-10 13:36:17'),
(15, 'Permissions', 0, '2021-02-10 13:36:24', '2021-02-10 13:36:24'),
(16, 'Accounts', 0, '2021-02-10 13:36:24', '2021-04-24 10:03:59'),
(17, 'Sales', 0, '2021-02-10 13:36:24', '2021-04-24 10:04:11'),
(18, 'Sales Report', 0, '2021-02-10 13:36:24', '2021-04-24 10:04:11'),
(19, 'Stock Report', 0, '2021-02-10 13:36:24', '2021-04-24 10:04:11'),
(20, 'View Customer', 0, '2021-02-10 13:36:24', '2021-04-24 10:04:11'),
(21, 'View Supplier', 0, '2021-02-10 13:36:24', '2021-04-24 10:04:11'),
(22, 'Readings', 0, '2021-02-10 13:36:24', '2021-04-24 10:04:11'),
(23, 'Categories', 0, '2021-02-10 13:36:24', '2021-04-24 10:04:11');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `productName` varchar(50) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `description` text,
  `unit` varchar(10) NOT NULL,
  `photoUrl` varchar(50) NOT NULL DEFAULT 'medicine.png',
  `alertLevel` double NOT NULL DEFAULT '0',
  `costPrice` decimal(10,2) NOT NULL,
  `sellPrice` decimal(10,2) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `productName`, `categoryId`, `description`, `unit`, `photoUrl`, `alertLevel`, `costPrice`, `sellPrice`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Petrol', 2, NULL, 'ltr', 'petrol_1620503796.jpg', 100, '100.00', '120.00', 1, 1, 0, '2021-05-09 00:56:36', '2021-05-16 08:16:30'),
(2, 'Disel', 2, NULL, 'ltr', 'petrol_1620506069.jpg', 100, '100.00', '110.00', 1, 1, 0, '2021-05-09 01:34:29', '2021-05-16 08:25:04'),
(3, 'SHELL Break Fluid', 3, NULL, 'ltr', 'shell_1620506362.', 10, '200.00', '100.00', 1, 1, 0, '2021-05-09 01:39:22', '2021-05-14 11:52:42'),
(4, 'Total Break Fluid', 3, NULL, 'ltr', 'shell_1620506430.', 10, '100.00', '120.00', 1, 1, 0, '2021-05-09 01:40:30', '2021-05-09 07:38:03'),
(5, 'gasoline', 2, NULL, 'gm', 'gasoline_1620969294.jpg', 1, '100.00', '100.00', 1, 1, 0, '2021-05-14 10:14:54', '2021-05-14 11:01:18'),
(6, 'Total gas', 4, NULL, 'Kg', 'zhanc shan_1620969467.jpg', 2, '900.00', '1500.00', 1, 1, 0, '2021-05-14 10:17:47', '2021-05-14 10:47:53'),
(7, 'Total gas', 4, NULL, 'Kg', 'totalgass_1620969846.Png', 2, '800.00', '900.00', 1, 1, 0, '2021-05-14 10:24:06', '2021-05-14 11:52:10');

-- --------------------------------------------------------

--
-- Table structure for table `pumps`
--

CREATE TABLE `pumps` (
  `id` int(11) NOT NULL,
  `pumpName` varchar(50) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pumps`
--

INSERT INTO `pumps` (`id`, `pumpName`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Pump 1', 1, 1, 0, '2021-05-09 00:10:50', '2021-05-09 00:10:50'),
(2, 'Pump 2', 1, 1, 0, '2021-05-09 00:15:40', '2021-05-09 00:15:40'),
(3, 'Pump 3', 1, 1, 0, '2021-05-14 10:25:00', '2021-05-14 10:25:00');

-- --------------------------------------------------------

--
-- Table structure for table `readings`
--

CREATE TABLE `readings` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `date` date NOT NULL,
  `meterReading` double NOT NULL,
  `tankId` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reportranges`
--

CREATE TABLE `reportranges` (
  `id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `customId` int(11) DEFAULT NULL,
  `fromdate` datetime NOT NULL,
  `todate` datetime NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `roleName` varchar(30) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `roleName`, `description`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', NULL, 1, 1, 0, '2021-03-02 19:55:14', '2021-03-02 19:55:14'),
(2, 'Supervisor', NULL, 1, 1, 0, '2021-03-02 19:55:14', '2021-04-24 07:01:23'),
(3, 'Pump Attendant', NULL, 1, 1, 0, '2021-05-16 16:19:02', '2021-05-16 16:19:02');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `unitCost` decimal(10,2) NOT NULL,
  `costPrice` decimal(10,2) DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `quantity` double NOT NULL,
  `shiftTypeId` int(11) NOT NULL,
  `cashQuantity` double NOT NULL DEFAULT '0',
  `creditQuantity` double NOT NULL DEFAULT '0',
  `attendantId` int(11) DEFAULT NULL,
  `plateNo` varchar(10) DEFAULT NULL,
  `date` date NOT NULL,
  `type` enum('Cash','Credit') NOT NULL DEFAULT 'Cash',
  `isPaid` varchar(10) NOT NULL DEFAULT 'Paid',
  `remarks` text NOT NULL,
  `tax` double NOT NULL DEFAULT '1',
  `taxAmount` decimal(10,2) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `categoryId`, `productId`, `total`, `unitCost`, `costPrice`, `discount`, `quantity`, `shiftTypeId`, `cashQuantity`, `creditQuantity`, `attendantId`, `plateNo`, `date`, `type`, `isPaid`, `remarks`, `tax`, `taxAmount`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 2, 2, '11000.00', '110.00', NULL, NULL, 100, 0, 0, 0, 4, NULL, '2021-05-16', 'Credit', 'Paid', '', 14, '1540.00', 1, 1, 0, '2021-05-16 08:28:16', '2021-05-16 08:28:16'),
(2, 2, 1, '2200.00', '120.00', NULL, '200.00', 20, 2, 10, 10, 42, NULL, '2021-05-17', 'Cash', 'Paid', '', 14, '308.00', 1, 1, 0, '2021-05-17 11:22:08', '2021-05-17 11:22:08');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `companyPhoto` varchar(50) NOT NULL DEFAULT 'company.jpg',
  `companyName` varchar(50) NOT NULL DEFAULT 'Our Awesome Health Centre',
  `companyTelephone` varchar(100) DEFAULT NULL,
  `companyEmail` varchar(100) DEFAULT NULL,
  `companyLocation` varchar(100) DEFAULT NULL,
  `companyAddress` varchar(40) NOT NULL,
  `companyAbout` varchar(500) NOT NULL,
  `balanceLimit` int(11) NOT NULL DEFAULT '10',
  `discount` double NOT NULL DEFAULT '0',
  `standardShippingCost` decimal(5,0) NOT NULL DEFAULT '100',
  `tax` double NOT NULL DEFAULT '16',
  `facebook` varchar(100) NOT NULL DEFAULT 'https://www.facebook.com/',
  `twitter` varchar(100) NOT NULL DEFAULT 'https://twitter.com/',
  `instagram` varchar(100) NOT NULL DEFAULT 'https://www.instagram.com/',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `adminId`, `companyPhoto`, `companyName`, `companyTelephone`, `companyEmail`, `companyLocation`, `companyAddress`, `companyAbout`, `balanceLimit`, `discount`, `standardShippingCost`, `tax`, `facebook`, `twitter`, `instagram`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, '84259.PNG', 'Gas and Petrol Management Ltd', '+254702066508', 'info@pgms.com', 'Upper Hill, Nairobi, Kenya', 'P.O Box 30305-00100', 'E-commerce solutions are the products and services that help a company conduct business electronically. The range of available e-commerce solutions is vast, including those that allow traditional businesses to design, create, and operate World Wide Web sites.', 10, 0, '100', 14, 'https://www.facebook.com/', 'https://twitter.com/', 'https://www.instagram.com/', 0, '2021-02-08 12:23:05', '2021-05-09 07:43:15');

-- --------------------------------------------------------

--
-- Table structure for table `shifts`
--

CREATE TABLE `shifts` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `date` date NOT NULL,
  `pumpId` int(11) NOT NULL,
  `type` enum('Day','Night') NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shifts`
--

INSERT INTO `shifts` (`id`, `userId`, `date`, `pumpId`, `type`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-05-08', 1, 'Day', 1, 1, 0, '2021-05-09 02:02:09', '2021-05-09 02:02:09'),
(2, 40, '2021-05-08', 2, 'Night', 1, 1, 0, '2021-05-09 02:03:19', '2021-05-09 02:03:19'),
(3, 41, '2021-05-14', 3, 'Day', 1, 1, 0, '2021-05-14 10:28:51', '2021-05-14 10:28:51');

-- --------------------------------------------------------

--
-- Table structure for table `shifttypes`
--

CREATE TABLE `shifttypes` (
  `id` int(11) NOT NULL,
  `shifttypeName` varchar(50) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shifttypes`
--

INSERT INTO `shifttypes` (`id`, `shifttypeName`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Shift One 6am-4pm', 1, 1, 0, '2021-05-16 19:29:35', '2021-05-16 19:29:35'),
(2, 'Shift Two 4pm-6am', 1, 1, 0, '2021-05-16 19:29:35', '2021-05-16 19:29:35');

-- --------------------------------------------------------

--
-- Table structure for table `statements`
--

CREATE TABLE `statements` (
  `id` int(11) NOT NULL,
  `accountId` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `type` varchar(20) NOT NULL,
  `description` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `customerId` int(11) DEFAULT NULL,
  `supplierId` int(11) DEFAULT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statements`
--

INSERT INTO `statements` (`id`, `accountId`, `amount`, `type`, `description`, `date`, `adminId`, `created_by`, `customerId`, `supplierId`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, '5000000.00', 'CreatedAccount', 'Created new account [KCB BANK]', '2021-05-16', 1, 1, 0, 0, 0, '2021-05-16 08:15:09', '2021-05-16 08:15:09'),
(2, 1, '5000.00', 'Payout', 'Paid supplier Ksh. 5000 from account [KCB BANK]', '2021-05-16', 1, 1, 0, 1, 0, '2021-05-16 08:16:30', '2021-05-16 08:16:30'),
(3, 1, '1000000.00', 'Withdraw', 'Withdrew cash amount [1000000] from [KCB BANK]', '2021-05-16', 1, 1, 0, 0, 0, '2021-05-16 08:18:29', '2021-05-16 08:18:29'),
(4, 1, '5000.00', 'Deposit', 'Customer Deposited Ksh. 5000 to account [KCB BANK]', '2021-05-16', 1, 1, 4, 0, 0, '2021-05-16 08:33:02', '2021-05-16 08:33:02'),
(5, 1, '5000.00', 'Withdraw', 'Withdrew cash amount [5000] from [KCB BANK]', '2021-05-17', 1, 1, 0, 0, 0, '2021-05-17 13:14:11', '2021-05-17 13:14:11'),
(6, 2, '200000.00', 'CreatedAccount', 'Created new account [EQUITY BANK]', '2021-05-18', 1, 1, 0, 0, 0, '2021-05-18 02:16:31', '2021-05-18 02:16:31'),
(7, 3, '10000.00', 'CreatedAccount', 'Created new account [M-PESA PAYBILL 25364]', '2021-05-18', 1, 1, 0, 0, 0, '2021-05-18 02:16:49', '2021-05-18 02:16:49'),
(8, 2, '690.00', 'Transfer', 'Admin transferred Ksh. 690 to account [EQUITY BANK]', '2021-05-18', 1, 1, 0, 0, 0, '2021-05-18 02:29:25', '2021-05-18 02:29:25');

-- --------------------------------------------------------

--
-- Table structure for table `stockbalances`
--

CREATE TABLE `stockbalances` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `balance` double NOT NULL,
  `date` date NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL,
  `supplierName` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `contacts` text,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `supplierName`, `email`, `contacts`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'VIVO ENERGY', 'vivoenergy@gmail.com', '0745367754', 1, 1, 0, '2021-05-08 23:44:27', '2021-05-08 23:44:27'),
(2, 'TOTAL', 'total@gmail.com', '0735648867', 1, 1, 0, '2021-05-08 23:45:25', '2021-05-08 23:45:25'),
(3, 'Royal Dutch Shell PLC', 'RoyaldutchshellPLC@gmail.com', '0745789236', 1, 1, 0, '2021-05-08 23:58:14', '2021-05-08 23:58:14'),
(4, 'OLA Energy Kenya Ltd', 'olanergykenya@gmail.com', '0756348901', 1, 1, 0, '2021-05-14 09:50:54', '2021-05-14 09:50:54'),
(5, 'KenolKobil Plc', 'kenolkobilplc@gmail.com', '0756257825', 1, 1, 0, '2021-05-14 09:52:05', '2021-05-14 09:52:05'),
(6, 'Canon Chemicals Ltd', 'canonchemicals@gmail.com', '0735783901', 1, 1, 0, '2021-05-14 09:53:42', '2021-05-14 09:53:42');

-- --------------------------------------------------------

--
-- Table structure for table `tanks`
--

CREATE TABLE `tanks` (
  `id` int(11) NOT NULL,
  `tankName` varchar(50) NOT NULL,
  `capacity` double NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '1',
  `adminId` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tanks`
--

INSERT INTO `tanks` (`id`, `tankName`, `capacity`, `status`, `adminId`, `created_by`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'Tank 1', 1000, 1, 1, 1, 0, '2021-05-09 00:16:51', '2021-05-09 00:16:51'),
(2, 'Tank 2', 2000, 1, 1, 1, 0, '2021-05-09 00:17:43', '2021-05-09 00:17:43'),
(3, 'Tank 3', 3000, 1, 1, 1, 0, '2021-05-14 10:25:28', '2021-05-14 10:25:28');

-- --------------------------------------------------------

--
-- Table structure for table `userpermissions`
--

CREATE TABLE `userpermissions` (
  `id` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  `adminId` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userpermissions`
--

INSERT INTO `userpermissions` (`id`, `roleId`, `permissionId`, `adminId`, `created_by`, `status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(2, 1, 2, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(3, 1, 3, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(4, 1, 4, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-14 09:27:17'),
(5, 1, 5, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(6, 1, 6, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-14 09:27:20'),
(7, 1, 7, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(8, 1, 8, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(9, 1, 9, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(10, 1, 10, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(11, 1, 11, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(12, 1, 12, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(13, 1, 13, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(14, 1, 14, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(15, 1, 15, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(16, 2, 1, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:19:24'),
(17, 2, 2, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(18, 2, 3, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:18:28'),
(19, 2, 4, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:20:52'),
(20, 2, 5, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(21, 2, 6, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:21:01'),
(22, 2, 7, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(23, 2, 8, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:20:54'),
(24, 2, 9, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(25, 2, 10, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(26, 2, 11, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(27, 2, 12, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(28, 2, 13, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 03:20:25'),
(29, 2, 14, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(30, 2, 15, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(31, 3, 1, 1, 1, 0, 1, '2021-02-11 03:27:02', '2021-02-11 03:33:25'),
(32, 3, 2, 1, 1, 0, 1, '2021-02-11 03:27:02', '2021-02-11 03:33:25'),
(33, 3, 3, 1, 1, 0, 1, '2021-02-11 03:27:02', '2021-02-11 03:33:25'),
(34, 7, 1, 1, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(35, 7, 2, 1, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(36, 7, 5, 1, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(37, 7, 6, 1, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(38, 7, 7, 1, 1, 1, 0, '2021-02-12 04:33:04', '2021-02-12 04:33:04'),
(39, 12, 0, 1, 1, 1, 0, '2021-02-12 08:22:31', '2021-02-12 08:22:31'),
(40, 13, 1, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(41, 13, 2, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(42, 13, 5, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(43, 13, 6, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(44, 13, 7, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(45, 13, 8, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(46, 13, 9, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(47, 13, 10, 1, 1, 1, 0, '2021-02-12 08:33:55', '2021-02-12 08:33:55'),
(48, 13, 11, 1, 1, 1, 0, '2021-02-12 08:33:56', '2021-02-12 08:33:56'),
(49, 13, 12, 1, 1, 1, 0, '2021-02-12 08:33:56', '2021-02-12 08:33:56'),
(50, 13, 13, 1, 1, 1, 0, '2021-02-12 08:33:56', '2021-02-12 08:33:56'),
(51, 14, 1, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(52, 14, 2, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(53, 14, 5, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(54, 14, 6, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(55, 14, 7, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(56, 14, 8, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(57, 14, 9, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(58, 14, 10, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(59, 14, 11, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(60, 14, 12, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(61, 14, 13, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(62, 14, 14, 1, 1, 1, 0, '2021-02-12 08:35:22', '2021-02-12 08:35:22'),
(63, 17, 1, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(64, 17, 2, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(65, 17, 5, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(66, 17, 6, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(67, 17, 7, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(68, 17, 8, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(69, 17, 9, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(70, 17, 10, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(71, 17, 11, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(72, 17, 12, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(73, 17, 13, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(74, 17, 14, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(75, 17, 15, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(76, 17, 16, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(77, 17, 17, 1, 1, 1, 0, '2021-02-12 11:48:12', '2021-02-12 11:48:12'),
(78, 19, 1, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(79, 19, 2, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(80, 19, 5, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(81, 19, 6, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(82, 19, 7, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(83, 19, 8, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(84, 19, 9, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(85, 19, 10, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(86, 19, 11, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(87, 19, 12, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(88, 19, 13, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(89, 19, 14, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(90, 19, 15, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(91, 19, 16, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(92, 19, 17, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(93, 19, 18, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(94, 19, 19, 1, 1, 1, 0, '2021-02-12 11:53:01', '2021-02-12 11:53:01'),
(95, 21, 1, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(96, 21, 2, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(97, 21, 5, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(98, 21, 6, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(99, 21, 7, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(100, 21, 8, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(101, 21, 9, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(102, 21, 10, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(103, 21, 11, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(104, 21, 12, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(105, 21, 13, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(106, 21, 14, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(107, 21, 15, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(108, 21, 16, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(109, 21, 17, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(110, 21, 18, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(111, 21, 19, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(112, 21, 20, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(113, 21, 21, 1, 1, 1, 0, '2021-02-12 11:56:23', '2021-02-12 11:56:23'),
(114, 22, 1, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(115, 22, 2, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(116, 22, 5, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(117, 22, 6, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(118, 22, 7, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(119, 22, 8, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(120, 22, 9, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(121, 22, 10, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(122, 22, 11, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(123, 22, 12, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(124, 22, 13, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(125, 22, 14, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(126, 22, 15, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(127, 22, 16, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(128, 22, 17, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(129, 22, 18, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(130, 22, 19, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(131, 22, 20, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(132, 22, 21, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(133, 22, 22, 1, 1, 1, 0, '2021-02-12 12:17:24', '2021-02-12 12:17:24'),
(134, 23, 1, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(135, 23, 2, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(136, 23, 5, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(137, 23, 6, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(138, 23, 7, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(139, 23, 8, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(140, 23, 9, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(141, 23, 10, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(142, 23, 11, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(143, 23, 12, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(144, 23, 13, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(145, 23, 14, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(146, 23, 15, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(147, 23, 16, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(148, 23, 17, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(149, 23, 18, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(150, 23, 19, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(151, 23, 20, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(152, 23, 21, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(153, 23, 22, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(154, 23, 23, 1, 1, 1, 0, '2021-02-12 12:19:04', '2021-02-12 12:19:04'),
(155, 1, 16, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(156, 1, 17, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(157, 1, 18, 1, NULL, 1, 0, '2021-02-11 05:15:56', '2021-02-11 05:15:56'),
(158, 6, 1, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(159, 6, 2, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(160, 6, 3, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(161, 6, 4, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(162, 6, 5, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(163, 6, 6, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(164, 6, 7, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(165, 6, 8, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(166, 6, 9, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(167, 6, 10, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(168, 6, 11, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(169, 6, 12, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(170, 6, 13, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(171, 6, 14, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(172, 6, 15, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(173, 6, 16, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(174, 6, 17, 1, 1, 0, 1, '2021-03-04 13:54:22', '2021-03-04 10:54:33'),
(175, 5, 1, 1, 1, 1, 0, '2021-03-04 11:04:32', '2021-03-04 11:04:32'),
(176, 5, 5, 1, 1, 1, 0, '2021-03-04 11:04:52', '2021-03-04 11:04:52'),
(177, 5, 7, 1, 1, 1, 0, '2021-03-04 11:04:55', '2021-03-04 11:04:55'),
(178, 3, 1, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(179, 3, 2, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(180, 3, 3, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(181, 3, 4, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(182, 3, 5, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(183, 3, 6, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(184, 3, 7, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(185, 3, 8, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(186, 3, 9, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(187, 3, 10, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(188, 3, 11, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(189, 3, 12, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(190, 3, 13, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(191, 3, 14, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(192, 3, 15, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(193, 3, 16, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(194, 3, 17, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(195, 3, 18, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(196, 3, 19, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(197, 3, 20, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(198, 3, 21, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(199, 3, 22, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02'),
(200, 3, 23, 1, 1, 1, 0, '2021-05-16 19:19:02', '2021-05-16 19:19:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `adminId` int(11) DEFAULT NULL,
  `firstName` varchar(20) NOT NULL,
  `middleName` varchar(20) DEFAULT NULL,
  `lastName` varchar(20) NOT NULL,
  `mobileNo` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `resetCode` varchar(100) DEFAULT NULL,
  `isVerified` tinyint(3) NOT NULL DEFAULT '0',
  `isVetted` tinyint(3) NOT NULL DEFAULT '0',
  `isDisabled` tinyint(3) NOT NULL DEFAULT '0',
  `isActive` tinyint(3) NOT NULL DEFAULT '0',
  `wardId` int(11) DEFAULT NULL,
  `userTypeId` varchar(20) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `roleId` tinyint(3) NOT NULL DEFAULT '1',
  `location` varchar(200) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(6) DEFAULT NULL,
  `patientNo` varchar(11) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `profilePic` varchar(30) NOT NULL DEFAULT 'profile.png',
  `countryId` int(11) NOT NULL DEFAULT '1',
  `townId` int(11) NOT NULL DEFAULT '1',
  `about` varchar(255) DEFAULT NULL,
  `isOnline` tinyint(3) NOT NULL DEFAULT '0',
  `companyId` int(11) DEFAULT NULL,
  `confirmpassword` varchar(100) DEFAULT NULL,
  `isDeleted` tinyint(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `adminId`, `firstName`, `middleName`, `lastName`, `mobileNo`, `email`, `password`, `resetCode`, `isVerified`, `isVetted`, `isDisabled`, `isActive`, `wardId`, `userTypeId`, `created_by`, `roleId`, `location`, `dob`, `gender`, `patientNo`, `latitude`, `longitude`, `profilePic`, `countryId`, `townId`, `about`, `isOnline`, `companyId`, `confirmpassword`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'Brian', NULL, 'Mulunda', '254724619842', 'medevkenya@gmail.com', '$2y$10$HxtagHZ0g0dbQPZ4yaVs9euua7vekftIEiG2rFpQNy76LzPB92gw.', '5a0917b6e81dc7dd29be2cf266c860f1', 1, 0, 0, 1, 0, 'Employer', NULL, 1, 'Nairobi, Kenya', '2021-03-03', NULL, NULL, -1.2920659, 36.8219462, '93599.jpg', 1, 1, 'Advertisement of all job categories\r\nA jobs advertisement company', 1, NULL, NULL, 0, '2020-08-09 05:47:37', '2021-05-18 01:30:14'),
(40, 1, 'Jacob', NULL, 'Mwilu', NULL, 'jacobmwilu@gmail.com', '123456', NULL, 0, 0, 0, 0, NULL, NULL, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, 'profile.png', 1, 1, NULL, 0, NULL, NULL, 0, '2021-05-09 02:02:59', '2021-05-09 02:02:59'),
(41, 1, 'Mike', NULL, 'Kibiko', NULL, 'mkibiko123@gmail.com', '123456', NULL, 0, 0, 0, 0, NULL, NULL, 1, 2, NULL, NULL, NULL, NULL, NULL, NULL, 'profile.png', 1, 1, NULL, 0, NULL, NULL, 0, '2021-05-14 10:28:22', '2021-05-14 10:28:22'),
(42, 1, 'Andrew', NULL, 'Mukoya', NULL, 'admin@admin.com', '123456', NULL, 0, 0, 0, 0, NULL, NULL, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL, 'profile.png', 1, 1, NULL, 0, NULL, NULL, 0, '2021-05-16 16:39:07', '2021-05-16 16:39:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounttransfers`
--
ALTER TABLE `accounttransfers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batches`
--
ALTER TABLE `batches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companyassets`
--
ALTER TABLE `companyassets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `creditpayments`
--
ALTER TABLE `creditpayments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customerconsumption`
--
ALTER TABLE `customerconsumption`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expensetypes`
--
ALTER TABLE `expensetypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fcmmessages`
--
ALTER TABLE `fcmmessages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paiddebts`
--
ALTER TABLE `paiddebts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pumps`
--
ALTER TABLE `pumps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `readings`
--
ALTER TABLE `readings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reportranges`
--
ALTER TABLE `reportranges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shifttypes`
--
ALTER TABLE `shifttypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statements`
--
ALTER TABLE `statements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stockbalances`
--
ALTER TABLE `stockbalances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tanks`
--
ALTER TABLE `tanks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userpermissions`
--
ALTER TABLE `userpermissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `accounttransfers`
--
ALTER TABLE `accounttransfers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `batches`
--
ALTER TABLE `batches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `companyassets`
--
ALTER TABLE `companyassets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `creditpayments`
--
ALTER TABLE `creditpayments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customerconsumption`
--
ALTER TABLE `customerconsumption`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expensetypes`
--
ALTER TABLE `expensetypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fcmmessages`
--
ALTER TABLE `fcmmessages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paiddebts`
--
ALTER TABLE `paiddebts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pumps`
--
ALTER TABLE `pumps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `readings`
--
ALTER TABLE `readings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reportranges`
--
ALTER TABLE `reportranges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `shifttypes`
--
ALTER TABLE `shifttypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `statements`
--
ALTER TABLE `statements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `stockbalances`
--
ALTER TABLE `stockbalances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tanks`
--
ALTER TABLE `tanks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `userpermissions`
--
ALTER TABLE `userpermissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
