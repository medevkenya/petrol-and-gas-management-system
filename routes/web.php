<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@signin');
Route::get('/signin', 'HomeController@signin');
Route::post('dosignin', 'HomeController@dosignin');
Route::get('login', [ 'as' => 'login', 'uses' => 'HomeController@signin']);
Route::get('/register', 'HomeController@register');
Route::post('doRegister', 'HomeController@doRegister');
Route::get('/logout', 'HomeController@logout');
Route::get('/activateAccount', 'HomeController@activateAccount');

Route::get('ResetPasswordLink/{PasswordConfirmation}/{email}', 'ForgotpasswordController@resetPasswordLink');
Route::get('forgotPassword', 'ForgotpasswordController@forgotPassword');
Route::post('resetPassword', 'ForgotpasswordController@resetPassword');
Route::post('DoResetMYPassword', 'ForgotpasswordController@doResetPassword');

Route::get('resendRecoveryEmail/{email}', 'ForgotpasswordController@resendRecoveryEmail');

Route::get('/terms', 'SiteController@terms');
Route::get('/privacy', 'SiteController@privacy');
Route::get('/help', 'SiteController@help');

Route::group(['middleware' => 'auth'], function () {

Route::get('dashboard', 'AccountController@dashboard');

Route::get('profile', 'AccountController@profile');
Route::post('updateProfile', 'AccountController@updateProfile');
Route::get('changepassword', 'AccountController@changepassword');
Route::post('doChangePass', 'AccountController@doChangePass');
Route::get('changePic', 'AccountController@changePic');
Route::post('UpdatePic', 'AccountController@UpdatePic')->name('UpdatePic');

Route::post('updateCompanyPhoto', 'SettingsController@updateCompanyPhoto')->name('updateCompanyPhoto');
Route::post('updateCompanyDetails', 'SettingsController@updateCompanyDetails');

Route::get('auditlogs', 'AccountController@auditlogs');

Route::get('roles', 'RolesController@roles');
Route::post('addrole', 'RolesController@addrole');
Route::post('deleterole', 'RolesController@deleterole');
Route::post('editrole', 'RolesController@editrole');

Route::get('batches', 'BatchesController@batches');
Route::post('addbatch', 'BatchesController@addbatch');
Route::post('deletebatch', 'BatchesController@deletebatch');
Route::post('editbatch', 'BatchesController@editbatch');
Route::get('stockreport', 'BatchesController@stockreport');
Route::get('filterstockreport', 'BatchesController@filterstockreport');
Route::post('postfilterstockreport', 'BatchesController@postfilterstockreport');
Route::get('postfilterstockreport', 'BatchesController@filterstockreport');

Route::post('postfilterstockreportsupplier', 'SuppliersController@postfilterstockreportsupplier');
Route::get('postfilterstockreportsupplier', 'SuppliersController@suppliers');

Route::post('updatequantity', 'BatchesController@updatequantity');

Route::get('exportfilterstockexcel/{fromdate}/{todate}', 'BatchesController@exportfilterstockexcel')->name('exportfilterstockexcel');
Route::get('exportfilterstockpdf/{fromdate}/{todate}', 'BatchesController@exportfilterstockpdf')->name('exportfilterstockpdf');

Route::post('addproduct', 'ProductsController@addproduct')->name('addproduct');
Route::post('editproduct', 'ProductsController@editproduct')->name('editproduct');

Route::get('getProducts', 'ProductsController@getProducts')->name('getProducts');
Route::get('getProduct', 'ProductsController@getProduct')->name('getProduct');

Route::get('getTanks', 'ProductsController@getTanks')->name('getTanks');

Route::get('products', 'ProductsController@products');
Route::post('deleteproduct', 'ProductsController@deleteproduct');
Route::get('viewProduct/{id}', 'ProductsController@viewProduct');

Route::post('ejectStock', 'ProductsController@ejectStock');

Route::post('postfilterproductsalereport', 'ProductsController@postfilterproductsalereport');

Route::get('salesReport', 'OrdersController@salesReport');
Route::post('postSalesReport', 'OrdersController@postSalesReport');
Route::get('postSalesReport', 'OrdersController@salesReport');
Route::post('deleteorder', 'OrdersController@deleteorder');

Route::get('categories', 'CategoriesController@categories');
Route::post('addcategory', 'CategoriesController@addcategory');
Route::post('deletecategory', 'CategoriesController@deletecategory');
Route::post('editcategory', 'CategoriesController@editcategory');

Route::get('users', 'UsersController@users');
Route::post('adduser', 'UsersController@adduser');
Route::post('deleteuser', 'UsersController@deleteuser');
Route::post('edituser', 'UsersController@edituser');

Route::get('viewAttendant/{id}', 'UsersController@viewAttendant');

Route::get('customers', 'CustomersController@customers');
Route::get('debtors', 'CustomersController@debtors');
Route::post('addcustomer', 'CustomersController@addcustomer');
Route::post('deletecustomer', 'CustomersController@deletecustomer');
Route::post('editcustomer', 'CustomersController@editcustomer');
Route::get('viewCustomer/{id}', 'CustomersController@viewCustomer');
Route::post('receivepayment', 'CustomersController@receivepayment');

Route::get('consumption', 'CustomersController@consumption');
Route::post('consumptionbyyear', 'CustomersController@consumptionbyyear');
Route::post('consumptionbymonth', 'CustomersController@consumptionbymonth');

Route::get('payments', 'CustomersController@payments');
Route::post('paymentsbyyear', 'CustomersController@paymentsbyyear');
Route::post('paymentsbymonth', 'CustomersController@paymentsbymonth');

Route::get('customerconsumption/{id}', 'CustomersController@customerconsumption');
Route::post('addconsumption', 'CustomersController@addconsumption');
Route::post('editconsumption', 'CustomersController@editconsumption');
Route::post('deleteconsumption', 'CustomersController@deleteconsumption');
Route::get('customerpayments/{id}', 'CustomersController@customerpayments');

Route::post('editdebt', 'CustomersController@editdebt');
Route::post('deletedebt', 'CustomersController@deletedebt');

Route::get('suppliers', 'SuppliersController@suppliers');
Route::get('creditors', 'SuppliersController@creditors');
Route::post('addsupplier', 'SuppliersController@addsupplier');
Route::post('deletesupplier', 'SuppliersController@deletesupplier');
Route::post('editsupplier', 'SuppliersController@editsupplier');
Route::get('viewSupplier/{id}', 'SuppliersController@viewSupplier');

Route::get('supplierbatches/{id}', 'SuppliersController@supplierbatches');
Route::get('supplierpayments/{id}', 'SuppliersController@supplierpayments');

Route::post('paysupplier', 'SuppliersController@paysupplier');
Route::post('editcredit', 'SuppliersController@editcredit');
Route::post('deletecredit', 'SuppliersController@deletecredit');

Route::get('tanks', 'TanksController@tanks');
Route::post('addtank', 'TanksController@addtank');
Route::post('deletetank', 'TanksController@deletetank');
Route::post('edittank', 'TanksController@edittank');

Route::get('pumps', 'PumpsController@pumps');
Route::post('addpump', 'PumpsController@addpump');
Route::post('deletepump', 'PumpsController@deletepump');
Route::post('editpump', 'PumpsController@editpump');

Route::post('editpumpreading', 'PumpsController@editpumpreading');

Route::get('shifttypes', 'ShifttypesController@shifttypes');
Route::post('addshifttype', 'ShifttypesController@addshifttype');
Route::post('deleteshifttype', 'ShifttypesController@deleteshifttype');
Route::post('editshifttype', 'ShifttypesController@editshifttype');

Route::get('accounts', 'AccountsController@accounts');
Route::post('addaccount', 'AccountsController@addaccount');
Route::post('deleteaccount', 'AccountsController@deleteaccount');
Route::post('editaccount', 'AccountsController@editaccount');
Route::post('withdrawcash', 'AccountsController@withdrawcash');
Route::get('viewAccount/{id}', 'AccountsController@viewAccount');

Route::get('statements', 'AccountsController@statements');

Route::post('accountpaymentsbymonth', 'AccountsController@accountpaymentsbymonth');

Route::get('readings', 'ReadingsController@readings');
Route::post('addreading', 'ReadingsController@addreading');
Route::post('deletereading', 'ReadingsController@deletereading');
Route::post('editreading', 'ReadingsController@editreading');

Route::get('accounttransfers', 'AccounttransfersController@accounttransfers');
Route::post('addaccounttransfer', 'AccounttransfersController@addaccounttransfer');
Route::post('deleteaccounttransfer', 'AccounttransfersController@deleteaccounttransfer');
Route::post('editaccounttransfer', 'AccounttransfersController@editaccounttransfer');

Route::post('postfiltertransfers', 'AccounttransfersController@postfiltertransfers');
Route::get('postfiltertransfers', 'AccounttransfersController@accounttransfers');

Route::post('postfiltertransfersbyaccount', 'AccounttransfersController@postfiltertransfersbyaccount');
Route::get('postfiltertransfersbyaccount', 'AccounttransfersController@accounts');

Route::get('shifts', 'ShiftsController@shifts');
Route::post('addshift', 'ShiftsController@addshift');
Route::post('deleteshift', 'ShiftsController@deleteshift');
Route::post('editshift', 'ShiftsController@editshift');

Route::get('expensetypes', 'ExpensetypesController@expensetypes');
Route::post('addexpensetype', 'ExpensetypesController@addexpensetype');
Route::post('deleteexpensetype', 'ExpensetypesController@deleteexpensetype');
Route::post('editexpensetype', 'ExpensetypesController@editexpensetype');

Route::get('companyassets', 'CompanyassetsController@companyassets');
Route::post('addcompanyasset', 'CompanyassetsController@addcompanyasset');
Route::post('deletecompanyasset', 'CompanyassetsController@deletecompanyasset');
Route::post('editcompanyasset', 'CompanyassetsController@editcompanyasset');

Route::get('expenses', 'ExpensesController@expenses');
Route::post('addexpense', 'ExpensesController@addexpense');
Route::post('deleteexpense', 'ExpensesController@deleteexpense');
Route::post('editexpense', 'ExpensesController@editexpense');
Route::post('postexpensesReport', 'ExpensesController@postexpensesReport');
Route::get('postexpensesReport', 'ExpensesController@expenses');

Route::get('permissions', 'PermissionsController@permissions');
Route::get('updatepermission', 'PermissionsController@updatepermission');

Route::post('flutterwaveverify', 'FlutterwaveController@flutterwaveverify')->name('flutterwaveverify');
Route::post('flutterwaveinitiate', 'FlutterwaveController@flutterwaveinitiate')->name('flutterwaveinitiate');

Route::get('notifications', 'NotificationsController@notifications');
Route::post('addnotification', 'NotificationsController@addnotification');

Route::get('makesales', 'SaleController@makesales');
Route::get('completesale', 'SaleController@completesale')->name('completesale');
Route::post('postfiltersalereport', 'SaleController@postfiltersalereport');
Route::get('postfiltersalereport', 'SaleController@sales');
Route::get('profitabilitysummary', 'SaleController@profitabilitysummary');
Route::post('searchprofitbyyear', 'SaleController@searchprofitbyyear');
Route::get('searchprofitbyyear', 'SaleController@profitabilitysummary');

Route::get('dailysales', 'SaleController@dailysales');
Route::get('searchdailysales', 'SaleController@dailysales');
Route::post('searchdailysales', 'SaleController@searchdailysales');

Route::post('searchprofitbymonth', 'SaleController@searchprofitbymonth');
Route::get('searchprofitbymonth', 'SaleController@profitabilitysummary');

Route::post('postfiltercustomersalereport', 'CustomersController@postfiltercustomersalereport');
Route::get('postfiltercustomersalereport', 'CustomersController@customers');

Route::get('exportfiltersaleexcel/{fromdate}/{todate}', 'SaleController@exportfiltersaleexcel')->name('exportfiltersaleexcel');
Route::get('exportfiltercustomersaleexcel/{customerId}/{fromdate}/{todate}', 'SaleController@exportfiltercustomersaleexcel')->name('exportfiltercustomersaleexcel');
Route::get('exportfiltersalepdf/{fromdate}/{todate}', 'SaleController@exportfiltersalepdf')->name('exportfiltersalepdf');

// Route::get('sales', 'SalesController@sales');
// Route::post('savecart', 'SalesController@savecart')->name('savecart');
// Route::get('getCart', 'SalesController@getCart')->name('getCart');
// Route::post('increaseCart', 'SalesController@increaseCart')->name('increaseCart');
// Route::post('decreaseCart', 'SalesController@decreaseCart')->name('decreaseCart');
// Route::get('completePrint', 'SalesController@completePrint')->name('completePrint');
// Route::post('search', 'SalesController@search');

});

//Cron
Route::get('cronsms', 'CronsController@cronsms');
