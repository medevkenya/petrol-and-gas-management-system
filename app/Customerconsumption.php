<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Userpermissions;
use App\Activities;

class Customerconsumption extends Model
{
    protected $table = 'customerconsumption';

    public static function getAll()
    {
        return Customerconsumption::select('customerconsumption.*','customers.name')
        ->leftJoin('customers','customerconsumption.customerId','customers.id')
        ->where('customerconsumption.adminId',Auth::user()->adminId)->where('customerconsumption.isDeleted',0)->orderBy('customerconsumption.id','DESC')->get();
    }

    public static function getAllByDate($date)
    {
        // return Customerconsumption::select('customerconsumption.*','customers.name')
        // ->leftJoin('customers','customerconsumption.customerId','customers.id')
        // ->where('customerconsumption.adminId',Auth::user()->adminId)
        // ->where('customerconsumption.date',$date)
        // ->where('customerconsumption.isDeleted',0)->orderBy('customerconsumption.id','DESC')->get();
        return Customerconsumption::selectRaw("SUM(amount) as total_debit")
        ->selectRaw("customerId")->selectRaw("qty")
        ->where('date',$date)
    ->groupBy('customerId')
    ->get();
    }

    public static function storeone($customerId,$amount,$qty,$productId)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Customerconsumption;
        $model->customerId = $customerId;
        $model->amount = $amount;
        $model->productId = $productId;
        $model->qty = $qty;
        $model->date = date('Y-m-d');
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
        Activities::saveLog("Added new customer consumption [".$customerId."]");
          return true;
        }

        return false;

    }

    public static function updateone($id,$amount,$qty)
    {
        $model = Customerconsumption::find($id);
        $model->amount = $amount;
        $model->qty = $qty;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited customer consumption [".$id."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Customerconsumption::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted customer consumption [".$id."]");
            return true;
        }
        return false;
    }

}
