<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Reportranges;
use App\Sales;
use Auth;
use DB;

class FilterSaleExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $details = Reportranges::select('fromdate','todate')->where('created_by',Auth::user()->id)->where('isDeleted',0)->orderBy('id','DESC')->limit(1)->first();
        return Sales::select('products.productName','products.unit','sales.total','sales.unitCost','sales.discount','sales.quantity','sales.type','customers.customerName', DB::raw("DATE_FORMAT(sales.created_at, '%d-%m-%Y %H:%i') as formatted_date"))
  			->leftJoin('products','sales.productId','=','products.id')
  			->leftJoin('categories','sales.categoryId','=','categories.id')
  			->leftJoin('customers','sales.customerId','=','customers.id')
  			->where('sales.adminId',Auth::user()->adminId)
  			->whereBetween('sales.created_at', [$details->fromdate, $details->todate])
  			->where('sales.isDeleted',0)
  			->orderBy('sales.id','DESC')->get();
    }

    public function headings(): array
    {
        return [
            'Product',
            'Unit',
            'Total Amount (Ksh)',
            'Unit Cost',
            'Discount (Ksh)',
            'Quantity',
            'Type',
            'Customer',
            'Date'
        ];
    }

}
