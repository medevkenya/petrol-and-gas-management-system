<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Batches;

class FilterStockExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Batches::getAll();
    }

    public function headings(): array
    {
        return [
            'id',
            'name',
        ];
    }

}
