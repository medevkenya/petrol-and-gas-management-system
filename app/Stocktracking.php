<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Products;

class Stocktracking extends Model
{
    protected $table = 'stocktracking';

    public static function getAll() {
      return $list = Stocktracking::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function updateStocktracking($productId)
    {

        $date = date('Y-m-d');
        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;

        $check = Stocktracking::where('date',$date)->where('productId',$productId)->first();
        if($check)
        {

          $checkp = Products::where('id',$productId)->first();
          $closingstock = $checkp->stockbalance;

          $model = Stocktracking::where('id',$check->id)->update(['closingstock'=>$closingstock]);

        }
        else {

          $checkp = Products::where('id',$productId)->first();
          $openingstock = $checkp->stockbalance;

          $model = new Stocktracking;
          $model->openingstock = $openingstock;
          $model->closingstock = $openingstock;
          $model->productId = $productId;
          $model->date = $date;
          $model->adminId = $adminId;
          $model->created_by = $created_by;
          $model->save();
        }

        if ($model)
        {
          Activities::saveLog("Added new Stock tracking [".$productId."]");
          return true;
        }

        return false;

    }

    public static function updateStocktrackingBatch($productId,$quantity)
    {

        $date = date('Y-m-d');
        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;

        $check = Stocktracking::where('date',$date)->where('productId',$productId)->first();
        if($check)
        {

          $checkp = Products::where('id',$productId)->first();
          $closingstock = $checkp->stockbalance;

          $model = Stocktracking::where('id',$check->id)->update(['closingstock'=>$closingstock]);

        }
        else {

          $checkp = Products::where('id',$productId)->first();
          $openingstock = $checkp->stockbalance;

          $model = new Stocktracking;
          $model->openingstock = $openingstock;
          $model->closingstock = $openingstock;
          $model->productId = $productId;
          $model->date = $date;
          $model->adminId = $adminId;
          $model->created_by = $created_by;
          $model->save();
        }

        if ($model)
        {
          Activities::saveLog("Added new Stock tracking [".$productId."]");
          return true;
        }

        return false;

    }

    public static function updateStocktrackingSale($productId,$quantity)
    {

        $date = date('Y-m-d');
        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;

        $check = Stocktracking::where('date',$date)->where('productId',$productId)->first();
        if($check)
        {

          $checkp = Products::where('id',$productId)->first();
          $closingstock = $checkp->stockbalance - $quantity;

          $model = Stocktracking::where('id',$check->id)->update(['closingstock'=>$closingstock]);

        }
        else {

          $checkp = Products::where('id',$productId)->first();
          $openingstock = $checkp->stockbalance;

          $model = new Stocktracking;
          $model->openingstock = $openingstock;
          $model->closingstock = $openingstock - $quantity;
          $model->productId = $productId;
          $model->date = $date;
          $model->adminId = $adminId;
          $model->created_by = $created_by;
          $model->save();
        }

        if ($model)
        {
          Activities::saveLog("Added new Stock tracking [".$productId."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $productId)
    {
        $model = Stocktracking::find($id);
        $model->openingstock = $openingstock;
        $model->closingstock = $closingstock;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited stock tracking [".$productId."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Stocktracking::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted stock tracking [".$id."]");
            return true;
        }
        return false;
    }

}
