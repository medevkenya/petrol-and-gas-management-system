<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Shifts;
use App\Pumpreadings;

class Readings extends Model
{
    protected $table = 'readings';

    public static function pumpMeterReading($shiftId,$date) {

      $checkshift = Shifts::where('id',$shiftId)->first();
      $pumpId = $checkshift->pumpId;
      $shiftTypeId = $checkshift->shiftTypeId;

      $last = Pumpreadings::where('pumpId',$pumpId)
      ->where('shiftTypeId',$shiftTypeId)->where('date',$date)
      ->where('isDeleted',0)->orderBy('id','DESC')->limit(1)->first();
      if($last) {
      return array('openReading'=>$last->openReading,'closeReading'=>$last->closeReading);
    }
    else {
      return array('openReading'=>null,'closeReading'=>null);
    }
    }

    // public static function PreviousMeterReading($shiftId) {
    //   $last = Readings::where('productId',$productId)->where('isDeleted',0)->orderBy('id','DESC')->limit(1)->first();
    //   if($last) {
    //     $id = $last->id - 1;
    //     $previos = Readings::where('id',$id)->first();
    //     if($previos) {
    //   return $previos->meterReading;
    // }
    // else {
    //   return 0;
    // }
    // }
    // else {
    //   return 0;
    // }
    // }

    public static function getAll() {
      return $list = Readings::select('readings.*','tanks.tankName')
      ->leftJoin('tanks','readings.tankId','=','tanks.id')
      ->where('readings.adminId',Auth::user()->adminId)->where('readings.isDeleted',0)->orderBy('readings.id','DESC')->get();
    }

    public static function storeone($meterReading,$tankId)
    {

        //$openingReading = 0;

        // $check = Readings::where('adminId',Auth::user()->adminId)->where('productId',$productId)->where('categoryId',$categoryId)
        // ->where('isDeleted',0)->orderBy('id','DESC')->first();
        // if($check) {
        //   $openingReading = $check->meterReading;
        // }

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Readings;
        $model->tankId = $tankId;
        $model->meterReading = $meterReading;
        $model->date = date('Y-m-d');
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new reading [".$meterReading."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $meterReading, $tankId)
    {
        $model = Readings::find($id);
        $model->tankId = $tankId;
        $model->meterReading = $meterReading;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited reading [".$meterReading."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Readings::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted reading [".$id."]");
            return true;
        }
        return false;
    }

}
