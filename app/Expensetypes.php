<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Costs;
use App\Activities;

class Expensetypes extends Model
{
    protected $table = 'expensetypes';

    public static function getAll() {
      return $list = Expensetypes::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function storeone($expensetypeName)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Expensetypes;
        $model->expensetypeName = $expensetypeName;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new expense type [".$expensetypeName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $expensetypeName)
    {
        $model = Expensetypes::find($id);
        $model->expensetypeName = $expensetypeName;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited expense type [".$expensetypeName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Expensetypes::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted expense type [".$id."]");
            return true;
        }
        return false;
    }

}
