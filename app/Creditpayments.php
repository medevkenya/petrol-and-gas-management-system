<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Accounts;

class Creditpayments extends Model
{
    protected $table = 'creditpayments';

    public static function getAll($supplierId)
    {

        if($supplierId == 0)
        {
          return Creditpayments::select('creditpayments.*','suppliers.supplierName')
          ->leftJoin('suppliers','creditpayments.supplierId','=','suppliers.id')
          ->where('creditpayments.isDeleted',0)
          ->orderBy('creditpayments.id','DESC')->get();
        }
        else
        {
          return Creditpayments::select('creditpayments.*','suppliers.supplierName')
          ->leftJoin('suppliers','creditpayments.supplierId','=','suppliers.id')
          ->where('creditpayments.supplierId',$supplierId)
          ->where('creditpayments.isDeleted',0)
          ->orderBy('creditpayments.id','DESC')->get();
        }

    }

    public static function paysupplier($supplierId,$amount,$accountId)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Creditpayments;
        $model->supplierId = $supplierId;
        $model->amount = $amount;
        $model->date = date('Y-m-d');
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Accounts::paysupplier($amount,$accountId);
          return true;
        }

        return false;

    }

    public static function editcredit($id, $amount)
    {
        $model = Creditpayments::find($id);
        $model->amount = $amount;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited credit payment [".$id."]");
            return true;
        }
        return false;
    }

    public static function deletecredit($id)
    {
        $model = Creditpayments::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted credit payment [".$id."]");
            return true;
        }
        return false;
    }

}
