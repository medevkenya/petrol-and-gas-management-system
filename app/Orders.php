<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Cart;
use Log;
use Session;

class Orders extends Model
{
    protected $table = 'orders';

    public static function getAll() {
      return $list = Orders::select('orders.*','users.firstName','users.lastName')
      ->leftJoin('users','orders.userId','=','users.id')->where('orders.paid','paid')
      ->where('orders.adminId',Auth::user()->adminId)->where('orders.isDeleted',0)->orderBy('orders.id','DESC')->paginate(20);
    }

    public static function deleteone($id)
    {
        $model = Orders::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted order [".$id."]");
            return true;
        }
        return false;
    }

    public static function saveNew($subtotal,$total,$discount,$tax,$taxamount)
{
     $nextId = 1;

     $lastId = Orders::latest()->whereRaw('Date(created_at) = CURDATE()')->first();
     if($lastId) {
       $nextId = $lastId->id+1;
     }

      $ii= strtoupper(str_shuffle(time().time()."abcdefghijklmnopqrstuvwxyz"));
      $orderNo = substr($ii, -6)."-".$nextId;

      if(Auth::user()) {
        $userId = Auth::user()->id;
      }
      else {
        $userId = 0;
      }

      $model = new Orders;
      $model->userId = $userId;
      $model->orderNo = $orderNo;
      $model->sessionId = Session::getId();
      $model->subtotal = $subtotal;
      $model->total = $total;
      $model->tax = $tax;
      $model->taxamount = $taxamount;
      $model->discount = $discount;
      $model->paid = "paid";
      $model->paidOn = date('Y-m-d H:i');
      $model->adminId = Auth::user()->adminId;
      $model->created_by = Auth::user()->id;
      $model->save();
      if($model) {
        return $model->id;
      }
      else {
        return false;
      }

}

}
