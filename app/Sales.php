<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Costs;
use App\Activities;
use App\Stockbalances;
use App\Batches;
use App\Settings;
use App\Products;
use App\Notifications;
use App\Stocktracking;
use Log;

class Sales extends Model
{

    protected $table = 'sales';

    public static function completesale($total,$cashQuantity,$productId,$discount,$categoryId,$creditQuantity,$shiftId)
    {

        $pDetails = Products::select('id','sellPrice')->where('id',$productId)->first();
        $unitCost = $pDetails->sellPrice;
        $costPrice = $pDetails->costPrice;

        $settings = Settings::getDetails();

        $taxAmount = $total * ($settings->tax / 100);

        $quantity = $creditQuantity + $cashQuantity;

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Sales;
        $model->categoryId = $categoryId;
        $model->productId = $productId;
        $model->shiftId = $shiftId;
        $model->total = $total;
        $model->discount = $discount;
        $model->unitCost = $unitCost;
        $model->costPrice = $costPrice;
        $model->quantity = $quantity;
        $model->creditQuantity = $creditQuantity;
        $model->cashQuantity = $cashQuantity;
        $model->taxAmount = $taxAmount;
        $model->tax = $settings->tax;
        $model->date = date('Y-m-d');
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if($model)
        {

          Stocktracking::updateStocktrackingSale($productId,$quantity);

          Batches::reduceProductBalance($productId,$quantity);

          $updatebatch = self::updateProductStock($productId,$quantity);
          if($updatebatch)
          {
            $balance = Batches::getProductQuantity($productId);
            Stockbalances::saveone($productId,$balance);
          }
          Activities::saveLog("Added new sale [".$productId."]");
          return true;
        }

        return false;

    }

    public static function updateProductStock($productId,$quantity)
    {

        $check = Batches::where('productId',$productId)->where('status','In Stock')->where('isDeleted',0)->orderBy('id','ASC')->first();
        if($check)
        {
          log::info("batch id--".$check->id);
          $checkdetails = Batches::where('id',$check->id)->first();
            $sold = $checkdetails->sold + $quantity;
            log::info("cart quantity--".$quantity);
            log::info("batch new sold--".$sold);
            Batches::where('id',$checkdetails->id)->update(['sold'=>$sold]);

        $checkdetails2 = Batches::where('id',$check->id)->first();
        if($checkdetails2->sold >= $checkdetails2->quantity)
        {

            Batches::where('id',$checkdetails->id)->update(['status'=>'Out of Stock']);
            //$message = $productDetails->productName." has run out of stock";
            //Notifications::storeone($message);
        }

      }

      $productDetails = Products::where('id',$productId)->first();
      $balance = Batches::getProductQuantity($productId);
      if($balance <= $productDetails->alertLevel)
      {
          $message = $productDetails->productName." has reached re-order level";
          Notifications::storeone($message);
      }

    }

    public static function updateone($id,$productId,$customerId,$quantity,$discount,$type)
    {

      if($discount == null || empty($discount)) {
        $discount = 0;
      }

        $batchDetails = Sales::select('id','unitCost')->where('id',$is)->where('isDeleted',0)->orderBy('id','DESC')->first();
        $unitCost = $batchDetails->unitCost;
        $total = ($unitCost * $quantity) - $discount;

        $model = Sales::find($id);
        $model->total = $total;
        $model->productId = $productId;
        $model->customerId = $customerId;
        $model->discount = $discount;
        $model->quantity = $quantity;
        $model->type = $type;
        $model->save();
        if ($model)
        {
            Activities::saveLog("Edited sale [".$productId."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Sales::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted sale [".$id."]");
            return true;
        }
        return false;
    }

    public static function processDates($data)
    {
      $day = "";
      $total = "";
      foreach ($data as $key)
      {
        $day .= date('l', strtotime($key['date'])).",";
        $total .= $key['total'].",";
      }
      if (substr($day, -1, 1) == ',')
      {
              $day = substr($day, 0, -1);
      }
      if (substr($total, -1, 1) == ',')
      {
              $total = substr($total, 0, -1);
      }

        $finaldays = implode(',', array_unique(explode(',', $day)));

        $finaldays = explode(',', $day);
        $strday = "'".implode("','", $finaldays)."'";

        $finaltotals = explode(',', $total);
        $str = "'".implode("','", $finaltotals)."'";
        return array('day'=>$strday,'total'=>ltrim($str, '"'));
    }

}
