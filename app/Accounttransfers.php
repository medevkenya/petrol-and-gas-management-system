<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Accounts;

class Accounttransfers extends Model
{
  
    protected $table = 'accounttransfers';

    public static function getAll() {
      return Accounttransfers::select('accounttransfers.*','accounts.accountName')
      ->leftJoin('accounts','accounttransfers.accountId','=','accounts.id')
      ->where('accounttransfers.adminId',Auth::user()->adminId)->where('accounttransfers.isDeleted',0)->orderBy('accounttransfers.id','DESC')->get();
    }

    public static function getAllByAccount($accountId) {
      return Accounttransfers::select('accounttransfers.*','accounts.accountName')
      ->leftJoin('accounts','accounttransfers.accountId','=','accounts.id')
      ->where('accounttransfers.accountId',$accountId)
      ->where('accounttransfers.isDeleted',0)
      ->orderBy('accounttransfers.id','DESC')->get();
    }

    public static function storeone($accountId,$amount,$type="Deposit",$reason)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Accounttransfers;
        $model->accountId = $accountId;
        $model->amount = $amount;
        $model->type = $type;
        $model->reason = $reason;
        $model->date = date('Y-m-d');
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Accounts::transferfunds($amount,$accountId);
          Activities::saveLog("Added new account transfer [".$amount."] Reason : ".$reason);
          return true;
        }

        return false;

    }

    public static function updateone($id,$amount,$reason)
    {
        $model = Accounttransfers::find($id);
        $model->amount = $amount;
        $model->reason = $reason;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited account transfer [".$amount."] Reason: ". $reason);
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Accounttransfers::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted account transfer [".$id."]");
            return true;
        }
        return false;
    }

}
