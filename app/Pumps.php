<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Shifttypes;
use App\Pumpreadings;

class Pumps extends Model
{
    protected $table = 'pumps';

    public static function getAll() {
      return $list = Pumps::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function seedAll() {
      $list = Pumps::getAll();
  		$shifttypes = Shifttypes::getAll();

  		$adminId	= Auth::user()->adminId;
  		$created_by	= Auth::user()->id;

  		//$pumpreadings = Pumpreadings::where('date',date('Y-m-d'))->where('isDeleted',0)->first();
  		//if(!$pumpreadings) {
  			foreach ($list as $key) {
  				foreach ($shifttypes as $keytype) {

            $check = Pumpreadings::where('pumpId',$key->id)->where('shiftTypeId',$keytype->id)->where('date',date('Y-m-d'))->where('isDeleted',0)->first();
            if(!$check) {
  					$model = new Pumpreadings;
  	        $model->pumpId = $key->id;
  	        $model->shiftTypeId = $keytype->id;
  	        $model->date = date('Y-m-d');
  	        $model->adminId = $adminId;
  	        $model->created_by = $created_by;
  	        $model->save();
          }
  				}
  			}
  		//}
    }

    public static function storeone($pumpName)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Pumps;
        $model->pumpName = $pumpName;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new pump [".$pumpName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $pumpName)
    {
        $model = Pumps::find($id);
        $model->pumpName = $pumpName;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited pump [".$pumpName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Pumps::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted pump [".$id."]");
            return true;
        }
        return false;
    }

}
