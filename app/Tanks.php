<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Tanks extends Model
{
    protected $table = 'tanks';

    public static function getAll() {
      return $list = Tanks::select('tanks.*','products.productName')
      ->leftJoin('products','tanks.productId','=','products.id')
      ->where('tanks.adminId',Auth::user()->adminId)
      ->where('tanks.isDeleted',0)
      ->orderBy('tanks.id','DESC')
      ->get();
    }

    public static function storeone($tankName,$capacity,$productId)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Tanks;
        $model->tankName = $tankName;
        $model->capacity = $capacity;
        $model->productId = $productId;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new tank [".$tankName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $tankName,$capacity)
    {
        $model = Tanks::find($id);
        $model->tankName = $tankName;
        $model->capacity = $capacity;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited tank [".$tankName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Tanks::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted tank [".$id."]");
            return true;
        }
        return false;
    }

}
