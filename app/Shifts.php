<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Shifts extends Model
{
    protected $table = 'shifts';

    public static function getAll() {
      return $list = Shifts::select('shifts.*','pumps.pumpName','users.firstName','users.lastName','shifttypes.shifttypeName')
      ->leftJoin('pumps','shifts.pumpId','=','pumps.id')
      ->leftJoin('users','shifts.userId','=','users.id')
      ->leftJoin('shifttypes','shifts.shiftTypeId','=','shifttypes.id')
      ->where('shifts.adminId',Auth::user()->adminId)->where('shifts.isDeleted',0)->orderBy('shifts.id','DESC')->get();
    }

    public static function getAllPumpAttendants() {
        return Shifts::select('shifts.*','pumps.pumpName','users.firstName','users.lastName','shifttypes.shifttypeName')
        ->leftJoin('pumps','shifts.pumpId','=','pumps.id')
        ->leftJoin('users','shifts.userId','=','users.id')
        ->leftJoin('shifttypes','shifts.shiftTypeId','=','shifttypes.id')
        ->where('shifts.adminId',Auth::user()->adminId)
        ->where('shifts.date',date('Y-m-d'))->where('shifts.isDeleted',0)->get();
    }

    public static function getShiftDetails($shiftId) {
        return Shifts::select('shifts.*','pumps.pumpName','users.firstName','users.lastName','shifttypes.shifttypeName')
        ->leftJoin('pumps','shifts.pumpId','=','pumps.id')
        ->leftJoin('users','shifts.userId','=','users.id')
        ->leftJoin('shifttypes','shifts.shiftTypeId','=','shifttypes.id')
        ->where('shifts.id',$shiftId)
        ->first();
    }

    public static function storeone($userId,$pumpId,$shiftTypeId)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Shifts;
        $model->userId = $userId;
        $model->pumpId = $pumpId;
        $model->shiftTypeId = $shiftTypeId;
        $model->date = date('Y-m-d');
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new shift [".$userId."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $userId, $pumpId, $shiftTypeId)
    {
        $model = Shifts::find($id);
        $model->userId = $userId;
        $model->pumpId = $pumpId;
        $model->shiftTypeId = $shiftTypeId;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited shift [".$userId."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Shifts::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted shift [".$id."]");
            return true;
        }
        return false;
    }

}
