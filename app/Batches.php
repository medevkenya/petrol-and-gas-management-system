<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Cart;
use App\Notifications;
use App\Products;
use App\Accounts;
use App\Stocktracking;
use Log;

class Batches extends Model
{
    protected $table = 'batches';

    public static function getAll() {
      return $list = Batches::select('batches.*','tanks.tankName','products.productName','products.unit','categories.categoryName','suppliers.supplierName')
      ->leftJoin('suppliers','batches.supplierId','=','suppliers.id')
      ->leftJoin('tanks','batches.tankId','=','tanks.id')
      ->leftJoin('products','batches.productId','=','products.id')
      ->leftJoin('categories','products.categoryId','=','categories.id')
      ->where('batches.adminId',Auth::user()->adminId)
      //->where('batches.status','In Stock')
      ->where('batches.isDeleted',0)
      ->orderBy('batches.id','DESC')->get();
    }

    public static function getBatchesBySupplierId($supplierId) {
      $currentMonth = date('m');
      return $list = Batches::select('batches.*','tanks.tankName','products.productName','products.unit','categories.categoryName','suppliers.supplierName')
      ->leftJoin('suppliers','batches.supplierId','=','suppliers.id')
      ->leftJoin('tanks','batches.tankId','=','tanks.id')
      ->leftJoin('products','batches.productId','=','products.id')
      ->leftJoin('categories','products.categoryId','=','categories.id')
      ->where('batches.supplierId',$supplierId)
      ->where('batches.adminId',Auth::user()->adminId)
      ->whereRaw('MONTH(batches.created_at) = ?',[$currentMonth])
      ->where('batches.type','Credit')
      //->where('batches.status','In Stock')
      ->where('batches.isDeleted',0)
      ->orderBy('batches.id','DESC')->get();
    }

    public static function getAllProducts() {
      return $list = Batches::select('batches.*','tanks.tankName','products.productName','products.unit','categories.categoryName','suppliers.supplierName')
      ->leftJoin('suppliers','batches.supplierId','=','suppliers.id')
      ->leftJoin('tanks','batches.tankId','=','tanks.id')
      ->leftJoin('products','batches.productId','=','products.id')
      ->leftJoin('categories','products.categoryId','=','categories.id')
      ->where('batches.adminId',Auth::user()->adminId)
      //->where('batches.status','In Stock')
      ->where('batches.isDeleted',0)
      ->groupBy('batches.productId')
      ->orderBy('batches.id','DESC')->get();
    }

    public static function getProductStockValue($productId) {
      $total = 0;
      $quantity = 0;
      $list = Batches::where('productId',$productId)->where('status','In Stock')->where('isDeleted',0)->get();
      foreach ($list as $key) {
        $quantity += $key->quantity - $key->sold;
        $total += $key->purchasePrice * $quantity;
      }
      return array('total'=>$total,'quantity'=>$quantity);
    }

    public static function getTotalStockValue() {
      $total = 0;
      $quantity = 0;
      $list = Batches::where('status','In Stock')->where('isDeleted',0)->get();
      foreach ($list as $key) {
        $quantity += $key->quantity - $key->sold;
        $total += $key->purchasePrice * $quantity;
      }
      return array('total'=>$total,'quantity'=>$quantity);
    }

    public static function getProductQuantity($productId)
    {

      $quantity = Batches::where('productId',$productId)->where('status','In Stock')->where('isDeleted',0)->sum('quantity');
      $sold = Batches::where('productId',$productId)->where('status','In Stock')->where('isDeleted',0)->sum('sold');
      $bal = $quantity - $sold;
      if($bal > 0) {
        return $bal;
      }
      else {
        return 0;
      }
    }

    public static function storeone($supplierId,$productId,$quantity,$categoryId,$type,$purchasePrice,$tankId,$accountId)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Batches;
        $model->supplierId = $supplierId;
        $model->purchasePrice = $purchasePrice;
        $model->productId = $productId;
        $model->categoryId = $categoryId;
        $model->quantity = $quantity;
        $model->total = $quantity * $purchasePrice;
        $model->type = $type;
        $model->tankId = $tankId;
        $model->date = date('Y-m-d');
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {

          self::updateProductCostPrice($productId,$purchasePrice);

          self::updateProductBalance($productId,$quantity);

          Stocktracking::updateStocktrackingBatch($productId,$quantity);

          if($type == "Cash" && $accountId != null) {
            $amount = $quantity * $purchasePrice;
            Accounts::paysupplier($amount,$accountId,$supplierId);
          }

          Activities::saveLog("Added new batch [".$supplierId."]");
          return true;

        }

        return false;

    }

    public static function updateProductBalance($productId,$quantity) {
      $check = Products::where('id',$productId)->first();
      $stockbalance = $check->stockbalance + $quantity;
      Products::where('id',$productId)->update(['stockbalance'=>$stockbalance]);
    }

    public static function reduceProductBalance($productId,$quantity) {
      $check = Products::where('id',$productId)->first();
      $stockbalance = $check->stockbalance - $quantity;
      Products::where('id',$productId)->update(['stockbalance'=>$stockbalance]);
      return true;
    }

    public static function updateProductCostPrice($productId) {
      $check = Batches::where('productId',$productId)->where('status','In Stock')->where('isDeleted',0)->orderBy('id','ASC')->first();
      if($check) {
        $costPrice = $check->purchasePrice;
      }
      else {
        $costPrice = $purchasePrice;
      }

      Products::where('id',$productId)->update(['costPrice'=>$costPrice]);
    }

    public static function ejectStock($id,$quantity) {
      // $check = Batches::where('id',$id)->first();
      // $productId = $check->productId;
      // $newquantity = $check->quantity - $quantity;
      // $res = Batches::where('id',$id)->update(['quantity'=>$newquantity]);
      $res = self::reduceProductBalance($id,$quantity);
      if($res) {
        //self::reduceProductBalance($productId,$quantity);
        Stocktracking::updateStocktrackingSale($id,$quantity);
        return true;
      }
      return false;
    }

    // public static function updatequantity($supplierId,$productId,$quantity,$type,$purchasePrice,$tankId)
    // {
    //   $details = Products::where('id',$productId)->first();
    //     $adminId	= Auth::user()->adminId;
    //     $created_by	= Auth::user()->id;
    //     $model = new Batches;
    //     $model->supplierId = $supplierId;
    //     $model->purchasePrice = $purchasePrice;
    //     $model->productId = $productId;
    //     $model->categoryId = $details->categoryId;
    //     $model->quantity = $quantity;
    //     $model->total = $quantity * $purchasePrice;
    //     $model->type = $type;
    //     $model->tankId = $tankId;
    //     $model->adminId = $adminId;
    //     $model->created_by = $created_by;
    //     $model->save();
    //     if ($model)
    //     {
    //       self::updateProductCostPrice($productId,$purchasePrice);
    //       Activities::saveLog("Added new batch [".$supplierId."]");
    //       return true;
    //     }
    //
    //     return false;
    //
    // }

    // public static function updateone($id,$supplierId,$productId,$quantity,$categoryId)
    // {
    //     $model = Batches::find($id);
    //     $model->supplierId = $supplierId;
    //     $model->productId = $productId;
    //     $model->categoryId = $categoryId;
    //     $model->quantity = $quantity;
    //     $model->total = $quantity * $model->purchasePrice;
    //     $model->save();
    //     if ($model) {
    //         Activities::saveLog("Edited batch [".$supplierId."]");
    //         return true;
    //     }
    //     return false;
    // }

    public static function deleteone($id)
    {

        $model = Batches::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
          self::reduceProductBalance($model->productId,$model->quantity);
          Stocktracking::updateStocktrackingSale($model->productId,$model->quantity);
          Activities::saveLog("Deleted batch [".$id."]");
          return true;
        }

        return false;

    }

    public static function updateProduct($orderId)
    {

      $list = Cart::where('orderId',$orderId)->where('isDeleted',0)->get();
      foreach ($list as $key) {

        $check = Batches::where('productId',$key->productId)->where('status','In Stock')->where('isDeleted',0)->orderBy('id','ASC')->first();
        if($check)
        {
          log::info("batch id--".$check->id);
          $checkdetails = Batches::where('id',$check->id)->first();
            $sold = $checkdetails->sold + $key->quantity;
            log::info("cart quantity--".$key->quantity);
            log::info("batch new sold--".$sold);
            Batches::where('id',$checkdetails->id)->update(['sold'=>$sold]);
            if($sold >= $checkdetails->quantity)
            {
                Batches::where('id',$checkdetails->id)->update(['status'=>'Out of Stock']);
                $message = $key->itemName." has run out of stock";
                Notifications::storeone($message);
            }
        }

      }

    }

}
