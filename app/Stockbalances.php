<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Stockbalances extends Model
{
    protected $table = 'stockbalances';

    public static function saveone($productId,$balance)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Stockbalances;
        $model->productId = $productId;
        $model->balance = $balance;
        $model->date = date('Y-m-d');
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          return true;
        }

        return false;

    }

}
