<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Notifications extends Model
{
    protected $table = 'notifications';

    public static function countToday() {
      return Notifications::where('adminId',Auth::user()->adminId)->where('isDeleted', 0)->whereRaw('Date(created_at) = CURDATE()')->count();
    }

    public static function getLatest() {
      return Notifications::select('users.firstName','users.lastName','notifications.message','notifications.created_at','users.profilePic')
      ->leftJoin('users','users.id','=','notifications.userId')
      ->where('notifications.adminId',Auth::user()->adminId)
      ->where('notifications.isDeleted',0)
      ->orderBy('notifications.id','DESC')
      ->take(10)
      ->get();
    }

    public static function getAll()
    {
        return Notifications::select('users.firstName','users.lastName','notifications.message','notifications.created_at','users.profilePic')
        ->leftJoin('users','users.id','=','notifications.userId')
        ->where('notifications.adminId',Auth::user()->adminId)
        ->where('notifications.isDeleted',0)
        ->orderBy('notifications.id','DESC')
        ->get();
    }

    public static function storeone($message)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Notifications;
        $model->message = $message;
        $model->adminId = $adminId;
        $model->userId = $created_by;
        $model->save();
        if ($model)
        {
          return true;

        }

        return false;

    }

}
