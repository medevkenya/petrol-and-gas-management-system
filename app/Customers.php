<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Userpermissions;
use App\Activities;
use App\Customerconsumption;

class Customers extends Model
{
    protected $table = 'customers';

    public static function getAll()
    {
        return Customers::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function getStatements($customerId) {
      $firstDayofThisMonth = date('Y-m-01');
      $currentMonth = date('m');

      $statements = [];
      if($customerId == 0) {
      $customers = Customers::getAll();
    }
    else {
      $customers = Customers::where('id',$customerId)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }
      foreach ($customers as $keystat) {
        $customerName = $keystat->customerName;

        $currentMonthDebt = Customerconsumption::where('customerId',$keystat->id)->whereRaw('MONTH(created_at) = ?',[$currentMonth])->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('amount');
        $debtPaidThisMonth = Paiddebts::where('customerId',$keystat->id)->whereRaw('MONTH(created_at) = ?',[$currentMonth])->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('amount');

        $amountOwedBeforeThisMonth = Customerconsumption::where('customerId',$keystat->id)->where('date','<',$firstDayofThisMonth)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('amount');
        $amountPaidBeforeThisMonth = Paiddebts::where('customerId',$keystat->id)->where('date','<',$firstDayofThisMonth)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('amount');

        if($amountOwedBeforeThisMonth > 0) {
          $balanceBroughtForward = $amountOwedBeforeThisMonth - $amountPaidBeforeThisMonth;
        }
        else {
          $balanceBroughtForward = 0;
        }

        $netDebt = ($balanceBroughtForward + $currentMonthDebt) - $debtPaidThisMonth;

        $statements[] = array('customerId'=>$keystat->id,'customerName'=>$customerName,'balanceBroughtForward'=>number_format($balanceBroughtForward,2),'currentMonthDebt'=>$currentMonthDebt,'debtPaidThisMonth'=>$debtPaidThisMonth,'netDebt'=>number_format($netDebt,2));
      }
      return $statements;
    }

    public static function storeone($customerName,$email,$contacts)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Customers;
        $model->customerName = $customerName;
        $model->email = $email;
        $model->contacts = $contacts;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
        Activities::saveLog("Added new customer [".$customerName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $customerName,$email,$contacts)
    {
        $model = Customers::find($id);
        $model->customerName = $customerName;
        $model->email = $email;
        $model->contacts = $contacts;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited customer [".$customerName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Customers::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted customer [".$id."]");
            return true;
        }
        return false;
    }

}
