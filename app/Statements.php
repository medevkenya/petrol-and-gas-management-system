<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Accounts;

class Statements extends Model
{
    protected $table = 'statements';

    public static function getTotalAcountForDate($id,$date) {
      return Statements::where('accountId',$id)
      ->where('date',$date)
      ->where('type','Deposit')
      ->where('isDeleted',0)
      ->orwhere('accountId',$id)
      ->where('date',$date)
      ->where('type','Transfer')
      ->where('isDeleted',0)
      ->sum('amount');
    }

    public static function getTotalForDate($date) {
      return Statements::where('date',$date)
      ->where('type','Deposit')
      ->where('isDeleted',0)
      ->orwhere('date',$date)
      ->where('type','Transfer')
      ->where('isDeleted',0)
      ->sum('amount');
    }

    public static function getTotalAcountForMonth($id,$month) {
      return Statements::where('accountId',$id)
      ->whereMonth('created_at', $month)
      ->where('type','Deposit')
      ->where('isDeleted',0)
      ->orwhere('accountId',$id)
      ->whereMonth('created_at', $month)
      ->where('type','Transfer')
      ->where('isDeleted',0)
      ->sum('amount');
    }

    public static function getAll() {
      return Statements::select('statements.*','accounts.accountName')
      ->leftJoin('accounts','statements.accountId','=','accounts.id')
      ->where('statements.adminId',Auth::user()->adminId)
      ->where('statements.isDeleted',0)
      ->orderBy('statements.id','DESC')
      ->get();
    }

    public static function storeone($accountId,$amount,$type,$description,$customerId,$supplierId)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Statements;
        $model->accountId = $accountId;
        $model->amount = $amount;
        $model->type = $type;
        $model->supplierId = $supplierId;
        $model->customerId = $customerId;
        $model->description = $description;
        $model->adminId = $adminId;
        $model->date = date('Y-m-d');
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new statement accountId [".$accountId."]");
          return true;
        }

        return false;

    }

    public static function deleteone($id)
    {
        $model = Statements::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted statement [".$id."]");
            return true;
        }
        return false;
    }

}
