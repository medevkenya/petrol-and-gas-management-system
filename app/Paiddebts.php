<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Accounts;

class Paiddebts extends Model
{
    protected $table = 'paiddebts';

    public static function getAll($customerId)
    {

        if($customerId == 0)
        {
          return Paiddebts::select('paiddebts.*','customers.customerName')
          ->leftJoin('customers','paiddebts.customerId','=','customers.id')
          ->where('paiddebts.isDeleted',0)
          ->orderBy('paiddebts.id','DESC')->get();
        }
        else
        {
          return Paiddebts::select('paiddebts.*','customers.customerName')
          ->leftJoin('customers','paiddebts.customerId','=','customers.id')
          ->where('paiddebts.customerId',$customerId)
          ->where('paiddebts.isDeleted',0)
          ->orderBy('paiddebts.id','DESC')->get();
        }

    }

    public static function receivepayment($amount,$accountId,$customerId)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Paiddebts;
        $model->customerId = $customerId;
        $model->amount = $amount;
        $model->date = date('Y-m-d');
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Accounts::receivepayment($amount,$accountId,$customerId);
          return true;
        }

        return false;

    }

    public static function editdebt($id, $amount)
    {
        $model = Paiddebts::find($id);
        $model->amount = $amount;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited debt payment [".$id."]");
            return true;
        }
        return false;
    }

    public static function deletedebt($id)
    {
        $model = Paiddebts::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted debt payment [".$id."]");
            return true;
        }
        return false;
    }

}
