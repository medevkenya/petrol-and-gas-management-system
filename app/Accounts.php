<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Accounttransfers;
use App\Statements;
use Mail;

class Accounts extends Model
{
    protected $table = 'accounts';

    public static function getAll() {
      return $list = Accounts::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function storeone($accountName,$balance)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Accounts;
        $model->accountName = $accountName;
        $model->balance = $balance;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          $description = "Created new account [".$accountName."]";
          Activities::saveLog($description);
          Statements::storeone($model->id,$balance,"CreatedAccount",$description,0,0);
          return true;
        }

        return false;

    }

    public static function updateone($id, $accountName)
    {
        $model = Accounts::find($id);
        $model->accountName = $accountName;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited account [".$accountName."]");
            return true;
        }
        return false;
    }

    public static function withdrawcash($id, $amount,$reason)
    {
      $check = Accounts::where('id',$id)->first();
      $bala = $check->balance - $amount;
        $model = Accounts::find($id);
        $model->balance = $bala;
        $model->save();
        if ($model) {
            Accounttransfers::storeone($id,$amount,"Withdraw",$reason);
            $description = "Withdrew cash amount [".$amount."] from [".$check->accountName."]";
            Activities::saveLog($description);
            Statements::storeone($id,$amount,"Withdraw",$description,0,0);
            self::notifyAdmins($amount,"Withdraw Cash",$description,$reason);
            return true;
        }
        return false;
    }

    public static function notifyAdmins($amount,$type,$description,$reason) {

      $userId = Auth::user()->id;
      $userDetails = User::where('id',$userId)->first();

      $users = User::where('roleId',1)->where('isDeleted',0)->get();
      foreach ($users as $keye) {
        $datanot=array('name'=>$keye->firstName." ".$keye->lastName,'amount'=>$amount,'type'=>$type,'description'=>$description,'reason'=>$reason);

        $email = $keye->email;
        $type = $type." By ".$userDetails->firstName." ".$userDetails->lastName;

         // Mail::send('emails.notifyadmins', $datanot, function($message) use ($email,$type) {
         // $message->to($email)->subject($type);
         // $message->from(env('MAIL_FROM_ADDRESS'),env('MAIL_FROM_NAME'));
         // });

      }

    }

    public static function deleteone($id)
    {
        $model = Accounts::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted account [".$id."]");
            return true;
        }
        return false;
    }

    public static function receivepayment($amount,$id,$customerId) {
      $check = Accounts::where('id', $id)->first();
      $model = Accounts::find($id);
      $model->balance = $check->balance + $amount;
      $model->save();
      if ($model) {
          $description = "Customer Deposited Ksh. ".$amount." to account [".$check->accountName."]";
          Activities::saveLog($description);
          Statements::storeone($id,$amount,"Deposit",$description,$customerId,0);
          return true;
      }
      return false;
    }

    public static function transferfunds($amount,$id) {
      $check = Accounts::where('id', $id)->first();
      $model = Accounts::find($id);
      $model->balance = $check->balance + $amount;
      $model->save();
      if ($model) {
          $description = "Admin transferred Ksh. ".$amount." to account [".$check->accountName."]";
          Activities::saveLog($description);
          Statements::storeone($id,$amount,"Transfer",$description,0,0);
          return true;
      }
      return false;
    }

    public static function paysupplier($amount,$id,$supplierId) {
      $check = Accounts::where('id', $id)->first();
      $model = Accounts::find($id);
      $model->balance = $check->balance - $amount;
      $model->save();
      if ($model) {
          $description = "Paid supplier Ksh. ".$amount." from account [".$check->accountName."]";
          Activities::saveLog($description);
          Statements::storeone($id,$amount,"Payout",$description,0,$supplierId);
          return true;
      }
      return false;
    }

}
