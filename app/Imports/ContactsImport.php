<?php
namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Contacts;
use App\Groups;
use App\SMS;
use App\Settings;
use Auth;

class ContactsImport implements ToCollection
{

  //protected $guarded = ['contactName'];
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {

        $check = Groups::where('adminId',Auth::user()->adminId)->where('created_by',Auth::user()->id)->where('isDeleted',0)->orderBy('id','DESC')->limit(1)->first();
        $groupId = $check->id;

        $data = [];
        $count = 0;

        foreach ($rows as $row)
        {
            $count++;
            $data[] = array(
                  'contactName'     => $row[1],
                  'mobileNo'    => $row[0],
                  'adminId'    => Auth::user()->adminId,
                  'created_by'    => Auth::user()->id,
                  'groupId'     => $groupId
                );
        }



        									if(!empty($data)){
        											Contacts::insert($data);

                              return true;

        									}

                          return false;

        // $excelCount = count($row);
        //
        // return new Contacts([
        //     'contactName'     => $row[0],
        //     'mobileNo'    => $row[1],
        //     'adminId'    => Auth::user()->adminId,
        //     'created_by'    => Auth::user()->id,
        //     'groupId'     => $groupId,
        //     'excelCount'     => $excelCount
        // ]);

    }

}
