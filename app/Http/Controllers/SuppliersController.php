<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Suppliers;
use App\Batches;
use App\Creditpayments;
use App\Accounts;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class SuppliersController extends Controller {

	public function suppliers()
	{
		$list = Suppliers::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
		$statements = Suppliers::getStatements(0);
		return view('suppliers',['list'=>$list,'statements'=>$statements]);
	}

	public function addsupplier(Request $request)
	{
			$adminId	= Auth::user()->adminId;
			$supplierName = $request->supplierName;
			$email = $request->email;
			$contacts = $request->contacts;

			$check = Suppliers::where('supplierName', $supplierName)->where('adminId',$adminId)->where('isDeleted', 0)->first();
			if ($check) {
					return Redirect::back()->with(['status0'=>'Supplier name already exists.']);
			} else {
					$add = Suppliers::storeone($supplierName,$email,$contacts);
					if ($add) {
							return Redirect::back()->with(['status1'=>'New supplier was created successfully.']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while creating supplier.']);
					}
			}
	}


	public function editsupplier(Request $request)
	{
			$id = $request->id;
			$supplierName = $request->supplierName;
			$email = $request->email;
			$contacts = $request->contacts;

					$update = Suppliers::updateone($id, $supplierName,$email,$contacts);
					if ($update) {
							return Redirect::back()->with(['status1'=>'The record was updated successfully']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while updating record']);
					}

	}

	public function deletesupplier(Request $request)
	{
			$id = $request->id;
			$delete = Suppliers::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

	public function viewSupplier($supplierId)
	{
		$supplierDetails = Suppliers::where('id',$supplierId)->first();
		if($supplierDetails) {
			$paidcredits = Creditpayments::getAll($supplierId);
			$statements = Suppliers::getStatements($supplierId);
		return view('viewSupplier',['supplierId'=>$supplierId,'statements'=>$statements,'supplierDetails'=>$supplierDetails,'paidcredits'=>$paidcredits]);
	}
	return Redirect::back()->with(['status0'=>'Invalid supplier']);
	}

	public function postfilterstockreportsupplier(Request $request)
	{
		$supplierDetails = Suppliers::where('id',$request->supplierId)->first();
		if($supplierDetails) {
			$fromdate = $request->fromdate;
			$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
			$fromdate = date("Y-m-d", strtotime($fromdate))." 23:59:59";
			$todate = $request->todate;
			$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
			$todate = date("Y-m-d", strtotime($todate))." 23:59:59";

				$list = Batches::select('batches.*','products.productName','products.unit','categories.categoryName','suppliers.supplierName')
	      ->leftJoin('suppliers','batches.supplierId','=','suppliers.id')
	      ->leftJoin('products','batches.productId','=','products.id')
	      ->leftJoin('categories','products.categoryId','=','categories.id')
				->where('batches.supplierId',$request->supplierId)
	      ->where('batches.adminId',Auth::user()->adminId)
	      //->where('batches.status','In Stock')
				->whereBetween('batches.created_at', [new Carbon($fromdate), new Carbon($todate)])
				->where('batches.isDeleted',0)
				->orderBy('batches.id','DESC')
				->get();

				$paidcredits = Creditpayments::getAll($request->supplierId);
				$statements = Suppliers::getStatements($request->supplierId);

			return view('viewSupplier',['list'=>$list,'statements'=>$statements,'supplierId'=>$request->supplierId,'supplierDetails'=>$supplierDetails,'paidcredits'=>$paidcredits,'fromdate'=>$fromdate,'todate'=>$todate]);
		}
		return Redirect::back()->with(['status0'=>'Invalid supplier']);
	}

	public function paysupplier(Request $request)
	{
			$id = $request->id;
			$amount = $request->amount;

			$check = Accounts::where('id', $request->accountId)->first();
			if ($check && $check->balance < $amount) {
					return Redirect::back()->with(['status0'=>'Insufficient balance in '.$check->accountName.'']);
			} else {

			$delete = Creditpayments::paysupplier($amount,$request->accountId,$id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Received payment successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while receiving payment']);
			}

		}
	}

	public function editcredit(Request $request)
	{
			$id = $request->id;
			$amount = $request->amount;
			$delete = Creditpayments::editcredit($id,$amount);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Updated payment successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating payment']);
			}
	}

	public function deletecredit(Request $request)
	{
			$id = $request->id;
			$delete = Creditpayments::deletecredit($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Deleted payment successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting payment']);
			}
	}

	public function supplierbatches($supplierId)
	{

		$supplierDetails = Suppliers::where('id',$supplierId)->first();
		if($supplierDetails)
		{
			$list = Batches::getBatchesBySupplierId($supplierId);
			return view('supplierbatches',['supplierId'=>$supplierId,'list'=>$list,'supplierDetails'=>$supplierDetails]);
		}

		return Redirect::back()->with(['status0'=>'Invalid supplier']);

	}

	public function supplierpayments($supplierId)
	{
		$currentMonth = date('m');
		$supplierDetails = Suppliers::where('id',$supplierId)->first();
		if($supplierDetails)
		{
			$paidcredits = Creditpayments::select('creditpayments.*','suppliers.supplierName')
			->leftJoin('suppliers','creditpayments.supplierId','=','suppliers.id')
			->where('creditpayments.supplierId',$supplierId)
			->whereRaw('MONTH(creditpayments.created_at) = ?',[$currentMonth])
			->where('creditpayments.isDeleted',0)
			->orderBy('creditpayments.id','DESC')->get();
			return view('supplierpayments',['supplierId'=>$supplierId,'supplierDetails'=>$supplierDetails,'paidcredits'=>$paidcredits]);
		}

		return Redirect::back()->with(['status0'=>'Invalid supplier']);

	}

}
