<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Tanks;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class TanksController extends Controller {

	public function tanks()
	{
		$list = Tanks::getAll();
		return view('tanks',['list'=>$list]);
	}

	public function addtank(Request $request)
	{
			$tankName = $request->tankName;
			$capacity = $request->capacity;
			$productId = $request->productId;
			$checkexists = Tanks::where('productId',$productId)->where('isDeleted',0)->first();
			if($checkexists) {
				return Redirect::back()->with(['status0'=>'Tank for product already exists']);
			}
			else
			{
				$add = Tanks::storeone($tankName,$capacity,$productId);
				if ($add) {
						return Redirect::back()->with(['status1'=>'New tank was created successfully.']);
				} else {
						return Redirect::back()->with(['status0'=>'Error occurred while creating tank.']);
				}
		}
	}

	public function edittank(Request $request)
	{

			$id = $request->id;
			$tankName = $request->tankName;
			$capacity = $request->capacity;
			//$productId = $request->productId;

				$update = Tanks::updateone($id,$tankName,$capacity);
				if ($update) {
						return Redirect::back()->with(['status1'=>'The tank was updated successfully']);
				} else {
						return Redirect::back()->with(['status0'=>'Error occurred while updating tank']);
				}

	}

	public function deletetank(Request $request)
	{
			$id = $request->id;
			$delete = Tanks::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
