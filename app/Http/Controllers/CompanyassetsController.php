<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Companyassets;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class CompanyassetsController extends Controller {

	public function companyassets()
	{
		$list = Companyassets::getAll();
		return view('companyassets',['list'=>$list]);
	}

	public function addcompanyasset(Request $request)
	{
			$name = $request->name;
			$quantity = $request->quantity;
			$amount = $request->amount;

			$checkexists = Companyassets::where('name',$request->name)->where('isDeleted',0)->first();
			if($checkexists) {
				return Redirect::back()->with(['status0'=>'Sorry, asset already exists']);
			}
			else {

			$add = Companyassets::storeone($name,$quantity,$amount);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New companyasset was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating companyasset.']);
			}

		}

	}


	public function editcompanyasset(Request $request)
	{
			$id = $request->id;
			$name = $request->name;
			$quantity = $request->quantity;
			$amount = $request->amount;
			$update = Companyassets::updateone($id, $name,$quantity,$amount);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The companyasset was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating companyasset']);
			}

	}

	public function deletecompanyasset(Request $request)
	{
			$id = $request->id;
			$delete = Companyassets::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
