<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Accounts;
use App\Accounttransfers;
use App\Statements;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class AccountsController extends Controller {

	public function accounts()
	{

		$currentMonth = date('m');

// $yearlist = Statements::select(
// 				DB::raw('sum(amount) as sums'),
// 				DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
// )
// ->whereYear('created_at', date('Y'))
// ->groupBy('monthKey')
// ->orderBy('created_at', 'ASC')
// ->get();

$monthlist = Statements::select(
			DB::raw('sum(amount) as sumsday'),
			DB::raw("DATE_FORMAT(created_at,'%d') as dayKey")
)
->whereMonth('created_at', date('m'))
->whereYear('created_at', date('Y'))
->groupBy('dayKey')
->orderBy('created_at', 'ASC')
->get();
		return view('accounts',['monthlist'=>$monthlist,'currentMonth'=>$currentMonth]);
	}

	public function accountpaymentsbymonth(Request $request)
	{

		$currentMonth = $request->month;

$monthlist = Statements::select(
			DB::raw('sum(amount) as sumsday'),
			DB::raw("DATE_FORMAT(created_at,'%d') as dayKey")
)
->whereMonth('created_at', $currentMonth)
->whereYear('created_at', date('Y'))
->groupBy('dayKey')
->orderBy('created_at', 'ASC')
->get();
		return view('accounts',['monthlist'=>$monthlist,'currentMonth'=>$currentMonth]);
	}


	public function statements()
	{
		$list = Statements::getAll();
		return view('statements',['list'=>$list]);
	}

	public function viewAccount($id)
	{
		$list = Accounttransfers::getAllByAccount($id);
		$details = Accounts::where('id',$id)->where('isDeleted',0)->first();
		if($details) {
			return view('viewAccount',['list'=>$list,'details'=>$details]);
		}
		else {
			return Redirect::back()->with(['status0'=>'Invalid request']);
		}
	}

	public function addaccount(Request $request)
	{
			$accountName = $request->accountName;
			$balance = $request->balance;
			$checkexists = Accounts::where('accountName',$request->accountName)->where('isDeleted',0)->first();
			if($checkexists) {
				return Redirect::back()->with(['status0'=>'Sorry, account name already exists']);
			}
			else
			{

			$add = Accounts::storeone($accountName,$balance);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New account was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating account.']);
			}

		}

	}


	public function editaccount(Request $request)
	{
			$id = $request->id;
			$accountName = $request->accountName;

			$update = Accounts::updateone($id, $accountName);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The account was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating account']);
			}

	}

	public function withdrawcash(Request $request)
	{
			$id = $request->id;
			$amount = $request->amount;
			$reason = $request->reason;

			$checkexists = Accounts::where('id',$request->id)->where('balance','<',$request->amount)->where('isDeleted',0)->first();
			if($checkexists) {
				return Redirect::back()->with(['status0'=>'Sorry, insufficient balance']);
			}
			else
			{

			$update = Accounts::withdrawcash($id, $amount,$reason);
			if ($update) {
					return Redirect::back()->with(['status1'=>'Withdrew cash successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while withdrawing cash']);
			}

		}

	}

	public function deleteaccount(Request $request)
	{
			$id = $request->id;
			$delete = Accounts::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
