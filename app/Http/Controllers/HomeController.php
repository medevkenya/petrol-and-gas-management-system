<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\User;
use Mail;
use Hash;
use Auth;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Intervention\Image\Facades\Image as Image;
use Log;

class HomeController extends Controller
{

    public function signin()
    {
        return view('signin');
    }

    public function dosignin(Request $request)
    {

        request()->validate([
            'email' => 'required|email',
            'password' => 'required|min:6',
        ], [
            'email.required' => 'Email is required',
        ]);

        $checkuser = User::where('email',request()->email)->first();
				if($checkuser) {

          if($checkuser->isVerified == 0)
          {
            return redirect()->back()->withInput($request->input())->with(['error'=>'Your account is not activated. Please check your email and click on the activation link.']);
          }
          else
          {

          $isDisabled = $checkuser->isDisabled;
          if($isDisabled == 0)
          {

            if (Auth::attempt(['email' => request()->email, 'password' => request()->password])) {
      				   User::where('email', request()->email)->update(['isOnline' => 1]);
                 return redirect()->intended('dashboard');
      			}
            else
            {
      				   return redirect()->back()->withInput($request->input())->with(['error'=>'Login failed. Wrong email or password']);
      			}

      }
      else {
          return redirect()->back()->withInput($request->input())->with(['error'=>'This account is blocked. Please contact the site administrator at admin@rentpoa.com']);
      }
}
      }
      else {
          return redirect()->back()->withInput($request->input())->with(['error'=>'Email address not found']);
      }

    }

    public function register()
    {
        return view('register');
    }

    public function doRegister(Request $request)
    {
      //https://stackoverflow.com/questions/51962486/datepicker-set-maximum-date-to-18-years-ago-from-current-date
        request()->validate([
            'firstName' => 'required|min:3|max:50',
            'lastName' => 'required|min:3|max:50',
            'mobileNo' => 'required|numeric|regex:/(07)[0-9]{8}/',// input starts with 01 and is followed by 9 numbers
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'cpassword' => 'required|min:6|max:20|same:password',
            'terms' => 'required',
        ], [
            'firstName.required' => 'First Name is required',
            'firstName.min' => 'First Name must be at least 3 characters.',
            'firstName.max' => 'First Name should not be greater than 20 characters.',
            'lastName.required' => 'Last Name is required',
            'lastName.min' => 'Last Name must be at least 3 characters.',
            'lastName.max' => 'Last Name should not be greater than 20 characters.',
            'mobileNo.required' => 'Mobile Number is required',
            'mobileNo.regex' => 'Mobile number should be 10 digits starting with 07',
            'password.required' => 'Please enter your password',
            'cpassword.required' => 'Please re-enter your password',
            'terms.required' => 'Please accept our terms of service',
        ]);

        // $input = request()->except('password','confirm_password');
        // $user=new User($input);
        // $user->password=bcrypt(request()->password);
        // $user->save();
        //request()->firstName,request()->lastName,request()->mobileNo,,request()->password
        $res = User::registerUser(request()->all());
        if($res) {
          return Redirect::to('login')->with('success', 'Successfully registration. Please check your email for an activation link & click it');
        }
        else {
          return redirect()->back()->withInput($request->input())->with('error', 'Registration failed. Please try again');
        }

    }

    public function terms()
    {
        return view('terms');
    }

  public function logout(Request $request) {
    Auth::logout();
    return redirect('/login');
  }

public function activateAccount(){

    $resetCode=input::get('c');
    $id=input::get('k');

	Log::info('activateAccount activate id--'.$id);
        Log::info('activateAccount resetCode--'.$resetCode);

    $m=User::where('id',$id)->first();

    if($m){

        $mt=User::where('id',$id)->first();
        Log::info('activate id--'.$id);
        Log::info('resetCode--'.$mt->resetCode);

        if($mt->status == 1){
        return Redirect::to('login')->with('success', 'Your account is already activated.Please login');
        }

      if($mt->resetCode==$resetCode){
        //make active

         $m=User::where('id',$id)->update(['isVerified'=>1,'resetCode'=>0]);

        return Redirect::to('login')->with('success', 'You have successfully activated your account. Login.');

      }else{
         return Redirect::to('login')->with('error', 'Sorry, we could not verify your account. Contact admin for help.');
      }

    }else{
      return Redirect::to('login')->with('error', 'Invalid verification attempt');
    }
  }

  public function subscribe(Request $request){
  //  log::info($request->email);

    $check=Subscriptions::where('email',$request->email)->where('is_deleted',0)->first();
    if(!$check) {
    if(Subscriptions::saveNew($request->email)) {
      $response = array(
          'status' => '1',
          'message'=>'Your email was successfully saved. Thank you for trusting us.',
          'email' => $request->email,
      );
    }
    else {
      $response = array(
          'status' => '1',
          'message'=>'Failed to save your email. Please try again',
          'email' => $request->email,
      );
    }
  }
  else {
    $response = array(
        'status' => '1',
        'message'=>'Email address [ '.$request->email.' ] already exists',
        'email' => $request->email,
    );
  }
      return response()->json($response);
  }

}
