<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Categories;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class CategoriesController extends Controller {

	public function categories()
	{
		$list = Categories::getAll();
		return view('categories',['list'=>$list]);
	}

	public function addcategory(Request $request)
	{
			$categoryName = $request->categoryName;

			$checkexists = Categories::where('categoryName',$request->categoryName)->where('isDeleted',0)->first();
			if($checkexists) {
				return Redirect::back()->with(['status0'=>'Sorry, category already exists']);
			}
			else {

			$add = Categories::storeone($categoryName);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New category was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating category.']);
			}

		}

	}


	public function editcategory(Request $request)
	{
			$id = $request->id;
			$categoryName = $request->categoryName;

			$update = Categories::updateone($id, $categoryName);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The category was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating category']);
			}

	}

	public function deletecategory(Request $request)
	{
			$id = $request->id;
			$delete = Categories::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
