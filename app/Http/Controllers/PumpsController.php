<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Pumps;
use App\Pumpreadings;
use App\Shifttypes;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class PumpsController extends Controller {

	public function pumps()
	{
		$list = Pumps::getAll();
		return view('pumps',['list'=>$list]);
	}

	public function addpump(Request $request)
	{
			$pumpName = $request->pumpName;

			$checkexists = Pumps::where('pumpName',$request->pumpName)->where('isDeleted',0)->first();
			if($checkexists) {
				return Redirect::back()->with(['status0'=>'Sorry, pump already exists']);
			}
			else {

			$add = Pumps::storeone($pumpName);
			if ($add) {

					Pumps::seedAll();

					return Redirect::back()->with(['status1'=>'New pump was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating pump.']);
			}

		}

	}


	public function editpump(Request $request)
	{
			$id = $request->id;
			$pumpName = $request->pumpName;

			$update = Pumps::updateone($id, $pumpName);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The pump was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating pump']);
			}

	}

	public function editpumpreading(Request $request)
	{
			$id = $request->id;
			$openReading = $request->openReading;
			$closeReading = $request->closeReading;
			$update = Pumpreadings::editpumpreading($id,$openReading,$closeReading);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The pump readings were updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating pump readings']);
			}

	}

	public function deletepump(Request $request)
	{
			$id = $request->id;
			$delete = Pumps::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
