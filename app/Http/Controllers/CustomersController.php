<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Customers;
use App\Sales;
use App\Reportranges;
use App\Paiddebts;
use App\Accounts;
use App\Customerconsumption;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class CustomersController extends Controller {

	public function customers()
	{
		$list = Customers::getAll();
		$statements = Customers::getStatements(0);
		return view('customers',['list'=>$list,'statements'=>$statements]);
	}

	public function addcustomer(Request $request)
	{
			$adminId	= Auth::user()->adminId;
			$customerName = $request->customerName;
			$email = $request->email;
			$contacts = $request->contacts;

			$check = Customers::where('customerName', $customerName)->where('adminId',$adminId)->where('isDeleted', 0)->first();
			if ($check) {
					return Redirect::back()->with(['status0'=>'Customer name already exists.']);
			} else {
					$add = Customers::storeone($customerName,$email,$contacts);
					if ($add) {
							return Redirect::back()->with(['status1'=>'New record was created successfully.']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while creating record.']);
					}
			}
	}


	public function editcustomer(Request $request)
	{
			$id = $request->id;
			$customerName = $request->customerName;
			$email = $request->email;
			$contacts = $request->contacts;

					$update = Customers::updateone($id, $customerName,$email,$contacts);
					if ($update) {
							return Redirect::back()->with(['status1'=>'The customer was updated successfully']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while updating customer']);
					}

	}


	public function deletecustomer(Request $request)
	{
			$id = $request->id;
			$delete = Customers::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

	public function receivepayment(Request $request)
	{
			$id = $request->id;
			$amount = $request->amount;

			$delete = Paiddebts::receivepayment($amount,$request->accountId,$id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Received payment successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while receiving payment']);
			}

	}

	public function editdebt(Request $request)
	{
			$id = $request->id;
			$amount = $request->amount;
			$delete = Paiddebts::editdebt($id,$amount);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Updated payment successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating payment']);
			}
	}

	public function deletedebt(Request $request)
	{
			$id = $request->id;
			$delete = Paiddebts::deletedebt($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Deleted payment successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting payment']);
			}
	}

	public function viewCustomer($customerId)
	{
		$customerDetails = Customers::where('id',$customerId)->first();
		if($customerDetails) {

		$consumptionlist = Customerconsumption::where('customerId',$customerId)
		->where('adminId',Auth::user()->adminId)
		->where('isDeleted',0)
		->orderBy('id','DESC')
		->get();

		$paiddebts = Paiddebts::getAll($customerId);

		$statements = Customers::getStatements($customerId);

		return view('viewCustomer',['consumptionlist'=>$consumptionlist,'customerId'=>$customerId,'customerDetails'=>$customerDetails,'paiddebts'=>$paiddebts,'statements'=>$statements]);
	}
	return Redirect::back()->with(['status0'=>'Invalid customer']);
	}

	public function postfiltercustomersalereport(Request $request)
	{
		$customerDetails = Customers::where('id',$request->customerId)->first();
		if($customerDetails) {
			$fromdate = $request->fromdate;
			$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
			$fromdate = date("Y-m-d", strtotime($fromdate))." 23:59:59";
			$todate = $request->todate;
			$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
			$todate = date("Y-m-d", strtotime($todate))." 23:59:59";

			// $list = Sales::select('sales.*','products.productName','products.unit','categories.categoryName')
			// ->leftJoin('products','sales.productId','=','products.id')
			// ->leftJoin('categories','sales.categoryId','=','categories.id')
			// ->where('sales.customerId',$request->customerId)
			// ->where('sales.adminId',Auth::user()->adminId)
			// ->whereBetween('sales.created_at', [$fromdate, $todate])
			// ->where('sales.isDeleted',0)
			// ->orderBy('sales.id','DESC')->get();

			Reportranges::saveone("viewCustomer",$fromdate,$todate,$request->customerId);

			$paiddebts = Paiddebts::getAll($request->customerId);

			$statements = Customers::getStatements($request->customerId);

			return view('viewCustomer',['customerId'=>$request->customerId,'customerDetails'=>$customerDetails,'statements'=>$statements,'paiddebts'=>$paiddebts,'fromdate'=>$fromdate,'todate'=>$todate]);
		}
		return Redirect::back()->with(['status0'=>'Invalid customer']);
	}

	public function customerconsumption($customerId)
	{
		$currentMonth = date('m');

		$customerDetails = Customers::where('id',$customerId)->first();
		if($customerDetails) {

		$list = Customerconsumption::where('customerId',$customerId)
		->whereRaw('MONTH(created_at) = ?',[$currentMonth])
		->where('adminId',Auth::user()->adminId)->where('isDeleted',0)
		->orderBy('id','DESC')
		->get();

		return view('customerconsumption',['list'=>$list,'customerId'=>$customerId,'customerDetails'=>$customerDetails]);
	}
	return Redirect::back()->with(['status0'=>'Invalid customer']);
	}

	public function consumption()
	{
		$currentMonth = date('m');

		$yearlist = Customerconsumption::select(
            DB::raw('sum(amount) as sums'),
            DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
  )
  ->whereYear('created_at', date('Y'))
  ->groupBy('monthKey')
  ->orderBy('created_at', 'ASC')
  ->get();

	$list = Customerconsumption::select(
					DB::raw('sum(amount) as sums'),
					DB::raw("DATE_FORMAT(created_at,'%d') as dayKey")
)
->whereMonth('created_at', date('m'))
->whereYear('created_at', date('Y'))
->groupBy('dayKey')
->orderBy('created_at', 'ASC')
->get();

		return view('consumption',['list'=>$list,'currentMonth'=>$currentMonth,'yearlist'=>$yearlist,'year'=>date('Y')]);

	}

	public function consumptionbymonth(Request $request)
	{
		$currentMonth = $request->month;

		$yearlist = Customerconsumption::select(
						DB::raw('sum(amount) as sums'),
						DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
	)
	->whereYear('created_at', date('Y'))
	->groupBy('monthKey')
	->orderBy('created_at', 'ASC')
	->get();

	$list = Customerconsumption::select(
					DB::raw('sum(amount) as sums'),
					DB::raw("DATE_FORMAT(created_at,'%d') as dayKey")
	)
	->whereMonth('created_at', $currentMonth)
	->whereYear('created_at', date('Y'))
	->groupBy('dayKey')
	->orderBy('created_at', 'ASC')
	->get();

		return view('consumption',['list'=>$list,'currentMonth'=>$currentMonth,'yearlist'=>$yearlist,'year'=>date('Y')]);

	}

	public function consumptionbyyear(Request $request)
	{
		$currentMonth = date('m');
		$year = $request->year;

		$yearlist = Customerconsumption::select(
						DB::raw('sum(amount) as sums'),
						DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
	)
	->whereYear('created_at', $year)
	->groupBy('monthKey')
	->orderBy('created_at', 'ASC')
	->get();

	$list = Customerconsumption::select(
					DB::raw('sum(amount) as sums'),
					DB::raw("DATE_FORMAT(created_at,'%d') as dayKey")
)
->whereMonth('created_at', date('m'))
->whereYear('created_at', date('Y'))
->groupBy('dayKey')
->orderBy('created_at', 'ASC')
->get();

		return view('consumption',['list'=>$list,'currentMonth'=>$currentMonth,'yearlist'=>$yearlist,'year'=>$year]);

	}

	public function payments()
	{
		$currentMonth = date('m');

		$yearlist = Paiddebts::select(
            DB::raw('sum(amount) as sums'),
            DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
  )
  ->whereYear('created_at', date('Y'))
  ->groupBy('monthKey')
  ->orderBy('created_at', 'ASC')
  ->get();

	$list = Paiddebts::select(
					DB::raw('sum(amount) as sums'),
					DB::raw("DATE_FORMAT(created_at,'%d') as dayKey")
)
->whereMonth('created_at', date('m'))
->whereYear('created_at', date('Y'))
->groupBy('dayKey')
->orderBy('created_at', 'ASC')
->get();

		return view('payments',['list'=>$list,'currentMonth'=>$currentMonth,'yearlist'=>$yearlist,'year'=>date('Y')]);

	}

	public function paymentsbymonth(Request $request)
	{
		$currentMonth = $request->month;

		$yearlist = Paiddebts::select(
						DB::raw('sum(amount) as sums'),
						DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
	)
	->whereYear('created_at', date('Y'))
	->groupBy('monthKey')
	->orderBy('created_at', 'ASC')
	->get();

	$list = Paiddebts::select(
					DB::raw('sum(amount) as sums'),
					DB::raw("DATE_FORMAT(created_at,'%d') as dayKey")
	)
	->whereMonth('created_at', $currentMonth)
	->whereYear('created_at', date('Y'))
	->groupBy('dayKey')
	->orderBy('created_at', 'ASC')
	->get();

		return view('payments',['list'=>$list,'currentMonth'=>$currentMonth,'yearlist'=>$yearlist,'year'=>date('Y')]);

	}

	public function paymentsbyyear(Request $request)
	{
		$currentMonth = date('m');
		$year = $request->year;

		$yearlist = Paiddebts::select(
						DB::raw('sum(amount) as sums'),
						DB::raw("DATE_FORMAT(created_at,'%m') as monthKey")
	)
	->whereYear('created_at', $year)
	->groupBy('monthKey')
	->orderBy('created_at', 'ASC')
	->get();

	$list = Paiddebts::select(
					DB::raw('sum(amount) as sums'),
					DB::raw("DATE_FORMAT(created_at,'%d') as dayKey")
)
->whereMonth('created_at', date('m'))
->whereYear('created_at', date('Y'))
->groupBy('dayKey')
->orderBy('created_at', 'ASC')
->get();

		return view('payments',['list'=>$list,'currentMonth'=>$currentMonth,'yearlist'=>$yearlist,'year'=>$year]);

	}

	public function customerpayments($customerId)
	{

		$currentMonth = date('m');

		$customerDetails = Customers::where('id',$customerId)->first();
		if($customerDetails) {

		$paiddebts = Paiddebts::select('paiddebts.*','customers.customerName')
		->leftJoin('customers','paiddebts.customerId','=','customers.id')
		->where('paiddebts.customerId',$customerId)
		->whereRaw('MONTH(paiddebts.created_at) = ?',[$currentMonth])
		->where('paiddebts.isDeleted',0)
		->orderBy('paiddebts.id','DESC')->get();

		return view('customerpayments',['customerId'=>$customerId,'customerDetails'=>$customerDetails,'paiddebts'=>$paiddebts]);
	}

	return Redirect::back()->with(['status0'=>'Invalid customer']);

	}


	public function addconsumption(Request $request)
	{
			$adminId	= Auth::user()->adminId;
			$customerId = $request->id;
			$amount = $request->amount;
			$qty = $request->qty;
			$productId = $request->productId;

					$add = Customerconsumption::storeone($customerId,$amount,$qty,$productId);
					if ($add) {
							return Redirect::back()->with(['status1'=>'New consumption was created successfully.']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while creating consumption.']);
					}
	}


	public function editconsumption(Request $request)
	{
			$id = $request->id;
			$amount = $request->amount;
			$qty = $request->qty;

					$update = Customerconsumption::updateone($id, $amount,$qty);
					if ($update) {
							return Redirect::back()->with(['status1'=>'The consumption was updated successfully']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while updating consumption']);
					}

	}


	public function deleteconsumption(Request $request)
	{
			$id = $request->id;
			$delete = Customerconsumption::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Consumption was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting consumption']);
			}
	}

}
