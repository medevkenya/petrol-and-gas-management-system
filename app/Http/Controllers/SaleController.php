<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Sales;
use App\Categories;
use App\Customers;
use App\Products;
use App\Expenses;
use App\Batches;
use App\Reportranges;
use App\Settings;
use App\Tanks;
use App\Shifts;
use App\Exports\FilterSaleExport;
use App\Exports\FilterCustomerSaleExport;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class SaleController extends Controller {

	public function makesales()
	{
		$pumpattendants = Shifts::getAllPumpAttendants();
		$list = Sales::select('sales.*','products.productName','products.unit','categories.categoryName')
		->leftJoin('products','sales.productId','=','products.id')
		->leftJoin('categories','sales.categoryId','=','categories.id')
		->where('sales.adminId',Auth::user()->adminId)
		->where('sales.isDeleted',0)
		->orderBy('sales.id','DESC')
		->get();

		return view('sale',['list'=>$list,'pumpattendants'=>$pumpattendants]);
	}

	public function postfiltersalereport(Request $request)
	{
			$fromdate = $request->fromdate;
		//	$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
			$fromdate = date("Y-m-d", strtotime($fromdate))."";
			$todate = $request->todate;
		//	$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
			$todate = date("Y-m-d", strtotime($todate))."";

			$list = Sales::select('sales.*','products.productName','products.unit','categories.categoryName')
			->leftJoin('products','sales.productId','=','products.id')
			->leftJoin('categories','sales.categoryId','=','categories.id')
			->where('sales.adminId',Auth::user()->adminId)
			->where('sales.date','>=',$fromdate)->where('sales.date','<=',$todate)
			//->whereBetween('sales.created_at', [$fromdate, $todate])
			->where('sales.isDeleted',0)
			->orderBy('sales.id','DESC')->get();

			Reportranges::saveone("sale",$fromdate,$todate);

			return view('sale',['list'=>$list,'fromdate'=>$fromdate,'todate'=>$todate]);
	}

	public function editsale(Request $request)
	{

			$id = $request->id;
			$productId = $request->productId;
			$customerId = $request->customerId;
			$quantity = $request->quantity;
			$type = $request->type;
			$discount = $request->discount;

			$update = Sales::updateone($id,$productId,$customerId,$quantity,$discount,$type);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The sale was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating sale']);
			}

	}

	public function deletesale(Request $request)
	{
			$id = $request->id;
			$delete = Sales::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

	public function completesale(Request $request)
	{

		log::info("post subTotal--".$request->discount);
		// $data = Cart::getCart();
		// foreach ($data as $key) {
		// 	$check = Batches::where('productId',$key->productId)->where('status','In Stock')->where('isDeleted',0)->orderBy('id','ASC')->first();
		// 	if($check)
		// 	{
		//
		// 	}
		// }

		$pDetails = Products::where('id',$request->productId)->first();
		if($pDetails->stockbalance > $request->quantity) {

		$res = Sales::completesale($request->total,$request->cashQuantity,$request->productId,$request->discount,$request->categoryId,$request->creditQuantity,$request->shiftId);
		if($res) {
			return response()->json(['status'=>1,'success'=>'Successfully completed sale']);
		}
		else {
			return response()->json(['status'=>0,'error'=>'Failed to complete sale, please try again']);
		}

	}
	else {
		return response()->json(['status'=>0,'error'=>'Failed to complete sale, Insufficient stock balance']);
	}

	}

	/**
	* @return \Illuminate\Support\Collection
	*/
	public function exportfiltersaleexcel($fromdate,$todate)
	{
			return Excel::download(new FilterSaleExport, 'sales.xlsx');
	}

	/**
	* @return \Illuminate\Support\Collection
	*/
	public function exportfiltercustomersaleexcel($customerId,$fromdate,$todate)
	{
			$customerDetails = Customers::where('id',$customerId)->first();
			return Excel::download(new FilterCustomerSaleExport, ''.$customerDetails->customerName.'_sales.xlsx');
	}

	public function profitabilitysummary()
	{
		$month = date('m');
		$year = date('Y');
		$list = Products::getProfitabilityStatements($month);
		$yearlist = Products::getYearProfitabilityStatements($year);
		$expenseForYear = Expenses::expensesForYear($year);
		//$expenseForMonth = Expenses::expensesForMonth($month);
		//$expenseForYear = Expenses::expensesForYear($year);
		$expenseForMonth = Expenses::expensesForMonth($month);
		$settings = Settings::getDetails();
		$taxPercentage = $settings->tax;
		return view('profitabilitysummary',['list'=>$list,'yearlist'=>$yearlist,'month'=>$month,'year'=>$year,'expenseForYear'=>$expenseForYear,'expenseForMonth'=>$expenseForMonth,'taxPercentage'=>$taxPercentage]);
	}

	public function searchprofitbymonth(Request $request)
	{
		$month = $request->month;
		$year = date('Y');
		$list = Products::getProfitabilityStatements($month);
		$yearlist = Products::getYearProfitabilityStatements($year);
		//$expenseForYear = Expenses::expensesForYear($year);
		//$expenseForMonth = Expenses::expensesForMonth($month);
		$expenseForYear = Expenses::expensesForYear($year);
		$expenseForMonth = Expenses::expensesForMonth($month);
		$settings = Settings::getDetails();
		$taxPercentage = $settings->tax;
		return view('profitabilitysummary',['list'=>$list,'yearlist'=>$yearlist,'month'=>$month,'year'=>$year,'expenseForYear'=>$expenseForYear,'expenseForMonth'=>$expenseForMonth,'taxPercentage'=>$taxPercentage]);
	}

	public function searchprofitbyyear(Request $request)
	{
		$month = date('m');
		$year = $request->year;
		$list = Products::getProfitabilityStatements($month);
		$yearlist = Products::getYearProfitabilityStatements($year);
		$expenseForYear = Expenses::expensesForYear($year);
		$expenseForMonth = Expenses::expensesForMonth($month);
		$settings = Settings::getDetails();
		$taxPercentage = $settings->tax;
		return view('profitabilitysummary',['list'=>$list,'yearlist'=>$yearlist,'month'=>$month,'year'=>$year,'expenseForYear'=>$expenseForYear,'expenseForMonth'=>$expenseForMonth,'taxPercentage'=>$taxPercentage]);
	}

	public function dailysales()
	{

		$date = date('Y-m-d');

		$list = Sales::select('sales.*','products.productName','products.unit','products.sellPrice','products.costPrice')
		->leftJoin('products','sales.productId','=','products.id')
		->where('sales.productId',1)
		->where('sales.date',$date)
		->where('sales.adminId',Auth::user()->adminId)
		->where('sales.isDeleted',0)
		->orderBy('sales.id','DESC')
		->get();

		$listdisel = Sales::select('sales.*','products.productName','products.unit','products.sellPrice','products.costPrice')
		->leftJoin('products','sales.productId','=','products.id')
		->where('sales.productId',2)
		->where('sales.date',$date)
		->where('sales.adminId',Auth::user()->adminId)
		->where('sales.isDeleted',0)
		->orderBy('sales.id','DESC')
		->get();

		$listtanks = Tanks::select('tanks.*','products.productName')
		->leftJoin('products','tanks.productId','=','products.id')
		//->whereIn('batches.productId',[1,2])
		//->groupBy('batches.tankId')
		->get();

		return view('dailysales',['list'=>$list,'listdisel'=>$listdisel,'listtanks'=>$listtanks,'date'=>$date]);
	}

	public function searchdailysales(Request $request)
	{

		$date = date("Y-m-d", strtotime($request->date));

		$list = Sales::select('sales.*','products.productName','products.unit','products.sellPrice','products.costPrice')
		->leftJoin('products','sales.productId','=','products.id')
		->where('sales.productId',1)
		->where('sales.date',$date)
		->where('sales.adminId',Auth::user()->adminId)
		->where('sales.isDeleted',0)
		->orderBy('sales.id','DESC')
		->get();

		$listdisel = Sales::select('sales.*','products.productName','products.unit','products.sellPrice','products.costPrice')
		->leftJoin('products','sales.productId','=','products.id')
		->where('sales.productId',2)
		->where('sales.date',$date)
		->where('sales.adminId',Auth::user()->adminId)
		->where('sales.isDeleted',0)
		->orderBy('sales.id','DESC')
		->get();

		$listtanks = Tanks::select('tanks.*','products.productName')
		->leftJoin('products','tanks.productId','=','products.id')->get();

		return view('dailysales',['list'=>$list,'listdisel'=>$listdisel,'date'=>$date,'listtanks'=>$listtanks]);
	}

}
