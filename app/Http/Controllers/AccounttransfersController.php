<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Accounttransfers;
use App\Notifications;
use App\Accounts;
use App\Sales;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class AccounttransfersController extends Controller {

	public function accounttransfers()
	{
		$list = Accounttransfers::getAll();

		$totaldeposits = Accounttransfers::where('type','Deposit')->where('isDeleted',0)->sum('amount');
		$totalwithdraw = Accounttransfers::where('type','Withdraw')->where('isDeleted',0)->sum('amount');

		return view('accounttransfers',['list'=>$list]);
	}

	public function postfiltertransfers(Request $request)
	{
			$fromdate = $request->fromdate;
			//$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
			$fromdate = date("Y-m-d", strtotime($fromdate))."";
			$todate = $request->todate;
			//$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
			$todate = date("Y-m-d", strtotime($todate))."";

			$list = Accounttransfers::select('accounttransfers.*','accounts.accountName')
      ->leftJoin('accounts','accounttransfers.accountId','=','accounts.id')
      ->where('accounttransfers.adminId',Auth::user()->adminId)
			->where('accounttransfers.date','>=',$fromdate)->where('accounttransfers.date','<=',$todate)
			->where('accounttransfers.isDeleted',0)
			->orderBy('accounttransfers.id','DESC')->get();

			return view('accounttransfers',['list'=>$list,'fromdate'=>$fromdate,'todate'=>$todate]);
	}

	public function postfiltertransfersbyaccount(Request $request)
	{
			$fromdate = $request->fromdate;
			$accountId = $request->accountId;
			//$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
			$fromdate = date("Y-m-d", strtotime($fromdate))."";
			$todate = $request->todate;
			//$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
			$todate = date("Y-m-d", strtotime($todate))."";

			$details = Accounts::where('id',$accountId)->where('isDeleted',0)->first();

			$list = Accounttransfers::select('accounttransfers.*','accounts.accountName')
			->leftJoin('accounts','accounttransfers.accountId','=','accounts.id')
			->where('accounttransfers.adminId',Auth::user()->adminId)
			->where('accounttransfers.date','>=',$fromdate)->where('accounttransfers.date','<=',$todate)
			->where('accounttransfers.accountId',$accountId)
			->where('accounttransfers.isDeleted',0)
			->orderBy('accounttransfers.id','DESC')->get();

			return view('viewAccount',['list'=>$list,'details'=>$details,'fromdate'=>$fromdate,'todate'=>$todate]);
	}

	public function addaccounttransfer(Request $request)
	{

			$accountId = $request->accountId;
			$amount = $request->amount;
			$reason = $request->reason;

			// $transferAmountToday = Accounttransfers::where('date', date('Y-m-d'))->where('isDeleted',0)->sum('amount');
			// $totalTransfersToday = $transferAmountToday + $amount;
			//
			// $soldAmountToday = Sales::where('date', date('Y-m-d'))->where('isDeleted',0)->sum('total');
			//
			// if($totalTransfersToday > $soldAmountToday) {
			// 	Notifications::storeone("Attempted to transfer ksh. ".$totalTransfersToday." which is more than daily sales of Ksh. ".$soldAmountToday."");
			// 	return Redirect::back()->with(['status0'=>'Amount sold is less than amount transferred']);
			// }
			// else {

			$add = Accounttransfers::storeone($accountId,$amount,"Transfer",$reason);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New account transfer was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating account transfer.']);
			}

		//}

	}


	public function editaccounttransfer(Request $request)
	{
			$id = $request->id;
			$accountId = $request->accountId;
			$amount = $request->amount;
			$reason = $request->reason;
			$update = Accounttransfers::updateone($id,$amount,$reason);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The account transfer was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating account transfer']);
			}

	}

	public function deleteaccounttransfer(Request $request)
	{
			$id = $request->id;
			$delete = Accounttransfers::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
