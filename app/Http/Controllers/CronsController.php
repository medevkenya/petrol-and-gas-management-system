<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Batches;
use App\Sales;
use App\Products;
use Hash;
use Session;
use PDF;
use Log;
use Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class CronsController extends Controller {

	public static function cheklowstock()
	{

		$list = Products::getLowStockAll();

		$settings = Settings::where('id',1)->where('isDeleted',0)->first();
		$email = $settings->companyEmail;

		$lowstock =0; foreach ($list as $product) {
			$quantity = Batches::getProductQuantity($product->id);
			if($quantity <= $product->alertLevel) { $lowstock++;
				$pass = time();
				$productName = $product->productName;
				Mail::send('emails.lowstock', $data, function($message) use ($pass,$email,$productName,$quantity) {
				$message->to($email)->subject('Low Stock Product');
				$message->from(env("MAIL_FROM_ADDRESS"),env("MAIL_FROM_NAME"));
				});
			}
		}

		}

}
