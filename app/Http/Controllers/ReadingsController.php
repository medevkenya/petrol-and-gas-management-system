<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Readings;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class ReadingsController extends Controller {

	public function readings()
	{
		$list = Readings::getAll();
		return view('readings',['list'=>$list]);
	}

	public function addreading(Request $request)
	{
			$meterReading = $request->meterReading;
			//$productId = $request->productId;
			//$categoryId = $request->categoryId;
			$tankId = $request->tankId;

			// $checkexists = Readings::where('productId',$request->productId)->where('isDeleted',0)->first();
			// if($checkexists) {
			// 	return Redirect::back()->with(['status0'=>'Sorry, record already exists']);
			// }
			// else {

			$add = Readings::storeone($meterReading,$tankId);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New reading was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating reading.']);
			}

		//}

	}


	public function editreading(Request $request)
	{
			$id = $request->id;
			$meterReading = $request->meterReading;
			$tankId = $request->tankId;

			$update = Readings::updateone($id, $meterReading, $tankId);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The reading was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating reading']);
			}

	}

	public function deletereading(Request $request)
	{
			$id = $request->id;
			$delete = Readings::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
