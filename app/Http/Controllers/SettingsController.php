<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Settings;
use App\Roles;
use App\Appointments;
use App\Admissions;
use App\Referrals;
use App\Referralhospitals;
use App\Services;
use App\Wards;
use App\Treatments;
use App\Costs;
use App\Doctors;
use Mail;
use Hash;
use Auth;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Log;
use Carbon\Carbon;

class SettingsController extends Controller
{

    public function updateCompanyDetails(Request $request)
    {
      //https://stackoverflow.com/questions/51962486/datepicker-set-maximum-date-to-18-years-ago-from-current-date
        request()->validate([
            'companyName' => 'required|min:3|max:50',
            'companyTelephone' => 'required|min:3|max:50',
            'companyEmail' => 'required',// input starts with 01 and is followed by 9 numbers
            'companyLocation' => 'required',
            'tax' => 'required',
            'discount' => 'required',
            // 'dob' => 'required|date_format:d-m-Y|before:18 years',
            //'terms' => 'accepted',
        ], [
            'companyName.required' => 'Company name is required',
        ]);

        $res = Settings::editMyProfile(request()->all());
        if($res) {
          return redirect()->back()->withInput($request->input())->with('success', 'Your company profile was updated successfully');
        }
        else {
          return redirect()->back()->withInput($request->input())->with('error', 'Failed to save. Please try again');
        }

    }


    public function updateCompanyPhoto(Request $request)
    {

  // getting all of the post data
        $id	= Auth::user()->id;
        $file = array('image' => $request->companyPhoto);

        // setting up rules
        //  $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000 required|image|mimes:jpeg,png,jpg,gif,svg|max:2048
        $rules = array('image' => 'required|image|mimes:jpeg,png,jpg,gif|max:5048',);
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);

        // checking file is valid.
        if ($request->companyPhoto->isValid()) {
            //$destinationPath = 'otheruploads'; // upload path
            $destinationPath = public_path('/profiles');

            $extension = $request->companyPhoto->getClientOriginalExtension(); // getting image extension

            $fileName = rand(11111, 99999).'.'.$extension; // renameing image

            $request->companyPhoto->move($destinationPath, $fileName); // uploading file to given path

            //file name to take to db
            $companyPhoto=  $fileName;

            Settings::where('adminId',Auth::user()->adminId)->update(['companyPhoto' => $companyPhoto]);

            return Redirect::to('profile')->with(['status1'=>'Your company photo was updated successfully.']);
        } else {
            return Redirect::to('profile')->with(['status0'=>'Sorry, error occurred while updating your company photo. Try again.']);
        }
    }


}
