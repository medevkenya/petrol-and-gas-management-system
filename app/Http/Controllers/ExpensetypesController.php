<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Expensetypes;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class ExpensetypesController extends Controller {

	public function expensetypes()
	{
		$list = Expensetypes::getAll();
		return view('expensetypes',['list'=>$list]);
	}

	public function addexpensetype(Request $request)
	{
			$expensetypeName = $request->expensetypeName;

			$checkexists = Expensetypes::where('expensetypeName',$request->expensetypeName)->where('isDeleted',0)->first();
			if($checkexists) {
				return Redirect::back()->with(['status0'=>'Sorry, expense type already exists']);
			}
			else {

			$add = Expensetypes::storeone($expensetypeName);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New expense type was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating expense type.']);
			}

		}

	}


	public function editexpensetype(Request $request)
	{
			$id = $request->id;
			$expensetypeName = $request->expensetypeName;

			$update = Expensetypes::updateone($id, $expensetypeName);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The expense type was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating expense type']);
			}

	}

	public function deleteexpensetype(Request $request)
	{
			$id = $request->id;
			$delete = Expensetypes::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
