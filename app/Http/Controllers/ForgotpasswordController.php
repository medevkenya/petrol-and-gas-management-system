<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use Auth;
use Hash;
use Mail;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ForgotpasswordController extends Controller
{

    public function forgotPassword()
    {
       return view('forgotPassword',['email'=>'']);
    }

    public function resetPassword(Request $request)
    {

        request()->validate([
            'email' => 'required|email',
        ], [
            'email.required' => 'Email address is required',
            'email.email' => 'Please provide a valid email address',
        ]);

        $check = User::where('email',request()->email)->where('isDeleted',0)->first();
      	if($check) {

        $res = User::resetPassword(request()->email);
        if($res) {
          return view('forgotPassword',['success'=>'Successful. We have sent recovery instructions to ('.request()->email.'). Check yopur email and complete password recovery process. If you haven\'t received it, ','email'=>request()->email]);
        //return back()->with(['success'=>'Successful. We have sent recovery instructions to ('.request()->email.'). Check yopur email and complete password recovery process. If you haven\'t received it, ','email'=>request()->email]);
        }
        else {
          return back()->with('error', 'Recovery failed. Please verify this email and try again');
        }

      }
      else {
        return back()->with('error', 'Email address not found. Please verify this email and try again');
      }

    }

    public function resendRecoveryEmail($email)
    {

        $check = User::where('email',$email)->where('isDeleted',0)->first();
      	if($check) {

        $res = User::resetPassword($email);
        if($res) {
          return view('forgotPassword',['success'=>'Successful. We have sent recovery instructions to ('.$email.'). Check yopur email and complete password recovery process. If you haven\'t received it, ','email'=>$email]);
        //return back()->with(['success'=>'Successful. We have sent recovery instructions to ('.request()->email.'). Check yopur email and complete password recovery process. If you haven\'t received it, ','email'=>request()->email]);
        }
        else {
          return back()->with('error', 'Recovery failed. Please verify this email and try again');
        }

      }
      else {
        return back()->with('error', 'Email address not found. Please verify this email and try again');
      }

    }

    public function resetPasswordLink($confirmpassword,$email){
      $userdetails = User::where('confirmpassword', $confirmpassword)->where('email',$email)->first();
    	if (!$userdetails) {
        return view('emails.setpassword', ['status0' => 'Sorry, invalid password confirmation secret']);
			}
			else {
			     $name = $userdetails->firstName." ".$userdetails->lastName;
           return view('emails.setpassword', ['email' => $email,'name' => $name]);
    	}
    }

    public function doResetPassword(Request $request) {
      request()->validate([
        'email' => 'required|email',
        'password' => 'required|min:6',
        'cpassword' => 'required|same:password',
      ], [
        'email.required' => 'Email address is required',
        'password.required' => 'Password is required',
        'password.min' => 'Password must be at least 6 characters',
        'cpassword.required' => 'Please confirm your new password',
        'cpassword.same' => 'Password and confirm password must be the same',
      ]);

      $email	= request()->email;
      $password	= request()->password;
      $cpassword	= request()->cpassword;

      $hashedPassword = Hash::make($password);

          $edituserpass = User::where('email', $email)->update(['password' => $hashedPassword]);
          if($edituserpass) {

			    $data=array('email'=>$email);

      		//send mail to confirm password change
      		Mail::send('emails.confirmtemplate', $data, function($message) use ($email) {
      		$message->to($email)->subject('Account New Password');
      		$message->from('mails@globaltempingservices.com','Global Temping Services');
      		});

      				//update user db with newpass
    				$pass = "";
    				$edituserdata = User::where('email', $email)->update(['confirmpassword' => $pass]);
            return Redirect::to('login')->with('success', 'Your new password has been set successfully. Login.');
      			} else {
              return view('emails.setpassword', ['status10' => 'An error occurred while setting your new password. Try again','email' => $email]);
      			}
    }

}
