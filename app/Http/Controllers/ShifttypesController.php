<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Shifttypes;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class ShifttypesController extends Controller {

	public function shifttypes()
	{
		$list = Shifttypes::getAll();
		return view('shifttypes',['list'=>$list]);
	}

	public function addshifttype(Request $request)
	{
			$shifttypeName = $request->shifttypeName;

			$checkexists = Shifttypes::where('shifttypeName',$request->shifttypeName)->where('isDeleted',0)->first();
			if($checkexists) {
				return Redirect::back()->with(['status0'=>'Sorry, shifttype already exists']);
			}
			else {

			$add = Shifttypes::storeone($shifttypeName);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New shifttype was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating shifttype.']);
			}

		}

	}


	public function editshifttype(Request $request)
	{
			$id = $request->id;
			$shifttypeName = $request->shifttypeName;

			$update = Shifttypes::updateone($id, $shifttypeName);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The shifttype was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating shifttype']);
			}

	}

	public function deleteshifttype(Request $request)
	{
			$id = $request->id;
			$delete = Shifttypes::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
