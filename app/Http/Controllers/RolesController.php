<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Roles;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class RolesController extends Controller {

	public function roles()
	{
		$list = Roles::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
		return view('roles',['list'=>$list]);
	}

	public function addrole(Request $request)
	{
			$roleName = $request->roleName;
			$adminId	= Auth::user()->adminId;

			$check = Roles::where('roleName', $roleName)->where('adminId',$adminId)->where('isDeleted', 0)->first();
			if ($check) {
					return Redirect::back()->with(['status0'=>'Role already exists.']);
			} else {
					$add = Roles::storeone($roleName);
					if ($add) {
							return Redirect::back()->with(['status1'=>'New role was created successfully.']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while creating role.']);
					}
			}
	}


	public function editrole(Request $request)
	{
			$id = $request->id;
			$roleName = $request->roleName;

					$update = Roles::updateone($id, $roleName);
					if ($update) {
							return Redirect::back()->with(['status1'=>'The record was updated successfully']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while updating record']);
					}

	}


	public function deleterole(Request $request)
	{
			$id = $request->id;
			$delete = Roles::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
