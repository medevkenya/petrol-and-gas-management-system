<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Shifts;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class ShiftsController extends Controller {

	public function shifts()
	{
		$list = Shifts::getAll();
		return view('shifts',['list'=>$list]);
	}

	public function addshift(Request $request)
	{
			$userId = $request->userId;
			$pumpId = $request->pumpId;
			$shiftTypeId = $request->shiftTypeId;

			$checkexists = Shifts::where('userId',$request->userId)->where('pumpId',$request->pumpId)->where('date',date('Y-m-d'))->where('isDeleted',0)->first();
			if($checkexists) {
				return Redirect::back()->with(['status0'=>'Sorry, attendant is already on another shift']);
			}
			else {

			$add = Shifts::storeone($userId,$pumpId,$shiftTypeId);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New shift was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating shift.']);
			}

		}

	}


	public function editshift(Request $request)
	{
			$id = $request->id;
			$pumpId = $request->pumpId;
			$userId = $request->userId;
			$shiftTypeId = $request->shiftTypeId;
			$update = Shifts::updateone($id, $userId, $pumpId, $shiftTypeId);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The shift was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating shift']);
			}

	}

	public function deleteshift(Request $request)
	{
			$id = $request->id;
			$delete = Shifts::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
