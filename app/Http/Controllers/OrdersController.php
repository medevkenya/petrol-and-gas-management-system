<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Sales;
use App\Services;
use App\Orders;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class OrdersController extends Controller {

	public function salesReport()
	{
		$list = Sales::select('sales.*','shifttypes.shifttypeName','products.productName','products.unit','categories.categoryName','users.firstName','users.lastName')
		->leftJoin('products','sales.productId','=','products.id')
		->leftJoin('categories','sales.categoryId','=','categories.id')
		->leftJoin('users','sales.attendantId','=','users.id')
		->leftJoin('shifttypes','sales.shiftTypeId','=','shifttypes.id')
		->where('sales.adminId',Auth::user()->adminId)
		->whereDate('sales.created_at', Carbon::today())
		->where('sales.isDeleted',0)
		->orderBy('sales.id','DESC')
		->get();
		
		$currentMonth = date('m');
		$yesterdayDate = date("Y-m-d", strtotime( '-1 days' ) );
		$yesterday = Sales::whereDate('created_at', $yesterdayDate )->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');
		$today = Sales::whereDate('created_at', Carbon::today())->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');
		$thismonth = Sales::whereRaw('MONTH(created_at) = ?',[$currentMonth])->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');
		$thisweek = Sales::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');

		return view('salesReport',['list'=>$list,'today'=>number_format($today,0),'yesterday'=>number_format($yesterday,0),'thisweek'=>number_format($thisweek,0),'thismonth'=>number_format($thismonth,0)]);
	}

	public function postSalesReport(Request $request)
	{
			$fromdate = $request->fromdate;
			$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
			$fromdate = date("Y-m-d", strtotime($fromdate))." 23:59:59";
			$todate = $request->todate;
			//$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
			$todate = date("Y-m-d", strtotime($todate))." 23:59:59";
			
			$list = Sales::select('sales.*','shifttypes.shifttypeName','products.productName','products.unit','categories.categoryName','users.firstName','users.lastName')
		->leftJoin('products','sales.productId','=','products.id')
		->leftJoin('categories','sales.categoryId','=','categories.id')
		->leftJoin('users','sales.attendantId','=','users.id')
		->leftJoin('shifttypes','sales.shiftTypeId','=','shifttypes.id')
		->where('sales.adminId',Auth::user()->adminId)
		->whereBetween('sales.created_at', [new Carbon($fromdate), new Carbon($todate)])
		->where('sales.isDeleted',0)
		->orderBy('sales.id','DESC')
		->get();

// 			$list = Sales::select('orders.*','users.firstName','users.lastName')
//       ->leftJoin('users','orders.userId','=','users.id')
// 			->where('orders.adminId',Auth::user()->adminId)
// 			->whereBetween('orders.created_at', [new Carbon($fromdate), new Carbon($todate)])
// 			//->whereBetween('orders.appointmentDate', [$fromdate, $todate])
// 			->where('orders.isDeleted',0)
// 			->orderBy('orders.id','DESC')
// 			->paginate(20);

			$currentMonth = date('m');
			$yesterdayDate = date("Y-m-d", strtotime( '-1 days' ) );
			$yesterday = Sales::whereDate('created_at', $yesterdayDate )->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');
			$today = Sales::whereDate('created_at', Carbon::today())->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');
			$thismonth = Sales::whereRaw('MONTH(created_at) = ?',[$currentMonth])->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');
			$thisweek = Sales::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');

			return view('salesReport',['list'=>$list,'fromdate'=>$request->fromdate,'todate'=>$request->todate,'today'=>number_format($today,0),'yesterday'=>number_format($yesterday,0),'thisweek'=>number_format($thisweek,0),'thismonth'=>number_format($thismonth,0)]);
	}

	public function deleteorder(Request $request)
	{
			$id = $request->id;
			$delete = Orders::deleteone($id);
			if ($delete)
			{
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			}
			else
			{
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
