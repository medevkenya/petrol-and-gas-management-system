<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Products;
use App\Batches;
use App\Sales;
use App\Tanks;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\Facades\Image as Image;

class ProductsController extends Controller {

	public function getProducts(Request $request)
	{
		if (!$request->categoryId) {
		$products = '<option value="">Select Category First</option>';
		}
		else {

		$products = '<option value="">Select</option>';
		$data = Products::select('id','productName')->where('categoryId',$request->categoryId)->where('isDeleted',0)->get();
		foreach ($data as $dt) {
				$products .= '<option value="'.$dt->id.'">'.$dt->productName.'</option>';
		}

		}
		log::info("products--".$products);
		return response()->json(['products'=>$products]);
	}

	public function getTanks(Request $request)
	{
		if (!$request->productId) {
		$tanks = '<option value="">Select Product First</option>';
		}
		else {

		$tanks = '<option value="">Select</option>';
		$data = Tanks::select('id','tankName')->where('productId',$request->productId)->where('isDeleted',0)->get();
		foreach ($data as $dt) {
				$tanks .= '<option value="'.$dt->id.'">'.$dt->tankName.'</option>';
		}

		}
		log::info("tanks--".$tanks);
		return response()->json(['tanks'=>$tanks]);
	}

	public function getProduct(Request $request)
	{
		if (!$request->productId)
		{
			$sellingPrice = 0;
			$availableStock = 0;
		}
		else
		{
			$data = Products::select('id','sellPrice')->where('id',$request->productId)->where('isDeleted',0)->orderBy('id','DESC')->first();
			if($data) {
			//$availableStock = ($data->quantity - $data->sold);
			$productDetails = Products::where('id',$request->productId)->first();
			$balance = Batches::getProductQuantity($request->productId);
			return response()->json(['status'=>1,'sellingPrice'=>$productDetails->sellPrice,'availableStock'=>$balance]);
		}
		else {
			return response()->json(['status'=>2,'sellingPrice'=>$data->sellPrice,'availableStock'=>$availableStock]);
		}
		}
		return response()->json(['status'=>0,'sellingPrice'=>$sellingPrice,'availableStock'=>$availableStock]);
	}

	public function viewProduct($productId)
	{
		$productDetails = Products::where('id',$productId)->first();
		if($productDetails) {

			$list = Sales::select('sales.*','shifttypes.shifttypeName','products.productName','products.unit','categories.categoryName','users.firstName','users.lastName')
		->leftJoin('products','sales.productId','=','products.id')
		->leftJoin('categories','sales.categoryId','=','categories.id')
		->leftJoin('users','sales.attendantId','=','users.id')
		->leftJoin('shifttypes','sales.shiftTypeId','=','shifttypes.id')
			->where('sales.productId',$productId)
			->where('sales.adminId',Auth::user()->adminId)
			->where('sales.isDeleted',0)
			->orderBy('sales.id','DESC')
			->get();

			$statements = Products::getStatements($productId);

			return view('viewProduct',['list'=>$list,'productId'=>$productId,'productDetails'=>$productDetails,'statements'=>$statements]);
		}
		return Redirect::back()->with(['status0'=>'Invalid product']);
	}

	public function postfilterproductsalereport(Request $request)
	{
		$productDetails = Products::where('id',$request->productId)->first();
		if($productDetails)
		{

			$fromdate = $request->fromdate;
			$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
			$fromdate = date("Y-m-d", strtotime($fromdate))." 23:59:59";
			$todate = $request->todate;
			$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
			$todate = date("Y-m-d", strtotime($todate))." 23:59:59";

			$list = Sales::select('sales.*','shifttypes.shifttypeName','products.productName','products.unit','categories.categoryName','users.firstName','users.lastName')
		->leftJoin('products','sales.productId','=','products.id')
		->leftJoin('categories','sales.categoryId','=','categories.id')
		->leftJoin('users','sales.attendantId','=','users.id')
		->leftJoin('shifttypes','sales.shiftTypeId','=','shifttypes.id')
			->where('sales.productId',$request->productId)
			->where('sales.adminId',Auth::user()->adminId)
			->whereBetween('sales.created_at', [$fromdate, $todate])
			->where('sales.isDeleted',0)
			->orderBy('sales.id','DESC')->get();

			Reportranges::saveone("viewProduct",$fromdate,$todate,$request->productId);

			$statements = Products::getStatements($request->productId);

			return view('viewProduct',['list'=>$list,'productId'=>$productId,'productDetails'=>$productDetails,'statements'=>$statements,'fromdate'=>$fromdate,'todate'=>$todate]);

		}

		return Redirect::back()->with(['status0'=>'Invalid product']);

	}

	public function products()
	{
		$list = Products::getAll();
		$statements = Products::getStatements(0);
		return view('products',['list'=>$list,'statements'=>$statements]);
	}

	public function addproduct(Request $request) {

	if($request->hasFile('photoUrl')) {


		$request->validate([

					'photoUrl' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

			]);

			//get filename with extension
			$filenamewithextension = $request->file('photoUrl')->getClientOriginalName();

			//get filename without extension
			$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

			//get file extension
			$extension = $request->file('photoUrl')->getClientOriginalExtension();

			//filename to store
			$filenametostore = $filename.'_'.time().'.'.$extension;

			//Upload File
			$destinationPath = public_path('/photos');
			$request->photoUrl->move($destinationPath, $filenametostore); // uploading file to given path

			$thumbnailpath = public_path('/photos'.'/'.$filenametostore);
			$img = Image::make($thumbnailpath)->resize(400, 500, function($constraint) {
					$constraint->aspectRatio();
			});
			$img->save($thumbnailpath);

			// $imgwatermark = Image::make(public_path('/photos'.'/'.$filenametostore));
			// /* insert watermark at bottom-right corner with 10px offset */
			// $imgwatermark->insert(public_path('images/logo.png'), 'bottom-right', 10, 10);
			// $imgwatermark->save(public_path('/photos'.'/'.$filenametostore));

		}
		else {
			$filenametostore = "medicine.png";
		}

			$productName = $request->productName;
			$unit = $request->unit;
			$categoryId = $request->categoryId;
			$alertLevel = $request->alertLevel;

			$sellPrice = $request->sellPrice;

			$saveimg = Products::storeone($productName,$unit,$categoryId,$filenametostore,$alertLevel,$sellPrice);
			if($saveimg) {
				return redirect()->back()->with(['success' => "Product was added successfully"]);
			}
			else {
				return redirect()->back()->with(['error' => "Failed to add product"]);
			}

	// }
	// else {
	// 	return redirect()->back()->withInput($request->input())->with('error', 'Failed to upload photo');
	// }
}

public function editproduct(Request $request) {

$details = Products::where('id',$request->id)->first();

if($request->hasFile('photoUrl')) {

	$request->validate([

				'photoUrl' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

		]);

		//get filename with extension
		$filenamewithextension = $request->file('photoUrl')->getClientOriginalName();

		//get filename without extension
		$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

		//get file extension
		$extension = $request->file('photoUrl')->getClientOriginalExtension();

		//filename to store
		$filenametostore = $filename.'_'.time().'.'.$extension;

		//Upload File
		$destinationPath = public_path('/photos');
		$request->photoUrl->move($destinationPath, $filenametostore); // uploading file to given path

		$thumbnailpath = public_path('/photos'.'/'.$filenametostore);
		$img = Image::make($thumbnailpath)->resize(400, 500, function($constraint) {
				$constraint->aspectRatio();
		});
		$img->save($thumbnailpath);

		// $imgwatermark = Image::make(public_path('/photos'.'/'.$filenametostore));
		// /* insert watermark at bottom-right corner with 10px offset */
		// $imgwatermark->insert(public_path('images/logo.png'), 'bottom-right', 10, 10);
		// $imgwatermark->save(public_path('/photos'.'/'.$filenametostore));

	}
	else {
		$filenametostore = $details->photoUrl;
	}

		$id = $request->id;
		$productName = $request->productName;
		$unit = $request->unit;
		$categoryId = $request->categoryId;
		$alertLevel = $request->alertLevel;
		$sellPrice = $request->sellPrice;

		$saveimg = Products::updateone($id,$productName,$unit,$categoryId,$filenametostore,$alertLevel,$sellPrice);
		if($saveimg) {
			return redirect()->back()->with(['success' => "Product was updated successfully"]);
		}
		else {
			return redirect()->back()->with(['error' => "Failed to update product"]);
		}
}

	public function deleteproduct(Request $request)
	{
			$id = $request->id;
			if($id !=1 && $id !=2) {
			$delete = Products::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
		} else {
				return Redirect::back()->with(['status0'=>'You cannot delete this product']);
		}
	}

	public function ejectStock(Request $request)
	{
			$id = $request->id;
			$quantity = $request->quantity;
			$check = Products::where('id',$id)->first();
			if($check->stockbalance > $quantity) {
			$delete = Batches::ejectStock($id,$quantity);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Quantities ejected successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while ejecting quantities']);
			}
		}
		else {
			return Redirect::back()->with(['status0'=>'Insufficient quantity balance']);
		}
	}

}
