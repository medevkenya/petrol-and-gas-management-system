<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Activities;
use App\Roles;
use App\Expenses;
use App\Sales;
use App\Customers;
use App\Suppliers;
use App\Tanks;
use App\Paiddebts;
use App\Creditpayments;
use App\Batches;
use App\Customerconsumption;
use Mail;
use Hash;
use Auth;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Log;
use Carbon\Carbon;

class AccountController extends Controller
{

    public function dashboard()
    {

      for ($i=0; $i<=6; $i++) {
         $dates[] = Carbon::now()->subDays($i)->format('Y-m-d');
      }

     //  return Sales::where('created_at', '>', Carbon::now()->startOfWeek())
     // ->where('created_at', '<', Carbon::now()->endOfWeek())
     // ->get();

        $weeklydata = Sales::whereIn('date', $dates)
        ->groupBy('date')
        ->orderBy('date', 'ASC')
        ->get(array(
             DB::raw('Date(created_at) as date'),
             DB::raw('SUM(total) as "total"')
          ))
          ->keyBy('date');

        $arrays = Sales::processDates($weeklydata);
        $chartdays = $arrays['day'];
        $charttotals = $arrays['total'];

        $stocks = Batches::getTotalStockValue();
        $stockvalue = $stocks['total'];
        $totalexpenses = Expenses::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('amount');


        $totaldebtorslist = Customerconsumption::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('amount');
        $totalcreditorslist = Batches::where('adminId',Auth::user()->adminId)->where('type','Credit')->where('isDeleted',0)->sum('total');

        $paiddebtslist = Paiddebts::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('amount');
        $paidcreditslist = Creditpayments::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('amount');

        $totaldebtors = $totaldebtorslist - $paiddebtslist;
        $totalcreditors = $totalcreditorslist - $paidcreditslist;

        $previous_week = strtotime("-1 week +1 day");
        $start_week = strtotime("last sunday midnight",$previous_week);
        $end_week = strtotime("next saturday",$start_week);
        $start_week = date("Y-m-d",$start_week);
        $end_week = date("Y-m-d",$end_week);

        $lastweek = Sales::whereBetween('created_at', [$start_week, $end_week])->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');
        $thisweek = Sales::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');

        if($lastweek > $thisweek) {
          $weeksign = "-";
          $diff = $lastweek - $thisweek;
          $wpercentage = ($diff / $lastweek) * 100;
        }
        else {
          if($thisweek > 0) {
          $weeksign = "+";
          $diff = $thisweek - $lastweek;
          $wpercentage = ($diff / $thisweek) * 100;
        }
          else {
            $weeksign = null;
            $wpercentage = 0;
          }
        }


        $currentMonth = date('m');
        $lastmonth = Sales::whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');
        $thismonth = Sales::whereRaw('MONTH(created_at) = ?',[$currentMonth])->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');

        if($lastmonth > $thismonth) {
          $monthsign = "-";
          $mdiff = $lastmonth - $thismonth;
          $mpercentage = ($mdiff / $lastmonth) * 100;
        }
        else {
          if($thismonth > 0) {
          $monthsign = "+";
          $mdiff = $thismonth - $lastmonth;
          $mpercentage = ($mdiff / $thismonth) * 100;
        }
        else {
          $monthsign = null;
          $mpercentage = 0;
        }
        }

        $customers = Customers::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
        $suppliers = Suppliers::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
        $creditors = Suppliers::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();
        $debtors = Customers::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->count();

        return view('dashboard',['customers'=>$customers,'suppliers'=>$suppliers,'creditors'=>$creditors,'debtors'=>$debtors,
        'monthsign'=>$monthsign,'mpercentage'=>round($mpercentage),'wpercentage'=>round($wpercentage),
        'weeksign'=>$weeksign,'thisweek'=>number_format($thisweek,0),'thismonth'=>number_format($thismonth,0),'totalcreditors'=>number_format($totalcreditors,0),
        'chartdays'=>$chartdays,'charttotals'=>$charttotals,'weeklydata'=>$weeklydata,'totaldebtors'=>number_format($totaldebtors,0),'totalexpenses'=>number_format($totalexpenses,0),'stockvalue'=>number_format($stockvalue,0)]);

    }

    public function profile()
    {

      $userid	= Auth::user()->id;
      $details = User::where('id',$userid)->first();
      return view('profile',['details'=>$details]);

    }

    public function auditlogs()
    {
      $list = Activities::select('activities.*','users.firstName','users.lastName')
      ->leftJoin('users','activities.created_by','=','users.id')->orderBy('activities.id','DESC')->get();
      return view('activities',['list'=>$list]);
    }

    public function changepassword()
    {
        return view('changepassword');
    }

    public function doChangePass(Request $request)
    {
        $oldpassword	= $request->oldpassword;
        $password	= $request->password;
        $cpassword 	= $request->cpassword;
        $userid	= Auth::user()->id;

        $oldhashedPassword = Hash::make($oldpassword);

        $checkold = User::where('id', '=', $userid)->first();
        if (!Hash::check($oldpassword, $checkold->password)) {
            return redirect()->back()->withInput($request->input())->with('error', 'Current password is wrong');
        } elseif (strlen($password) < 6) {
            return redirect()->back()->withInput($request->input())->with('error', 'Password must be at least 6 characters');
        } elseif ($password != $cpassword) {
            return redirect()->back()->withInput($request->input())->with('error', 'Passwords do not match');
        }
        else
        {

            $hashedPassword = Hash::make($password);

            $edituserpass = User::where('id', $userid)->update(['password' => $hashedPassword]);
            if ($edituserpass) {
                return redirect()->back()->withInput($request->input())->with('success', 'Your password was changed successfully');
            } else {
                return redirect()->back()->withInput($request->input())->with('error', 'Failed to change password, try again');
            }

        }
    }


    public function updateProfile(Request $request)
    {
      //https://stackoverflow.com/questions/51962486/datepicker-set-maximum-date-to-18-years-ago-from-current-date
        request()->validate([
            'firstName' => 'required|min:3|max:50',
            'lastName' => 'required|min:3|max:50',
            'mobileNo' => 'required',// input starts with 01 and is followed by 9 numbers
            'email' => 'required',
            // 'dob' => 'required|date_format:d-m-Y|before:18 years',
            //'terms' => 'accepted',
        ], [
            'firstName.required' => 'First Name is required',
            'firstName.min' => 'First Name must be at least 3 characters.',
            'firstName.max' => 'First Name should not be greater than 20 characters.',
            'lastName.required' => 'Last Name is required',
            'lastName.min' => 'Last Name must be at least 3 characters.',
            'lastName.max' => 'Last Name should not be greater than 20 characters.',
            'mobileNo.required' => 'Mobile Number is required',
            'mobileNo.regex' => 'Mobile number should be 10 digits starting with 07',
            //'about.required' => 'Provide brief description about yourself',
            //'terms.required' => 'Please accept our terms of service',
        ]);

        // $input = request()->except('password','confirm_password');
        // $user=new User($input);//
        // $user->password=bcrypt(request()->password);
        // $user->save();
        //request()->firstName,request()->lastName,request()->mobileNo,,request()->password
        $res = User::editMyProfile(request()->all());
        if($res) {
          return redirect()->back()->withInput($request->input())->with('success', 'Your profile was updated successfully');
        }
        else {
          return redirect()->back()->withInput($request->input())->with('error', 'Failed to save. Please try again');
        }

    }

    public function changePic()
    {
      $userid	= Auth::user()->id;
      $details = User::where('id',$userid)->first();
      return view('changePic',['details'=>$details]);
    }

    public function UpdatePic(Request $request)
    {

  // getting all of the post data
        $id	= Auth::user()->id;
        $file = array('image' => $request->profilePic);

        // setting up rules
        //  $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000 required|image|mimes:jpeg,png,jpg,gif,svg|max:2048
        $rules = array('image' => 'required|image|mimes:jpeg,png,jpg,gif|max:5048',);
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);

        // checking file is valid.
        if ($request->profilePic->isValid()) {
            //$destinationPath = 'otheruploads'; // upload path
            $destinationPath = public_path('/profiles');

            $extension = $request->profilePic->getClientOriginalExtension(); // getting image extension

            $fileName = rand(11111, 99999).'.'.$extension; // renameing image

            $request->profilePic->move($destinationPath, $fileName); // uploading file to given path

            //file name to take to db
            $profilePic=  $fileName;

            User::where('id', $id)->update(['profilePic' => $profilePic]);

            return Redirect::to('profile')->with(['status1'=>'Your profile picture was updated successfully.']);
        } else {
            return Redirect::to('profile')->with(['status0'=>'Sorry, error occurred while updating your profile picture. Try again.']);
        }
    }

    public function ErrorPage()
    {
        $datee = date('Y-m-d');
        return view('error');
    }

}
