<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Notifications;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class NotificationsController extends Controller {

	public function notifications()
	{
		return view('notifications');
	}

	public function addnotification(Request $request)
	{
			$message = $request->message;
			$adminId	= Auth::user()->adminId;

			$add = Notifications::storeone($message);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New notification was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating notification.']);
			}

	}

}
