<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Products;
use App\Cart;
use App\Orders;
use App\Settings;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class SalesController extends Controller {

	public function sales(Request $request)
	{

		//Cart::where('sessionId',Session::getId())->update(['completed'=>1]);

		$settings = Settings::where('adminId',Auth::user()->adminId)->first();

		if(!empty($request->id)) {
			$list = Products::leftJoin('cart', 'products.id', '=', 'cart.productId')
			->leftJoin('categories','products.categoryId','=','categories.id')
				->selectRaw('products.*,categories.categoryName, count(cart.productId) as cartCount')
				->where('products.categoryId',$request->id)
				->where('products.isDeleted',0)
				->groupBy('products.id')
				->orderBy('cartCount','DESC')
				->paginate(40);
		}
		else {
		$list = Products::leftJoin('cart', 'products.id', '=', 'cart.productId')
		->leftJoin('categories','products.categoryId','=','categories.id')
			->selectRaw('products.*,categories.categoryName,count(cart.productId) as cartCount')
			->where('products.isDeleted',0)
			->groupBy('products.id')
			->orderBy('cartCount','DESC')
			->paginate(40);
		}
		return view('sales',['list'=>$list,'settings'=>$settings]);
	}

	public function search(Request $request)
	{

		$settings = Settings::where('adminId',Auth::user()->adminId)->first();

		if(!empty($request->search)) {
			$list = Products::leftJoin('cart', 'products.id', '=', 'cart.productId')
			->leftJoin('categories','products.categoryId','=','categories.id')
      ->selectRaw('products.*,categories.categoryName, count(cart.productId) as cartCount')
			->where('productName', 'LIKE', "%$request->search%")
			->where('products.isDeleted',0)
      ->groupBy('products.id')
			->orderBy('cartCount','DESC')
			->limit(20)
      ->paginate(20);
		}
		else {
			return redirect()->back()->withInput($request->input())->with('error', 'Invalid search term');
		}
		return view('sales',['list'=>$list,'settings'=>$settings]);
	}

	public static function savecart(Request $request)
	{

		if(!empty($request->productId)) {
			if(Cart::saveNew($request->productId)) {
        $response = array(
            'status' => '1',
            'message'=>'Your cart was successfully saved'
        );
      }
      else {
        $response = array(
            'status' => '0',
            'message'=>'Failed to save cart. Please try again'
        );
      }

		}
		else {
			$response = array(
					'status' => '0',
					'message'=>'Invalid request. Please try again'
			);
		}
		return response()->json($response);
	}

	public static function getCart(Request $request)
	{

		$settings = Settings::where('adminId',Auth::user()->adminId)->first();
		$data = Cart::getCart();
		$total = Cart::where('sessionId',Session::getId())->where('completed',0)->sum('totalCost');
    $response = array(
        'data' => $data,
				'subtotal'=>$total - $settings->discount,
				'total'=>$total + ($settings->tax / 100 * $total) - $settings->discount,
				'discount'=>$settings->discount,
				'tax'=>$settings->tax,
				'taxamount'=>$settings->tax / 100 * $total,
        'message'=>'your cart'
    );
		return response()->json($response);

	}

	public function increaseCart(Request $request)
  {

    log::info("post productId--".$request->productId);

    $res = Cart::increaseCart($request->productId);
    if($res) {
      return response()->json(['success'=>'Successfully added to cart']);
    }
    else {
      return response()->json(['error'=>'Failed to add, please try again']);
    }

  }

  public function decreaseCart(Request $request)
  {

    log::info("post productId--".$request->productId);

    $res = Cart::decreaseCart($request->productId);
    if($res) {
      return response()->json(['success'=>'Successfully removed from cart']);
    }
    else {
      return response()->json(['error'=>'Failed to remove, please try again']);
    }

  }

	public function completePrint(Request $request)
	{

		//log::info("post subTotal--".$request->subtotal);
		// $data = Cart::getCart();
		// foreach ($data as $key) {
		// 	$check = Batches::where('productId',$key->productId)->where('status','In Stock')->where('isDeleted',0)->orderBy('id','ASC')->first();
		// 	if($check)
		// 	{
		//
		// 	}
		// }

		$res = Cart::completesale($request->subtotal,$request->total,$request->discount,$request->tax,$request->taxamount);
		if($res) {
			return response()->json(['success'=>'Successfully completed sale']);
		}
		else {
			return response()->json(['error'=>'Failed to complete sale, please try again']);
		}

	}


}
