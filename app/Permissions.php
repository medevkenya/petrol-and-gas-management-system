<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Permissions extends Model
{
    protected $table = 'permissions';

    public static function getAll()
    {
        return Permissions::where('isDeleted',0)->get();
    }

    public static function updateone($id, $roleName)
    {
        $model = Permissions::find($id);
        $model->roleName = $roleName;
        $model->save();
        if ($model) {
            return true;
        }
        return false;
    }

}
