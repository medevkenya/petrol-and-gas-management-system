<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Shifttypes extends Model
{
    protected $table = 'shifttypes';

    public static function getAll() {
      return $list = Shifttypes::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function storeone($shifttypeName)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Shifttypes;
        $model->shifttypeName = $shifttypeName;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new shift type [".$shifttypeName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $shifttypeName)
    {
        $model = Shifttypes::find($id);
        $model->shifttypeName = $shifttypeName;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited shift type [".$shifttypeName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Shifttypes::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted shift type [".$id."]");
            return true;
        }
        return false;
    }

}
