<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Pumpreadings extends Model
{
    protected $table = 'pumpreadings';

    public static function getAll() {
        return Pumpreadings::select('pumpreadings.*','pumps.pumpName','shifttypes.shifttypeName')
        ->leftJoin('shifttypes','pumpreadings.shiftTypeId','=','shifttypes.id')
        ->leftJoin('pumps','pumpreadings.pumpId','=','pumps.id')
        ->where('pumpreadings.isDeleted',0)->orderBy('pumpreadings.id','DESC')->get();
    }

    public static function getToday($pumpId) {
      return Pumpreadings::select('pumpreadings.*','shifttypes.shifttypeName')
      ->leftJoin('shifttypes','pumpreadings.shiftTypeId','=','shifttypes.id')
      ->where('pumpreadings.pumpId',$pumpId)->where('pumpreadings.date',date('Y-m-d'))
      ->where('pumpreadings.isDeleted',0)->orderBy('pumpreadings.id','DESC')->get();
    }

    // public static function storeone($pumpName)
    // {
    //
    //     $adminId	= Auth::user()->adminId;
    //     $created_by	= Auth::user()->id;
    //     $model = new Pumpreadings;
    //     $model->pumpName = $pumpName;
    //     $model->adminId = $adminId;
    //     $model->created_by = $created_by;
    //     $model->save();
    //     if ($model)
    //     {
    //       Activities::saveLog("Added new pump [".$pumpName."]");
    //       return true;
    //     }
    //
    //     return false;
    //
    // }

    public static function editpumpreading($id,$openReading,$closeReading)
    {
        $model = Pumpreadings::find($id);
        $model->openReading = $openReading;
        $model->closeReading = $closeReading;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited pump readings openReading [".$openReading."] closeReading [".$closeReading."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Pumpreadings::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted pump readings openReading [".$model->openReading."] closeReading [".$model->closeReading."]");
            return true;
        }
        return false;
    }

}
