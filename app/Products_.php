<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Sales;
use App\Batches;
use App\Readings;
use App\Stockbalances;

class Products extends Model
{

    protected $table = 'products';

    public static function getAll()
    {
      return Products::select('products.*','categories.categoryName')
      ->leftJoin('categories','products.categoryId','=','categories.id')
      ->where('products.adminId',Auth::user()->adminId)->where('products.isDeleted',0)->orderBy('products.id','DESC')->get();
    }

    public static function getLowStockAll()
    {
      return Products::select('products.*','categories.categoryName')
      ->leftJoin('categories','products.categoryId','=','categories.id')
      ->where('products.adminId',Auth::user()->adminId)->where('products.isDeleted',0)->orderBy('products.id','DESC')->get();
    }

    public static function MonthProductSales($productId,$currentMonth) {
      $quantity = 0;
      $profit = 0;
      $totalCost = 0;
      $totalSales = 0;
      return $list = Sales::where('productId',$productId)
      ->whereMonth('created_at', $currentMonth)
      //->where('adminId',Auth::user()->adminId)
      ->where('isDeleted',0)->get();
      foreach ($list as $key) {
        $totalCost += intval(preg_replace('/[^\d.]/', '', $key->costPrice)) * $key->quantity;
        $totalSales += intval(preg_replace('/[^\d.]/', '', $key->unitPrice)) * $key->quantity;
      }
      $profit = $totalCost - $totalSales;
      return array('profit'=>$profit,'totalCost'=>$totalCost,'totalSales'=>$totalSales);
    }

    public static function getStatements($productId)
    {

      $today = date('Y-m-d');
      $yesterday = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $today) ) ));
      $currentMonth = date('m');
      $openingReading = null;
      $meterReading = null;
      $lossExcess = null;
      $rtt = 0;

      $statements = [];
      if($productId == 0) {
      $products = Products::getAll();
      }
      else {
        $products = Products::where('id',$productId)->where('isDeleted',0)->orderBy('id','DESC')->get();
      }

      foreach ($products as $keystat)
      {

        $productName = $keystat->productName;

        if($keystat->id == 1 || $keystat->id == 2) {
          $openingReading = Stockbalances::where('productId',$keystat->id)->where('date',$yesterday)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->limit(1)->first();
          if($openingReading) {
            $openingReading = $openingReading->balance;
          }
          else {
            $openingReading = 0;
          }
          $meterReading = Readings::where('productId',$keystat->id)->where('date',$today)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->limit(1)->first();
          if($meterReading) {
            $meterReading = $meterReading->meterReading;
          }
          else {
            $meterReading = 0;
          }
        }

        $soldAmount = Sales::where('productId',$keystat->id)->where('date',$today)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');
        $soldQuantity = Sales::where('productId',$keystat->id)->where('date',$today)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('quantity');

        $purchaseAmount = Batches::where('productId',$keystat->id)->where('date',$today)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');
        $purchaseQuantity = Batches::where('productId',$keystat->id)->where('date',$today)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('quantity');

        $actualBalance = Batches::getProductQuantity($keystat->id);

        if($keystat->id == 1 || $keystat->id == 2)
        {
          $lossExcess = $actualBalance - $meterReading;
        }

        $costPrice = $keystat->costPrice;
        $sellPrice = $keystat->sellPrice;

        $totalSales = $soldQuantity * $sellPrice;
        $totalCost =  $soldQuantity * $costPrice;

        if($totalSales > 0) {
          $profit = $totalSales - $totalCost;
        }
        else {
          $profit = 0;
        }

        $statements[] = array('productName'=>$productName,'meterReading'=>$meterReading,'openingReading'=>$openingReading,'rtt'=>$rtt,
        'actualBalance'=>number_format($actualBalance,2),'lossExcess'=>number_format($lossExcess,2),'costPrice'=>number_format($costPrice,2),
        'sellPrice'=>number_format($sellPrice,2),'totalSales'=>number_format($totalSales,2),'totalCost'=>number_format($totalCost,2),
        'profit'=>number_format($profit,2),'soldAmount'=>number_format($soldAmount,2),'soldQuantity'=>$soldQuantity,'purchaseAmount'=>number_format($purchaseAmount,2),
        'purchaseQuantity'=>number_format($purchaseQuantity,2));

      }

      return $statements;

    }

    public static function getProfitabilityStatements($month)
    {

      $today = date('Y-m-d');
      $yesterday = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $today) ) ));
      $currentMonth = $month;//date('m');

      $statements = [];
return $getproductsales = self::MonthProductSales(3,$currentMonth);
      //$expenseForMonth = Expenses::expensesForMonth($currentMonth);

      $products = Products::getAll();

      $mgrandProfit = 0;

      foreach ($products as $keystat)
      {

        $productName = $keystat->productName;

        $soldAmount = Sales::where('productId',$keystat->id)->whereMonth('created_at', $currentMonth)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');
        $soldQuantity = Sales::where('productId',$keystat->id)->whereMonth('created_at', $currentMonth)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('quantity');

        $taxAmount = Sales::where('productId',$keystat->id)->whereMonth('created_at', $currentMonth)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('taxAmount');

        $purchaseAmount = Batches::where('productId',$keystat->id)->whereMonth('created_at', $currentMonth)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');
        $purchaseQuantity = Batches::where('productId',$keystat->id)->whereMonth('created_at', $currentMonth)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('quantity');

        $actualBalance = Batches::getProductQuantity($keystat->id);

        //$costPrice = $keystat->costPrice;
        //$sellPrice = $keystat->sellPrice;

        $getproductsales = self::MonthProductSales($keystat->id,$currentMonth);

        $totalSales = $getproductsales['totalSales'];
        $totalCost =  $getproductsales['totalCost'];

        if($totalSales > 0) {
          $profit = $totalSales - $totalCost;
        }
        else {
          $profit = 0;
        }

        //$mgrandProfit += intval(preg_replace('/[^\d.]/', '', $profit));

        $statements[] = array('id'=>$keystat->id,'productName'=>$productName,
        'actualBalance'=>number_format($actualBalance,0),
        'totalSales'=>number_format($totalSales,0),'totalCost'=>number_format($totalCost,0),
        'profit'=>number_format($profit,0),'soldAmount'=>number_format($soldAmount,2),'soldQuantity'=>$soldQuantity,
        'purchaseAmount'=>number_format($purchaseAmount,0),'taxAmount'=>number_format($taxAmount,0),
        'purchaseQuantity'=>number_format($purchaseQuantity,0));

      }

      return $statements;

    }

    public static function getYearProfitabilityStatements($year)
    {

      $today = date('Y-m-d');
      $yesterday = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $today) ) ));
      $currentMonth = date('m');

      $statements = [];

      $products = Products::getAll();

      foreach ($products as $keystat)
      {

        $productName = $keystat->productName;

        $soldAmount = Sales::where('productId',$keystat->id)->whereYear('created_at', $year)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');
        $soldQuantity = Sales::where('productId',$keystat->id)->whereYear('created_at', $year)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('quantity');

        $taxAmount = Sales::where('productId',$keystat->id)->whereYear('created_at', $year)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('taxAmount');

        $purchaseAmount = Batches::where('productId',$keystat->id)->whereYear('created_at', $year)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('total');
        $purchaseQuantity = Batches::where('productId',$keystat->id)->whereYear('created_at', $year)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('quantity');

        $actualBalance = Batches::getProductQuantity($keystat->id);

        $costPrice = $keystat->costPrice;
        $sellPrice = $keystat->sellPrice;

        $totalSales = $soldQuantity * $sellPrice;
        $totalCost =  $soldQuantity * $costPrice;

        if($totalSales > 0) {
          $profit = $totalSales - $totalCost;
        }
        else {
          $profit = 0;
        }

        $statements[] = array('productName'=>$productName,
        'actualBalance'=>number_format($actualBalance,0),'costPrice'=>number_format($costPrice,0),
        'sellPrice'=>number_format($sellPrice,0),'totalSales'=>number_format($totalSales,0),'totalCost'=>number_format($totalCost,0),
        'profit'=>number_format($profit,0),'soldAmount'=>number_format($soldAmount,0),'soldQuantity'=>$soldQuantity,
        'purchaseAmount'=>number_format($purchaseAmount,0),'taxAmount'=>number_format($taxAmount,0),
        'purchaseQuantity'=>number_format($purchaseQuantity,0));

      }

      return $statements;

    }

    public static function storeone($productName,$unit,$categoryId,$photoUrl,$alertLevel,$costPrice,$sellPrice)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Products;
        $model->productName = $productName;
        $model->categoryId = $categoryId;
        $model->unit = $unit;
        $model->photoUrl = $photoUrl;
        $model->alertLevel = $alertLevel;
        $model->sellPrice = $sellPrice;
        $model->costPrice = $costPrice;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new product [".$productName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $productName,$unit,$categoryId,$photoUrl,$alertLevel,$costPrice,$sellPrice)
    {
        $model = Products::find($id);
        $model->productName = $productName;
        $model->categoryId = $categoryId;
        $model->unit = $unit;
        $model->photoUrl = $photoUrl;
        $model->alertLevel = $alertLevel;
        $model->sellPrice = $sellPrice;
        $model->costPrice = $costPrice;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited product [".$productName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Products::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted product [".$id."]");
            return true;
        }
        return false;
    }

}
