<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Userpermissions;
use App\Activities;
use App\Batches;
use App\Creditpayments;

class Suppliers extends Model
{
    protected $table = 'suppliers';

    public static function getAll()
    {
        return Suppliers::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
    }

    public static function getStatements($supplierId) {
      $firstDayofThisMonth = date('Y-m-01');
      $currentMonth = date('m');

      $statements = [];
      if($supplierId == 0) {
      $suppliers = Suppliers::getAll();
    }
    else {
      $suppliers = Suppliers::where('id',$supplierId)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }
      foreach ($suppliers as $keystat) {
        $supplierName = $keystat->supplierName;

        $currentMonthDebt = Batches::where('supplierId',$keystat->id)->whereRaw('MONTH(created_at) = ?',[$currentMonth])->where('adminId',Auth::user()->adminId)->where('type','Credit')->where('isDeleted',0)->sum('total');
        $debtPaidThisMonth = Creditpayments::where('supplierId',$keystat->id)->whereRaw('MONTH(created_at) = ?',[$currentMonth])->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('amount');

        $amountOwedBeforeThisMonth = Batches::where('supplierId',$keystat->id)->where('date','<',$firstDayofThisMonth)->where('adminId',Auth::user()->adminId)->where('type','Credit')->where('isDeleted',0)->sum('total');
        $amountPaidBeforeThisMonth = Creditpayments::where('supplierId',$keystat->id)->where('date','<',$firstDayofThisMonth)->where('adminId',Auth::user()->adminId)->where('isDeleted',0)->sum('amount');

        if($amountOwedBeforeThisMonth > 0) {
          $balanceBroughtForward = $amountOwedBeforeThisMonth - $amountPaidBeforeThisMonth;
        }
        else {
          $balanceBroughtForward = 0;
        }

        $netDebt = ($balanceBroughtForward + $currentMonthDebt) - $debtPaidThisMonth;

        $statements[] = array('supplierId'=>$keystat->id,'supplierName'=>$supplierName,'balanceBroughtForward'=>number_format($balanceBroughtForward,2),'currentMonthDebt'=>$currentMonthDebt,'debtPaidThisMonth'=>$debtPaidThisMonth,'netDebt'=>number_format($netDebt,2));
      }
      return $statements;
    }

    public static function storeone($supplierName,$email,$contacts)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Suppliers;
        $model->supplierName = $supplierName;
        $model->email = $email;
        $model->contacts = $contacts;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
        Activities::saveLog("Added new supplier [".$supplierName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $supplierName,$email,$contacts)
    {
        $model = Suppliers::find($id);
        $model->supplierName = $supplierName;
        $model->email = $email;
        $model->contacts = $contacts;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited supplier [".$supplierName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Suppliers::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted supplier [".$id."]");
            return true;
        }
        return false;
    }

}
