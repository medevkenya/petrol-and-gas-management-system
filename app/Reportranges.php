<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Reportranges extends Model
{
    protected $table = 'reportranges';

    public function getLast($type)
    {

        $created_by	= Auth::user()->id;
        return Reportranges::select('id','fromdate','todate')->where('created_by',$created_by)->where('isDeleted',0)->orderBy('id','DESC')->limit(1)->first();

    }

    public static function saveone($type,$fromdate,$todate,$customId=null)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Reportranges;
        $model->fromdate = $fromdate;
        $model->todate = $todate;
        $model->type = $type;
        $model->customId = $customId;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          return true;
        }

        return false;

    }

}
